import argparse
import os
import ROOT

parser = argparse.ArgumentParser(description='Plot result of a fit')
parser.add_argument('input_file')
parser.add_argument('--ws-name', default='combWS')
parser.add_argument('--data', default='AsimovSB')
parser.add_argument('--output-prefix')
parser.add_argument('--nbins', type=int, default=220)
args = parser.parse_args()

ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)


if args.output_prefix is None:
    args.output_prefix = os.path.splitext(os.path.basename(args.input_file))[0]

f = ROOT.TFile.Open(args.input_file)
ws = f.Get(args.ws_name)


cat = ws.obj("channellist")
ncat = cat.numTypes()
cat_names = [cat.lookupType(i).GetName() for i in range(ncat)]
top_pdf = ws.obj('CombinedPdf')

data = ws.data(args.data)
data_splitted = data.split(cat, True)

RFNorm = ROOT.RooFit.Normalization(1.0, ROOT.RooAbsReal.RelativeExpected)

for icat in range(ncat):
    canvas = ROOT.TCanvas()
    frame = ws.obj('atlas_invMass_%s' % cat_names[icat]).frame(ROOT.RooFit.Title("category: %d" % icat), ROOT.RooFit.Bins(args.nbins))
    data.plotOn(frame, ROOT.RooFit.DataError(ROOT.RooAbsData.Poisson),
                ROOT.RooFit.Cut("channellist==channellist::%s" % cat_names[icat]))
    pdfi = top_pdf.getPdf(cat_names[icat])

    pdfi.plotOn(frame, ROOT.RooFit.Components("pdf__background_{catname}".format(catname=cat_names[icat])), RFNorm, ROOT.RooFit.LineStyle(ROOT.kDashed))
    pdfi.plotOn(frame, RFNorm, ROOT.RooFit.LineColor(ROOT.kRed-4))


    frame.GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
    frame.GetYaxis().SetTitleOffset(1.4)
    frame.Draw()
    canvas.SaveAs('%s__category_%d.pdf' % (args.output_prefix, icat))
