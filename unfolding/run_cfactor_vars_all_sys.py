import papermill as pm
import os

import sys
sys.path.append('..')
from common import common

def create_dir_if_not_exits(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

output_dir = '../output/cfactor_vars-papermill-notebooks/'
create_dir_if_not_exits(output_dir)
        
for quantity in common.quantities:
    output_fn = os.path.join(output_dir, 'bin_by_bin_unfolding_{quantity}.ipynb'.format(quantity=quantity))
    pm.execute_notebook('bin_by_bin_unfolding.ipynb',
                        output_fn,
                        parameters=dict(quantity=quantity))
