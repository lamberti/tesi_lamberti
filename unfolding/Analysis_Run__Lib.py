from array import array
import numpy as np

name_variables = [
    'm_yy',
    'pT_yy',
    'yAbs_yy',
    'Dphi_j_j_30_signed',
    'pT_j1_30',
    'N_j_30',
    'm_jj_30'
    ]

m_yy_nbins = 220
m_yy_nparray = np.linspace(105E3, 160E3, m_yy_nbins + 1)

binnings = {
    'm_yy': array('f', m_yy_nparray), #MeV
    'pT_yy': array('f', [0, 5E3, 10E3, 15E3, 20E3, 25E3, 30E3, 35E3, 45E3, 60E3, 80E3, 100E3, 120E3, 140E3, 170E3, 200E3, 250E3, 350E3]), #MeV
    'yAbs_yy': array('f', [0, 0.15, 0.30, 0.45, 0.60, 0.75, 0.90, 1.2, 1.6, 2.4]),
    'Dphi_j_j_30_signed': array('f', [-3.15, -1.57, 0, 1.57, 3.15]),
    'pT_j1_30': array('f', [0, 30E3, 60E3, 90E3, 120E3, 350E3]), #MeV
    'N_j_30': array('f', [0, 1, 2, 3]),
    'm_jj_30': array('f', [0, 170E3, 500E3, 1500E3]) #MeV
    }

formulas_true = {
    'm_yy': 'm_yy_true',
    'pT_yy': 'pT_yy_true',
    'yAbs_yy': 'yAbs_yy_true',
    'Dphi_j_j_30_signed': 'Dphi_j_j_true',
    'pT_j1_30': 'pT_j1_true',
    'N_j_30': 'N_j_true',
    'm_jj_30': 'm_jj_true'
    }

formulas_reco = {
    'm_yy': 'm_yy_reco',
    'pT_yy': 'pT_yy_reco',
    'yAbs_yy': 'yAbs_yy_reco',
    'Dphi_j_j_30_signed': 'Dphi_j_j_reco',
    'pT_j1_30': 'pT_j1_reco',
    'N_j_30': 'N_j_reco',
    'm_jj_30': 'm_jj_reco'
    }

name_histos = [
    'histo1d_true__All_notDalitz',
    'histo1d_true__All_isDalitz',
    'histo1d_true__isFiducial_notDalitz',
    'histo1d_true__notFiducial_notDalitz',
    'histo1d_reco__All_isPassed',
    'histo1d_reco__isDalitz_isPassed',
    'histo1d_reco__isFiducial_isPassed', 
    'histo1d_reco__notFiducial_isPassed', 
    'histo1d_reco__notFiducial_isPassed_notDalitz',
    'histo2d__isFiducial_isPassed_notDalitz'
]

weights = {
    'truth': 'w_initial',
    'reco': 'w_final'
    }

branches_types = [
    'truth',
    'reco'
    ]
