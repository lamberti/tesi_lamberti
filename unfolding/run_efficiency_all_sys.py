import papermill as pm
import os

import sys
sys.path.append('..')
from common import common

def create_dir_if_not_exits(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

from glob import glob
basedir = '../merged_normalized_final'
all_root_fn = glob(os.path.join(basedir, '*.root'))   # the nominal one
all_root_fn += glob(os.path.join(basedir, '**/*1up*.root'))

for i, fn in enumerate(all_root_fn):
    print("%d. %s" % (i, fn))

output_dir = '../output/unfolding-papermill-notebooks/'
create_dir_if_not_exits(output_dir)
    
for quantity in common.quantities:
    for fn in all_root_fn:
        output_fn = 'plot_to_unfolding_matrix_{quantity}__{fn}.ipynb'.format(quantity=quantity, fn=os.path.splitext(os.path.basename(fn))[0])
        output_fn = os.path.join(output_dir, output_fn)
        
        print("running on quantity: %s fn: %s" % (quantity, fn))
        print("output: %s" % output_fn)

        do_plots = False
        if 'EG_SCALE_ALL' in fn or 'PH_EFF_ISO' in fn or 'sysNominal' in fn:
            do_plots = True
        
        pm.execute_notebook(
            'plot_to_unfolding_matrix.ipynb',
            output_fn,
            parameters=dict(quantity=quantity, file_name=fn, do_plots=do_plots)
        )
