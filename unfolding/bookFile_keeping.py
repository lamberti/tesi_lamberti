from glob import glob
import os
import pickle

all_mc_names = ['mc16a', 'mc16d', 'mc16e']
all_production_names_dict = {'.PowhegPy8_NNLOPS_ggH125.': 'ggH',
                        '.PowhegPy8_ttH125.': 'ttH',
                        '.PowhegPy8_WmH125J.': 'WmH',
                        '.PowhegPy8_WpH125J.': 'WpH',
                        '.PowhegPy8_ZH125J.': 'ZH',
                        '.PowhegPy8EG_NNPDF30_VBFH125.': 'VBFH',
                        '.aMCnloHwpp_tWH125_yt_plus1.': 'tWH',
                        '.PowhegPy8_bbH125.': 'bbH',
                        '.aMCnloPy8_tHjb125_4fl_shw_fix.': 'tHjb',
                        '.PowhegPy8_ggZH125.': 'ggZH'
}
all_production_names = list(all_production_names_dict.values())



def get_mc_from_filename(fn):
    for mc_name in all_mc_names:
        if mc_name in fn:
            return mc_name
    raise ValueError("cannot find mc name in filename: %s" % fn)

def get_production_from_filename(fn):
    for production_name in all_production_names_dict:
        if production_name in fn:
            return all_production_names_dict[production_name]
    #raise ValueError("cannot find production name: %s" % fn)
        
def get_mxAODFlavour_from_filename(fn):
    result = fn.split('.MxAOD')[1].split('.')[0]
    if not result:
        return nominal
    return result

BASEDIR = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024"
all_root_files = []
mc_name = {}
production_name = {}


for r, d, f in os.walk(BASEDIR):
    if 'runs' in r or 'runs' in d:
        continue
    for ff in f:
#        print(r, d, ff, os.path.isfile(os.path.join(r, ff)))
        if not os.path.isfile(os.path.join(r, ff)):
            continue
        full_fn = os.path.join(r, ff)
        mc_name = get_mc_from_filename(full_fn)
        production_name = get_production_from_filename(full_fn)
        flavour = get_mxAODFlavour_from_filename(full_fn)
        all_root_files.append((mc_name, production_name, flavour, full_fn))
from pprint import pprint
#pprint(all_root_files)

# save all_root_files to pickle...

pickle_file = open('bookFile_dict.pkl', 'wb')
pickle.dump(all_root_files, pickle_file)
pickle_file.close()


"""
my_data = {'mc16a': 
           {'flavourSys':
            {'ggH': ['file1', 'file2', ...]

for mc, my_data_mc in my_data.iteritems():
             for flavour, my_data_mc_flavour in my_data_mc.iteritems():
             for process, files in my_data_mc_flavour.iteritems():
             for f in files:
             ...


oppure

for mc, prouction, flavour, fn in all_root_files:
             ...
"""
