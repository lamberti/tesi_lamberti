#!/usr/bin/env python

import ROOT
import os
from array import array
import Analysis_Run__Lib as pTLib
import pickle
import argparse
from argparse import RawTextHelpFormatter


parser = argparse.ArgumentParser(description='Dump yield for response matrix',
                                 formatter_class=RawTextHelpFormatter,
                                 epilog="""
                                 Example: python Analysis_Run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/LeptonMETSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODLeptonMETSys.e5607_s3126_r9364_p3665.h024.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODLeptonMETSys.e5607_s3126_r9364_p3665.h024.001.root --base_reco_branch HGamEventInfoAuxDyn --luminosity 36215 --flavourSys LeptonMETSys --prodMode ggH --output_name mc16a.PowhegPy8_NNLOPS_ggH125.MxAODLeptonMETSys.e5607_s3126_r9364_p3665.h024.001_sysNominal.root """)

parser.add_argument('input_file', help='ROOT file to be processed')
parser.add_argument('--tree_name', default='CollectionTree')
parser.add_argument('--base_reco_branch', default='HGamEventInfoAuxDyn')
parser.add_argument('--luminosity', help='luminosity') #LUMINOSITIES [mc16a = 36215; mc16d = 44307; mc16e = 58450]
parser.add_argument('--flavourSys', help='flavourSys')
parser.add_argument('--prodMode', help='production mode')
parser.add_argument('--output_name', help='output_file name')
parser.add_argument('--verbosity', default=False)
args = parser.parse_args()

RDF = ROOT.ROOT.RDataFrame
tree_name = args.tree_name
fn = args.input_file

print("INFO: Processing file '%s'" % fn)
print("INFO: Tree Name '%s'" % tree_name)
print("INFO: Base_Reco_Branch '%s'" % args.base_reco_branch)
f = ROOT.TFile(fn)
tree = f.Get(tree_name)
df = RDF(tree)

LUMI = args.luminosity
if LUMI == '36215':
    sampleSet_name = 'mc16a'
elif LUMI == '44307':
    sampleSet_name = 'mc16d'
elif LUMI == '58450':
    sampleSet_name = 'mc16e'
print("INFO: Luminosity %s" % LUMI)
print("INFO: Sample Set %s" % sampleSet_name)
output_file = ROOT.TFile.Open("%s" % args.output_name, "RECREATE")

initialWeights_Name = "%s.weightInitial" % args.base_reco_branch
finalWeights_Name = "%s.weightCatCoup_XGBoost_ttH" % args.base_reco_branch
print("INFO: initialWeights Name '%s'" % initialWeights_Name)
print("INFO: finalWeights Name '%s'" % finalWeights_Name)

isFiducial_name = "HGamTruthEventInfoAuxDyn.isFiducial"
isDalitz_name = "%s.isDalitz" % args.base_reco_branch
isPassed_name = "%s.isPassed" % args.base_reco_branch

#Define columns for observable values
df = df.Define("isPassed", isPassed_name)\
        .Define("isFiducial", isFiducial_name)\
        .Define("isDalitz", isDalitz_name)\
        .Define("zero", "0.")

#Define columns for weights:
#     - 'w_initial_xsec_br_lumi' is used to weight true histograms;
#     - 'w_final_xsec_br_lumi' is used to weight reco histograms;
#     - 'w_initial' and 'w_final' are used to produce the sum of weights, needed to normalize histograms
df = df.Define("w_initial_xsec_br_lumi", "(float)(%s * %s * %s)" % (initialWeights_Name, "HGamEventInfoAuxDyn.crossSectionBRfilterEff", LUMI))\
       .Define("w_initial", initialWeights_Name)\
       .Define("w_final_xsec_br_lumi", "(float)(%s * %s * %s)" % (finalWeights_Name, "HGamEventInfoAuxDyn.crossSectionBRfilterEff", LUMI))\
       .Define("w_final", finalWeights_Name)

#Define name_variables
for name_variable in pTLib.name_variables:
    df = df.Define("%s" % pTLib.formulas_true[name_variable], "HGamTruthEventInfoAuxDyn.%s" % name_variable)
    if args.verbosity==True:
        print("INFO: '%s' created" % pTLib.formulas_true[name_variable])
for name_variable in pTLib.name_variables:
    df = df.Define("%s" % pTLib.formulas_reco[name_variable], "%s.%s" % (args.base_reco_branch, name_variable))
    if args.verbosity==True:
        print("INFO: '%s' created" % pTLib.formulas_reco[name_variable])

#Apply filters to the dataframe
df_isFiducial = df.Filter("isFiducial==1")
df_isDalitz = df.Filter("isDalitz==1")
df_notDalitz = df.Filter("isDalitz==0")
df_notFiducial = df.Filter("isFiducial==0")
df_isPassed = df.Filter("isPassed==1")
df_isDalitz_isPassed = df_isDalitz.Filter("isPassed==1")
df_isFiducial_notDalitz = df_isFiducial.Filter("isDalitz==0")
df_isFiducial_isPassed = df_isPassed.Filter("isFiducial==1")
df_notFiducial_isPassed = df_notFiducial.Filter("isPassed==1")
df_notFiducial_notDalitz = df_notFiducial.Filter("isDalitz==0")
df_isFiducial_isPassed_notDalitz = df_isFiducial_isPassed.Filter("isDalitz==0")
df_notFiducial_isPassed_notDalitz = df_notFiducial_isPassed.Filter("isDalitz==0")

#Sum of weights histograms 
h_sum_weights_initial_plusDalitz = df.Histo1D(("w_initial_plusDalitz", "w_initial plusDalitz", 1, 0., 1.),
                                             "zero",
                                             "w_initial")
h_sum_weights_initial_notDalitz = df_notDalitz.Histo1D(("w_initial_notDalitz", "w_initial notDalitz", 1, 0., 1.),
                                                      "zero",
                                                       "w_initial")
h_sum_weights_initial_isDalitz = df_isDalitz.Histo1D(("w_initial_isDalitz", "w_initial_isDalitz", 1, 0., 1.),
                                                     "zero",
                                                     "w_initial")
h_sum_weights_final_plusDalitz = df.Histo1D(("w_final_plusDalitz", "w_final_plusDalitz", 1, 0., 1.),
                                            "zero",
                                            "w_final")
h_sum_weights_final_notDalitz = df_notDalitz.Histo1D(("w_final_notDalitz", "w_final_notDalitz", 1, 0., 1.),
                                                     "zero",
                                                     "w_final")
h_sum_weights_final_isDalitz = df_isDalitz.Histo1D(("w_final_isDalitz", "w_final_isDalitz", 1, 0., 1.),
                                                     "zero",
                                                     "w_final")

histoList = {}
histo1d_true__All_notDalitz = {}
histo1d_true__All_isDalitz = {}
histo1d_true__isFiducial_notDalitz = {}
histo1d_true__notFiducial_notDalitz = {}
histo1d_reco__All_isPassed = {}
histo1d_reco__isDalitz_isPassed = {}
histo1d_reco__isFiducial_isPassed = {}
histo1d_reco__notFiducial_isPassed = {}
histo1d_reco__notFiducial_isPassed_notDalitz = {}
histo2d__isFiducial_isPassed_notDalitz = {}
histo2d_m_yy__All_isPassed = {}

for name_variable in pTLib.name_variables:
    if (name_variable == "m_yy"):
        for variable_for_m_yy in pTLib.name_variables:
            if (variable_for_m_yy == "m_yy"):
                continue
            else:
                histo2d_m_yy__All_isPassed[variable_for_m_yy] = df_isPassed.Histo2D(("histo2d_%s_%s__All_isPassed" % (name_variable, variable_for_m_yy),
                                                                                     "All & isPassed plus Dalitz",
                                                                                     len(pTLib.binnings[variable_for_m_yy]) - 1, pTLib.binnings[variable_for_m_yy],
                                                                                     len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                    pTLib.formulas_true[variable_for_m_yy], pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
                histoList['histo2d_m_yy__All_isPassed'] = histo2d_m_yy__All_isPassed
    else:
        #Histos 1D
        histo1d_true__All_notDalitz[name_variable] = df_notDalitz.Histo1D(("histo1d_%s_true__All_notDalitz" % name_variable,
                                                                           "%s true All notDalitz" % name_variable,
                                                                           len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                          "%s" % pTLib.formulas_true[name_variable], "w_initial_xsec_br_lumi")
        histoList['histo1d_true__All_notDalitz'] = histo1d_true__All_notDalitz
        histo1d_true__All_isDalitz[name_variable] = df_isDalitz.Histo1D(("histo1d_%s_true__All_isDalitz" % name_variable,
                                                                         "%s true All isDalitz" % name_variable,
                                                                         len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                        "%s" % pTLib.formulas_true[name_variable], "w_initial_xsec_br_lumi")
        histoList['histo1d_true__All_isDalitz'] = histo1d_true__All_isDalitz
        histo1d_true__isFiducial_notDalitz[name_variable] = df_isFiducial_notDalitz.Histo1D(("histo1d_%s_true__isFiducial_notDalitz" % name_variable,
                                                                                             "%s true isFiducial not Dalitz" % name_variable,
                                                                                             len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                            "%s" % pTLib.formulas_true[name_variable], "w_initial_xsec_br_lumi")
        histoList['histo1d_true__isFiducial_notDalitz'] = histo1d_true__isFiducial_notDalitz
        histo1d_true__notFiducial_notDalitz[name_variable] = df_notFiducial_notDalitz.Histo1D(("histo1d_%s_true__notFiducial_notDalitz" % name_variable,
                                                                                               "%s true notFiducial notDalitz" % name_variable,
                                                                                               len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                              "%s" % pTLib.formulas_true[name_variable], "w_initial_xsec_br_lumi")
        histoList['histo1d_true__notFiducial_notDalitz'] = histo1d_true__notFiducial_notDalitz
        histo1d_reco__isFiducial_isPassed[name_variable] = df_isFiducial_isPassed.Histo1D(("histo1d_%s_reco__isFiducial_isPassed" % name_variable,
                                                                                           "%s reco isFiducial & isPassed" % name_variable,
                                                                                           len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                          "%s" % pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo1d_reco__isFiducial_isPassed'] = histo1d_reco__isFiducial_isPassed
        histo1d_reco__notFiducial_isPassed[name_variable] = df_notFiducial_isPassed.Histo1D(("histo1d_%s_reco__notFiducial_isPassed" % name_variable,
                                                                                             "%s reco notFiducial & isPassed" % name_variable,
                                                                                             len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                            "%s" % pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo1d_reco__notFiducial_isPassed'] = histo1d_reco__notFiducial_isPassed
        histo1d_reco__All_isPassed[name_variable] = df_isPassed.Histo1D(("histo1d_%s_reco__All_isPassed" % name_variable,
                                                                         "%s reco All isPassed" % name_variable,
                                                                         len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                        "%s" % pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo1d_reco__All_isPassed'] = histo1d_reco__All_isPassed
        histo1d_reco__isDalitz_isPassed[name_variable] = df_isDalitz_isPassed.Histo1D(("histo1d_%s_reco__isDalitz_isPassed" % name_variable,
                                                                                       "%s true isDalitz $ isPassed" % name_variable,
                                                                                       len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                      "%s" % pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo1d_reco__isDalitz_isPassed'] = histo1d_reco__isDalitz_isPassed
        histo1d_reco__notFiducial_isPassed_notDalitz[name_variable] = df_notFiducial_isPassed_notDalitz.Histo1D(("histo1d_%s_reco__notFiducial_isPassed_notDalitz" % name_variable,
                                                                                                                 "%s reco notFiducial & isPassed & notDalitz" % name_variable,
                                                                                                                 len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),                                                                                                                                            "%s" % pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo1d_reco__notFiducial_isPassed_notDalitz'] = histo1d_reco__notFiducial_isPassed_notDalitz

        #Histos 2D
        histo2d__isFiducial_isPassed_notDalitz[name_variable] = df_isFiducial_isPassed_notDalitz.Histo2D(("histo2d_%s__isFiducial_isPassed_notDalitz" % name_variable,
                                                                                                          "isFiducial & isPassed not Dalitz",
                                                                                                          len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable],
                                                                                                          len(pTLib.binnings[name_variable]) - 1, pTLib.binnings[name_variable]),
                                                                                                         pTLib.formulas_true[name_variable], pTLib.formulas_reco[name_variable], "w_final_xsec_br_lumi")
        histoList['histo2d__isFiducial_isPassed_notDalitz'] = histo2d__isFiducial_isPassed_notDalitz


for name_variable in pTLib.name_variables:
    if (name_variable == "m_yy"):
        for variable_for_m_yy in pTLib.name_variables:
            if (variable_for_m_yy == "m_yy"):
                continue
            else:
                histoList["histo2d_m_yy__All_isPassed"][variable_for_m_yy].Write()
    else:
        for name_histo in pTLib.name_histos:
            histoList[name_histo][name_variable].Write()


h_sum_weights_initial_plusDalitz.Write()
h_sum_weights_initial_notDalitz.Write()
h_sum_weights_initial_isDalitz.Write()
h_sum_weights_final_plusDalitz.Write()
h_sum_weights_final_notDalitz.Write()
h_sum_weights_final_isDalitz.Write()

                                  

output_file.Close()
