systNames_list = [
    'Nominal',
    'FlavorSysLoose'
    'JetSys'
    'LeptonMETSys',
    'PhotonAllSys1',
    'PhotonAllSys2',
    'PhotonAllSys3',
    'PhotonAllSys4',
    'PhotonSys'
    ]

systNames_dict = {
    'Nominal': 'HGamEventInfoAuxDyn',
    'FlavorSysLoose': [
        'HGamEventInfo_FT_EFF_Eigen_B_0__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_0__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_1__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_1__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_2__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_2__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_3__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_3__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_4__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_4__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_5__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_5__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_6__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_6__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_7__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_7__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_8__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_B_8__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_0__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_0__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_1__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_1__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_2__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_2__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_3__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_C_3__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_0__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_0__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_1__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_1__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_2__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_2__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_3__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_3__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_4__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_4__1upAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_5__1downAuxDyn',
        'HGamEventInfo_FT_EFF_Eigen_Light_5__1upAuxDyn',
        'HGamEventInfo_FT_EFF_extrapolation__1downAuxDyn',
        'HGamEventInfo_FT_EFF_extrapolation__1upAuxDyn',
        'HGamEventInfo_FT_EFF_extrapolation_from_charm__1downAuxDyn',
        'HGamEventInfo_FT_EFF_extrapolation_from_charm__1upAuxDyn'
        ],
    'JetSys': [
        'HGamEventInfo_JET_BJES_Response__1downAuxDyn',
        'HGamEventInfo_JET_BJES_Response__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_1__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_1__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_2__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_2__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_3__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_3__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_4__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_4__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_5__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_5__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_6__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_6__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_7__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_7__1upAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_8restTerm__1downAuxDyn',
        'HGamEventInfo_JET_EffectiveNP_8restTerm__1upAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_Modelling__1downAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_Modelling__1upAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1downAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_highE__1upAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1downAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_negEta__1upAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1downAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_NonClosure_posEta__1upAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_TotalStat__1downAuxDyn',
        'HGamEventInfo_JET_EtaIntercalibration_TotalStat__1upAuxDyn',
        'HGamEventInfo_JET_Flavor_Composition__1downAuxDyn',
        'HGamEventInfo_JET_Flavor_Composition__1upAuxDyn',
        'HGamEventInfo_JET_Flavor_Response__1downAuxDyn',
        'HGamEventInfo_JET_Flavor_Response__1upAuxDyn',
        'HGamEventInfo_JET_JER_DataVsMC__1downAuxDyn',
        'HGamEventInfo_JET_JER_DataVsMC__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_1__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_1__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_2__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_2__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_3__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_3__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_4__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_4__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_5__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_5__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_6__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_6__1upAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1downAuxDyn',
        'HGamEventInfo_JET_JER_EffectiveNP_7restTerm__1upAuxDyn',
        'HGamEventInfo_JET_JvtEfficiency__1downAuxDyn',
        'HGamEventInfo_JET_JvtEfficiency__1upAuxDyn',
        'HGamEventInfo_JET_Pileup_OffsetMu__1downAuxDyn',
        'HGamEventInfo_JET_Pileup_OffsetMu__1upAuxDyn',
        'HGamEventInfo_JET_Pileup_OffsetNPV__1downAuxDyn',
        'HGamEventInfo_JET_Pileup_OffsetNPV__1upAuxDyn',
        'HGamEventInfo_JET_Pileup_PtTerm__1downAuxDyn',
        'HGamEventInfo_JET_Pileup_PtTerm__1upAuxDyn',
        'HGamEventInfo_JET_Pileup_RhoTopology__1downAuxDyn',
        'HGamEventInfo_JET_Pileup_RhoTopology__1upAuxDyn',
        'HGamEventInfo_JET_PunchThrough_MC16__1downAuxDyn',
        'HGamEventInfo_JET_PunchThrough_MC16__1upAuxDyn',
        'HGamEventInfo_JET_SingleParticle_HighPt__1downAuxDyn',
        'HGamEventInfo_JET_SingleParticle_HighPt__1upAuxDyn',
        'HGamEventInfo_JET_fJvtEfficiency__1downAuxDyn',
        'HGamEventInfo_JET_fJvtEfficiency__1upAuxDyn'
        ],
    'LeptonMETSys': [
        'HGamEventInfo_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1downAuxDyn',
        'HGamEventInfo_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1upAuxDyn',
        'HGamEventInfo_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1downAuxDyn',
        'HGamEventInfo_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1upAuxDyn',
        'HGamEventInfo_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1downAuxDyn',
        'HGamEventInfo_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1upAuxDyn',
        'HGamEventInfo_MET_SoftTrk_ResoParaAuxDyn',
        'HGamEventInfo_MET_SoftTrk_ResoPerpAuxDyn',
        'HGamEventInfo_MET_SoftTrk_ScaleDownAuxDyn',
        'HGamEventInfo_MET_SoftTrk_ScaleUpAuxDyn',
        'HGamEventInfo_MUON_EFF_ISO_STAT__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_ISO_STAT__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_ISO_SYS__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_ISO_SYS__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_STAT_LOWPT__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_STAT_LOWPT__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_STAT__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_STAT__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_SYS_LOWPT__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_SYS_LOWPT__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_SYS__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_RECO_SYS__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_TTVA_STAT__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_TTVA_STAT__1upAuxDyn',
        'HGamEventInfo_MUON_EFF_TTVA_SYS__1downAuxDyn',
        'HGamEventInfo_MUON_EFF_TTVA_SYS__1upAuxDyn',
        'HGamEventInfo_MUON_ID__1downAuxDyn',
        'HGamEventInfo_MUON_ID__1upAuxDyn',
        'HGamEventInfo_MUON_MS__1downAuxDyn',
        'HGamEventInfo_MUON_MS__1upAuxDyn',
        'HGamEventInfo_MUON_SAGITTA_RESBIAS__1downAuxDyn',
        'HGamEventInfo_MUON_SAGITTA_RESBIAS__1upAuxDyn',
        'HGamEventInfo_MUON_SAGITTA_RHO__1downAuxDyn',
        'HGamEventInfo_MUON_SAGITTA_RHO__1upAuxDyn',
        'HGamEventInfo_MUON_SCALE__1downAuxDyn',
        'HGamEventInfo_MUON_SCALE__1upAuxDyn'
        ],
    'PhotonAllSys1': [
        'HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALCALO__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALCALO__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALCRYO__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALCRYO__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALGAP__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALGAP__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALIBL__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALIBL__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALID__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALID__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALPP0__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_MATERIALPP0__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_PILEUP__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_PILEUP__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_SAMPLINGTERM__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_SAMPLINGTERM__1upAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_ZSMEARING__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_ZSMEARING__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PEDESTAL__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PEDESTAL__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS_BARREL_B12__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS_BARREL_B12__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN3__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN3__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN4__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN4__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN5__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN5__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN6__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN6__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN7__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN7__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN8__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_PS__ETABIN8__1upAuxDyn'
        ],
    'PhotonAllSys2': [
        'HGamEventInfo_EG_SCALE_AF2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_AF2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_ALL__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_ALL__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_E4SCINTILLATOR__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_G4__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_G4__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_L1GAIN__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_L1GAIN__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_L2GAIN__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_L2GAIN__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARCALIB__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARCALIB__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARCALIB__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARCALIB__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECCALIB__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECCALIB__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECUNCONV__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECUNCONV__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECUNCONV__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARELECUNCONV__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARUNCONVCALIB__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARUNCONVCALIB__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_LARUNCONVCALIB__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_LARUNCONVCALIB__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN3__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN3__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN4__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_S12__ETABIN4__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_TOPOCLUSTER_THRES__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_TOPOCLUSTER_THRES__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_WTOTS1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_WTOTS1__1upAuxDyn'
        ],
    'PhotonAllSys3': [
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN10__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN10__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN11__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN11__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN3__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN3__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN4__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN4__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN5__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN5__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN6__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN6__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN7__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN7__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN8__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN8__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN9__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCALO__ETABIN9__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN3__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATID__ETABIN3__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATPP0__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATPP0__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATPP0__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATPP0__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_ZEESTAT__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_ZEESTAT__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_ZEESYST__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_ZEESYST__1upAuxDyn'
        ],
    'PhotonAllSys4': [
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN0__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN0__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN10__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN10__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN11__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN11__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN1__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN1__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN3__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN3__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN4__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN4__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN5__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN5__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN6__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN6__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN7__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN7__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN8__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN8__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN9__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_MATCRYO__ETABIN9__1upAuxDyn',
        'HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVEFFICIENCY__1downAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVEFFICIENCY__1upAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVFAKERATE__1downAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVFAKERATE__1upAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVRADIUS__1downAuxDyn',
        'HGamEventInfo_PH_SCALE_CONVRADIUS__1upAuxDyn',
        'HGamEventInfo_PH_SCALE_LEAKAGECONV__1downAuxDyn',
        'HGamEventInfo_PH_SCALE_LEAKAGECONV__1upAuxDyn',
        'HGamEventInfo_PH_SCALE_LEAKAGEUNCONV__1downAuxDyn',
        'HGamEventInfo_PH_SCALE_LEAKAGEUNCONV__1upAuxDyn'
        ],
    'PhotonSys': [
        'HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn',
        'HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_AF2__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_AF2__1upAuxDyn',
        'HGamEventInfo_EG_SCALE_ALL__1downAuxDyn',
        'HGamEventInfo_EG_SCALE_ALL__1upAuxDyn',
        'HGamEventInfo_PH_EFF_ID_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_ID_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PH_EFF_ISO_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_ISO_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1downAuxDyn',
        'HGamEventInfo_PH_EFF_TRIGGER_Uncertainty__1upAuxDyn',
        'HGamEventInfo_PRW_DATASF__1downAuxDyn',
        'HGamEventInfo_PRW_DATASF__1upAuxDyn'
        ]
    }
