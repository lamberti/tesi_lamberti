import os
import pickle
from itertools import product
import condorConfig_Analysis_Run__Lib as confLib

common_command = """
universe              = vanilla
executable            = Analysis_Run.py
transfer_input_files  = /afs/cern.ch/user/l/lamberti/tesi_lamberti/unfolding/Analysis_Run__Lib.py
arguments             = $(input_name) --base_reco_branch $(base_reco_branch) --luminosity $(luminosity) --flavourSys $(flavourSys) --prodMode $(prodMode) --output_name $(output_name)
output                = /afs/cern.ch/user/l/lamberti/work/condorLog_Analysis/Analysis_Run.$(ClusterId).$(ProcId).out
error                 = /afs/cern.ch/user/l/lamberti/work/condorLog_Analysis/Analysis_Run.$(ClusterId).$(ProcId).err
log                   = /afs/cern.ch/user/l/lamberti/work/condorLog_Analysis/Analysis_Run.$(ClusterId).log
getenv                = True
request_memory        = 200 MB
initialdir	      = /afs/cern.ch/user/l/lamberti/work/condorOutput_Analysis

+JobFlavour = "espresso"

"""

f = open('bookFile_dict.pkl', 'rb')
all_files_info = pickle.load(f)
condor_files = {}

for mc, sys in product(confLib.mcNames_list, confLib.systNames_list + ['Nominal']):
    cfile = condor_files[(mc, sys)] = open('run_Analysis_Run.%s.%s.condor' % (mc, sys), 'w')
    cfile.write(common_command)
    cfile.write('queue input_name,base_reco_branch,luminosity,flavourSys,prodMode,output_name from (\n')

FLAVOUR_FOR_NOMINAL = confLib.systNames_list[2]

for finfo in all_files_info:
    mc, prod, mxAOD_flavour, fname = finfo
    if mc not in confLib.mcNames_list:
        print("skippping (mc) %s" % str(finfo))
        continue
    if prod not in confLib.productionMode_list:
        print("skipping (prod) %s" % str(finfo))
        continue
    if mxAOD_flavour not in confLib.systNames_list:
        print("skipping (sys) %s" % str(finfo))
        continue
    if (fname=="/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16e/PhotonAllSys4/mc16e.PowhegPy8_ZH125J.MxAODPhotonAllSys4.e5743_s3126_r10724_p3665.h024.root/mc16e.PowhegPy8_ZH125J.MxAODPhotonAllSys4.e5743_s3126_r10724_p3665.h024.002.root"):
        print("BRROKEN FILE: %s" % fname)
        continue

    luminosity = confLib.luminosities[mc]

    cfile = condor_files[(mc, mxAOD_flavour)]    
    for sysbranch in confLib.systNames_dict[mxAOD_flavour]:
        output_fn = os.path.splitext(os.path.split(fname)[1])[0] + ("_sys%s.root" % sysbranch)
        line = '{file_path}, {sysbranch}, {lumi}, {flavour}, {prod}, {output_fn}\n'.format(file_path=fname,
                                                                                sysbranch=sysbranch,
                                                                                lumi=luminosity,
                                                                                flavour=mxAOD_flavour,
                                                                                prod=prod,
                                                                                output_fn=output_fn)     
        cfile.write(line)

    # nominal
    cfile = condor_files[(mc, 'Nominal')]
    if mxAOD_flavour == FLAVOUR_FOR_NOMINAL:
        output_fn = os.path.splitext(os.path.split(fname)[1])[0] + "_sysNominal.root"
        line = '{file_path}, {sysbranch}, {lumi}, {flavour}, {prod}, {output_fn}\n'.format(file_path=fname,
                                                                                sysbranch='HGamEventInfoAuxDyn',
                                                                                lumi=luminosity,
                                                                                flavour=mxAOD_flavour,
                                                                                prod=prod,
                                                                                output_fn=output_fn)       
        cfile.write(line)

for f in condor_files.values():
    f.write(')\n')
