import ROOT
import os
from glob import glob

BASEDIR = "merged"
BASEDIR_NORM = "merged_normalized"
BR = 2.270E-3

def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

def normalize_file(fname):
    f = ROOT.TFile.Open(fname)
    fname_for_output = fname.split("merged/")[1]
    print("INFO: Normalizing %s" % fname_for_output)
    f_output = ROOT.TFile.Open(os.path.join(BASEDIR_NORM, fname_for_output.split(".root")[0] + ".norm.root"), "RECREATE")
    
    w_initial_notDalitz = f.Get("w_initial_notDalitz").GetBinContent(1)
    w_initial_plusDalitz = f.Get('w_initial_plusDalitz').GetBinContent(1)
    w_initial_isDalitz = w_initial_plusDalitz - w_initial_notDalitz

    if (w_initial_isDalitz == 0):
        print("WARNING: %s doesn't have any Dalitz event" % fname_for_output) 
        pass
    else:
        BR_isDalitz = (w_initial_isDalitz/w_initial_notDalitz) * BR
    
        for m_yy_histo in f.GetListOfKeys()[:6]:
            histo = m_yy_histo.ReadObj()
            histo.Write()

        for histo_name in f.GetListOfKeys()[6:-6]: #CHECK: if also m_yy plots need to be normalized, f.GetListOfKeys()[:-6]
            histo = histo_name.ReadObj()
            histo_norm = histo.Clone(histo.GetName() + '_norm')
            if "isDalitz" in histo.GetName():
                histo_norm.Scale(BR_isDalitz / (w_initial_isDalitz * BR))
            else:
                histo_norm.Scale(1. / w_initial_notDalitz)
            histo_norm.Write()

            for weight_name in f.GetListOfKeys()[-6:]:
                histo = weight_name.ReadObj()
                histo.Write()


create_folder_if_not_exists(BASEDIR_NORM)

for fname in glob(os.path.join(BASEDIR, "*.root")):
    normalize_file(fname)
    
print("INFO: Normalization performed")
    
    
