import papermill as pm
import os
import argparse

import sys
sys.path.append('..')
from common import common

def create_dir_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


parser = argparse.ArgumentParser(description='Loop notebook over all quantities')
parser.add_argument('--output-dir', default="../output", help="directory where there is the quickFit directory with all the fit results, e.g. ../output")
args = parser.parse_args()
        
output_dir = '../output/POI_plots-papermill-notebooks'
create_dir_if_not_exists(output_dir)

for quantity in common.quantities:
    for method in 'binbybin', 'matrix':
        for dataset in 'AsimovSB', 'combDatabinned':
            print('Running notebook for quantity {quantity} method {method} dataset {dataset}'.format(quantity=quantity,
                                                                                                      method=method,
                                                                                                      dataset=dataset))
            output_fn = os.path.join(output_dir, 'workspace_POI_plots_{quantity}_{method}_{dataset}.ipynb'.format(quantity=quantity,
                                                                                                      method=method,
                                                                                                      dataset=dataset))
            pm.execute_notebook('workspace_POI_plots.ipynb',
                                output_fn,
                                parameters=dict(quantity=quantity,
                                                dataset=dataset,
                                                method=method,
                                                workspace_folder=os.path.join(args.output_dir, "quickFit_{method}/{quantity}".format(method=method, quantity=quantity))))
            
            
