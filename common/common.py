import sys
from array import array

if sys.version_info.major == 2:
    from itertools import tee, izip

    def pairwise(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return izip(a, b)

else:
    from itertools import tee

    def pairwise(iterable):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        a, b = tee(iterable)
        next(b, None)
        return zip(a, b)

luminosity_pb = {'mc16a': 36215,
                 'mc16d': 44307,
                 'mc16e': 58450,
                 'mcAll': 138972}

Br = 2.270E-3

mc_samples = list(luminosity_pb.keys())

productions = ['FlavorSysLoose', 'JetSys', 'LeptonMETSys', 'PhotonAllSys1', 'PhotonAllSys2', 'PhotonAllSys3', 'PhotonAllSys4', 'PhotonSys']

quantities = ['pT_yy', 'yAbs_yy', 'Dphi_j_j_30_signed', 'pT_j1_30', 'N_j_30', 'm_jj_30']

binnings = {
    'pT_yy': array('f', [0, 5E3, 10E3, 15E3, 20E3, 25E3, 30E3, 35E3, 45E3, 60E3, 80E3, 100E3, 120E3, 140E3, 170E3, 200E3, 250E3, 350E3]), #MeV
    'yAbs_yy': array('f', [0, 0.15, 0.30, 0.45, 0.60, 0.75, 0.90, 1.2, 1.6, 2.4]),
    'Dphi_j_j_30_signed': array('f', [-3.15, -1.57, 0, 1.57, 3.15]),
    'pT_j1_30': array('f', [0, 30E3, 60E3, 90E3, 120E3, 350E3]), #MeV
    'N_j_30': array('f', [0, 1, 2, 3]),
    'm_jj_30': array('f', [0, 170E3, 500E3, 1500E3]) #MeV
    }

nbins_noextraflow = {k: len(binnings[k]) - 1 for k in binnings}

unities_root_file =  {'pT_yy': 'MeV',
                      'yAbs_yy': None,
                      'Dphi_j_j_30_signed': None,
                      'pT_j1_30': 'MeV',
                      'N_j_30': None,
                      'm_jj_30': 'MeV'}


latex_labels = [r'$p_{T}^{\gamma\gamma}$ [GeV]',
                r'$|y_{\gamma\gamma}|$',
                r'$\Delta\phi_{jj}^{(30)}$',
                r'$p_{T}^{j1}$ [GeV]',
                r'$N_j^{(30)}$',
                r'$m_{jj}^{(30)}$ [GeV]']
latex_labels = {q: v for q, v in zip(quantities, latex_labels)}

show_overflows = (True, False, False, True, True, True)
show_overflows =  {q: v for q, v in zip(quantities, show_overflows)}
show_underflows = (False, False, False, False, False, False)
show_underflows =  {q: v for q, v in zip(quantities, show_underflows)}
do_normalizations = (True, True, True, True, False, True)
do_normalizations = {q: v for q, v in zip(quantities, do_normalizations)}
do_logs_x = (True, False, False, True, False, False)
do_logs_x = {q: v for q, v in zip(quantities, do_logs_x)}


pT_yy_binning = ["[%d - %d)" % (a / 1000., b/ 1000.) for a, b in pairwise(binnings['pT_yy'])]
yAbs_yy_binning = ["[%.1f - %.1f)" % (a, b) for a, b in pairwise(binnings['yAbs_yy'])]#'$0.15$', '$0.30$', '$0.45$', '$0.60$', '$0.75$', '$0.90$', '$1.2$', '$1.6$', '$2.4$']
Dphi_j_j_30_signed_binning = ["[%.2f - %.2f)" % (a, b) for a, b in pairwise(binnings['Dphi_j_j_30_signed'])]#'$-3.15$', '$-1.57$', '$0$', '$1.57$', '$3.15$']
pT_j1_30_binning = ["[%d - %d)" % (a / 1000, b / 1000) for a, b in pairwise(binnings['pT_j1_30'])]#'$30E3$', '$60E3$', '$90E3$', '$120E3$', '$350E3$']
N_j_30_binning = ["%d" % a for a in binnings['N_j_30'][:-1]]
m_jj_30_binning = ["[%d - %d)" % (a / 1000., b / 1000.) for a, b in pairwise(binnings['m_jj_30'])]#'$170E3$', '$500E3$', '$1500E3$']

bin_labels = (pT_yy_binning, yAbs_yy_binning, Dphi_j_j_30_signed_binning, pT_j1_30_binning, N_j_30_binning, m_jj_30_binning)
bin_labels = {q: v for q, v in zip(quantities, bin_labels)}


pT_yy_theor_errors = [0.07, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.07, 0.08, 0.09, 0.1, 0.12, 0.13, 0.14, 0.15, 0.16]
yAbs_yy_theor_errors = [0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05]
Dphi_j_j_30_signed_theor_errors = [0.11, 0.14, 0.14, 0.11]
pT_j1_30_theor_errors = [0.06, 0.12, 0.11, 0.13, 0.19]
N_j_30_theor_errors = [0.06, 0.12, 0.2, 0.21]
m_jj_30_theor_errors = [0.3, 0.2, 0.18]

theoretical_relat_errors = (pT_yy_theor_errors, yAbs_yy_theor_errors, Dphi_j_j_30_signed_theor_errors, pT_j1_30_theor_errors, N_j_30_theor_errors, m_jj_30_theor_errors)
theoretical_relat_errors = {q: v for q, v in zip(quantities, theoretical_relat_errors)}
