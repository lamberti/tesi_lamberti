import os
import ROOT
import argparse
import logging

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser(description='create .sh script for analysis')
parser.add_argument('quantity', help='quantity to study')
parser.add_argument('--bins', help='number of bins for quantity')
parser.add_argument('--method', help='binbybin/matrix')
parser.add_argument('--fixNP', default=0)
args = parser.parse_args()

def create_folder_if_not_exists(name_dir):
  if not os.path.exists(name_dir):
    os.system('mkdir %s' % name_dir)

def loop_iterator(iterator):
  object = iterator.Next()
  while object:
    yield object
    object = iterator.Next()

def iter_collection(rooAbsCollection):
  iterator = rooAbsCollection.createIterator()
  return loop_iterator(iterator)

def get_NP_list_from_RooArgSet_selection(root_file, name_selection):
    NP_list = []
    f = ROOT.TFile.Open(root_file)
    ws = f.Get('combWS')
    RooArgSet_selection = ws.allVars().selectByName(name_selection)
    for item in iter_collection(RooArgSet_selection):
        NP_list.append("%s" % item.GetName())
    return NP_list
    

output_name = '%s' % args.quantity
create_folder_if_not_exists(output_name)

root_fname_blinded = '../output/%s_unfolding/workspace_%s_%s_blinded/workspace_%s_%s_blinded/XSectionWS_%s.root' % (args.method, args.method, args.quantity, args.method, args.quantity, args.quantity)
root_fname_unblinded = '../output/%s_unfolding/workspace_%s_%s_unblinded/workspace_%s_%s_unblinded/XSectionWS_%s.root' % (args.method, args.method, args.quantity, args.method, args.quantity, args.quantity)
logging.info('Open file for BLINDED analysis: %s' % root_fname_blinded)
logging.info('Open file for UNBLINDED analysis: %s' % root_fname_unblinded)

NP_list = get_NP_list_from_RooArgSet_selection(root_fname_blinded, "ATLAS*")
NP_list_for_quickFit = ','.join(NP_list)
logging.info('Nuisance Parameters list: %s' % NP_list_for_quickFit)

bins_array = []
for nbin in range(1, int(args.bins)+1):
    bins_array.append('mu_xsec_%s_%d' % (args.quantity, nbin))
mu_list = ','.join(bins_array)
logging.info('POI list: %s' % mu_list)

f = open('%s_analysis.sh' % args.quantity, 'w+')
f.write('#!/bin/bash \n') 
f.write('\n')

if args.fixNP == 0:
    f.write('quickFit -f %s -w combWS -d AsimovSB -p %s --saveWS 1 -o %s/XSectionWS_%s-fitted_AsimovSB.root' % (root_fname_blinded, mu_list, args.quantity, output_name))
elif args.fixNP == "all":
    f.write('quickFit -f %s -w combWS -d AsimovSB -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_statONLY-fitted_AsimovSB.root' % (root_fname_blinded, mu_list, NP_list_for_quickFit, output_name, args.quantity))
elif args.fixNP == 'scan':
    for NP_elem in NP_list:
        logging.info('Scanning Nuisance Parameter for BLINDED analysis: %s' % NP_elem)
        f.write('quickFit -f %s -w combWS -d AsimovSB -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_fixed_%s-fitted_AsimovSB.root' % (root_fname_blinded, mu_list, NP_elem, output_name, args.quantity, NP_elem))
        f.write('\n')
        f.write('\n')
else:
    f.write('quickFit -f %s -w combWS -d AsimovSB -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_fixed_%s-fitted_AsimovSB.root' % (root_fname_blinded, mu_list, args.fixNP, output_name, args.quantity, args.fixNP))

f.write('\n')
f.write('\n')

if args.fixNP == 0:
    f.write('quickFit -f %s -w combWS -d combDatabinned -p %s --saveWS 1 -o %s/XSectionWS_%s-fitted_combDatabinned.root' % (root_fname_unblinded, mu_list, args.quantity, output_name))
elif args.fixNP =="all":
    f.write('quickFit -f %s -w combWS -d combDatabinned -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_statONLY-fitted_combDatabinned.root' % (root_fname_unblinded, mu_list, NP_list_for_quickFit, output_name, args.quantity))
elif args.fixNP == 'scan':
    for NP_elem in NP_list:
        logging.info('Scanning Nuisance Parameter for UNBLINDED analysis: %s' % NP_elem)
        f.write('quickFit -f %s -w combWS -d combDatabinned -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_fixed_%s-fitted_combDatabinned.root' % (root_fname_unblinded, mu_list, NP_elem, output_name, args.quantity, NP_elem))
        f.write('\n')
        f.write('\n')
else:
    f.write('quickFit -f %s -w combWS -d combDatabinned -p %s --fixNP %s --saveWS 1 -o %s/XSectionWS_%s_fixed_%s-fitted_combDatabinned.root' % (root_fname_unblinded, mu_list, args.fixNP, output_name, args.quantity, args.fixNP))

os.system('chmod u+x %s_analysis.sh' % args.quantity)
