import os
import ROOT
from glob import glob
import logging

logging.basicConfig(level=logging.DEBUG)

def create_folder_if_not_exists(name_dir):
  if not os.path.exists(os.path.join(output_basedir,name_dir)):
    os.makedirs(os.path.join(output_basedir, name_dir))

def loop_iterator(iterator):
  object = iterator.Next()
  while object:
    yield object
    object = iterator.Next()

def iter_collection(rooAbsCollection):
  iterator = rooAbsCollection.createIterator()
  return loop_iterator(iterator)

def get_NP_list(ws):
  np_list = ws.allVars().selectByName("ATLAS*")
  return [s.GetName() for s in iter_collection(np_list)]

def get_poi_list(ws):
  return [s.GetName() for s in iter_collection(ws.obj('ModelConfig').GetParametersOfInterest()) if 'xsec' in s.GetName()]


def fit_workspace(ws_fname, pois, data_name, output_fname, fix_np=None):
  command = 'quickFit -f {file_name} -w combWS -d {dataset} -p {POI} --saveWS 1 -o {output_file}'.format(
      file_name=ws_fname, POI=','.join(pois), dataset=data_name, output_file=output_fname)
  if fix_np is not None:
    command += " --fixNP {NP}".format(NP=','.join(fix_np))

  logging.info("running command %s", command)
  os.system(command)

def loop(quantities, output_basedir, do_syst_loop):
  for quantity in quantities:
    output_basedir_quantity = os.path.join(output_basedir, quantity)
    create_folder_if_not_exists(output_basedir_quantity)
    
    for blinding in 'blinded', 'unblinded':
      ws_fname = os.path.join('../make_workspace/{method}_method/workspace_{quantity}_{blind}/workspace_{quantity}_{blind}/XSectionWS_{quantity}.root').format(
        quantity=quantity, blind=blinding, method=args.method)
      logging.info('Fitting for {blind} analysis: {file_name}'.format(blind=blinding, file_name=ws_fname))

      f = ROOT.TFile.Open(ws_fname)
      ws = f.Get('combWS')

      NP_list = get_NP_list(ws) 
      logging.info('Nuisance Parameters list: %s' % NP_list)

      mus = get_poi_list(ws)
      logging.info('POI list: %s' % mus)

      data_name = {'blinded': 'AsimovSB', 'unblinded': 'combDatabinned'}[blinding]

      # full error
      logging.info("fitting full error %s, %s", quantity, blinding)
      output_ws_name = 'XSectionWS_{quantity}-fitted_{dataset}.root'.format(quantity=quantity, dataset=data_name)
      output_ws_fname = os.path.join(output_basedir_quantity, output_ws_name)
      fit_workspace(ws_fname, mus, data_name, output_ws_fname)

      # stat only
      logging.info("fitting stat-only error %s, %s", quantity, blinding)
      output_ws_name = 'XSectionWS_{quantity}-statonly-fitted_{dataset}.root'.format(quantity=quantity, dataset=data_name)
      output_ws_fname = os.path.join(output_basedir_quantity, output_ws_name)
      fit_workspace(ws_fname, mus, data_name, output_ws_fname, fix_np=NP_list)

      # full error minus one
      if do_syst_loop:
        for NP_elem in NP_list:
          logging.info("fitting full minus %s %s, %s", NP_elem, quantity, blinding)
          output_ws_name = 'XSectionWS_{quantity}_fixed_{NP}-fitted_{data_name}.root'.format(quantity=quantity,
                                                                                             data_name=data_name,
                                                                                             NP=NP_elem)
          output_ws_fname = os.path.join(output_basedir_quantity, output_ws_name)
          fit_workspace(ws_fname, mus, data_name, output_ws_fname, [NP_elem])


if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser(description='fit over all systematic variations')
  parser.add_argument('method', choices=('binbybin', 'matrix'), help='binbybin/matrix')  
  parser.add_argument('--quantity')
  parser.add_argument('--no-sys-loop', action='store_true')
  args = parser.parse_args()

  import sys
  sys.path.append('..')
  from common import common

  quantities = common.quantities
  if args.quantity:
    quantities = [args.quantity]
  output_basedir = '../output'

  output_basedir_method = os.path.join(output_basedir, 'quickFit_{method}'.format(method=args.method))
  create_folder_if_not_exists(output_basedir_method)

  loop(quantities, output_basedir_method, not args.no_sys_loop)


