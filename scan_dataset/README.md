This script read the data mxAOD file and create the txt files with the myy values needed to create the workspace. Each txt file correspond to an input file, a quantity (observable) and a bin.

You need to setup HGam framework first.

Run interactively
=================
To run it interactively (all runs for data15, few minutes to run):

    ./read_data.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data15/runs/data15_13TeV*.root --split-folders

with `--split-folders` sub-directories are created. All the output will be in the `output` directory.

Run via condor
==============
To run via condor (it will run 20 file for each job):

    python condorConfig_read_data.py
    condor_submit run_read_data.condor

By default when running with condor data are blinded.

Merging
=======

    python merging.py output
