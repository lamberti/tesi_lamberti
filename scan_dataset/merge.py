import argparse
import sys
from glob import glob
import logging
import os
from itertools import groupby
sys.path.append("../unfolding")
from Analysis_Run__Lib import binnings

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser(description="merge txt files")

parser.add_argument('directory')
parser.add_argument("--quantities", nargs='*', help="quantities name")
args = parser.parse_args()

all_observables_list = [
    'pT_yy',
    'yAbs_yy',
    'Dphi_j_j_30_signed',
    'pT_j1_30',
    'N_j_30',
    'm_jj_30'
]

if args.quantities is None:
    logging.info("observables not speficied: using all of them: %s" % ','.join(all_observables_list))
    args.quantities = all_observables_list
logging.info("quantities: %s" % args.quantities)

all_files = glob("{}/**/*.txt".format(args.directory))

for k, v in groupby(sorted(all_files), lambda x: os.path.basename(x).split("__")[0]):
    output_filename = k + '.txt'
    logging.info("merging %s", output_filename)
    command = "cat %s > %s" % (' '.join(v), output_filename)
    print(command)
    os.system(command)



