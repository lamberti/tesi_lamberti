from glob import glob
from itertools import islice

def grouper(n, iterable):
    it = iter(iterable)
    return iter(lambda: tuple(islice(it, n)), ())


condor_command = """
universe              = vanilla
executable            = read_data.py
arguments             = --split-folders --blind $(files)
transfer_input_files  = ../unfolding/Analysis_Run__Lib.py
transfer_output_files = output
output                = read_data.$(ClusterId).$(ProcId).out
error                 = read_data.$(ClusterId).$(ProcId).err
log                   = read_data.$(ClusterId).log
getenv                = True
+MaxRuntime           = 800
request_memory        = 100MB

queue files from (
"""

grouping = 20

all_root_files = glob("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data*/runs/*.root")
print("found %d root_files" % len(all_root_files))

for i, files in enumerate(grouper(grouping, all_root_files), 1):
    condor_command += ' '.join(files) + '\n'

print("created %d processes" % i)

condor_file = open("run_read_data.condor", "w")
condor_file.write(condor_command)

condor_file.write(")\n")
