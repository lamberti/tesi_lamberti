#!/bin/env python

import ROOT
from array import array
import os
import numpy as np
import argparse
from argparse import RawTextHelpFormatter
import sys
import logging
import datetime
sys.path.append("../unfolding")
from Analysis_Run__Lib import binnings

logging.basicConfig(level=logging.DEBUG)


def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def read_file(fname, all_binnings, n_events=None, output_folder='output', split_directories=False, blind=False):

    quantities = list(all_binnings.keys())

    all_out_files = {}
    create_folder_if_not_exists(output_folder)
    for quantity in quantities:
        folder = output_folder
        if split_directories:
            folder = os.path.join(output_folder, quantity)
            create_folder_if_not_exists(folder)
        nedges = len(all_binnings[quantity])

        # include file for underflow and overflow
        for ibin in range(0, nedges + 1):
            output_filename = "mass_points_%s_bin%d__%s.txt" % (quantity, ibin, os.path.splitext(os.path.basename(fname))[0])
            output_filename = os.path.join(folder, output_filename)
            all_out_files[(quantity, ibin)] = open(output_filename, 'w')

    logging.info("number of empty txt file created: %d" % len(all_out_files))

    f = ROOT.TFile.Open(fname)
    tree = f.Get("CollectionTree")
    tree.SetBranchStatus("*", False)  # disable all branches


    m_yy_value = array('f', [0])
    isPassed_value = array('b', [0])

    m_yy_branch = "HGamEventInfoAuxDyn.m_yy"
    isPassed_branch = "HGamEventInfoAuxDyn.isPassed"

    tree.SetBranchStatus(m_yy_branch, True)
    tree.SetBranchStatus(isPassed_branch, True)

    tree.SetBranchAddress(m_yy_branch, m_yy_value)
    tree.SetBranchAddress(isPassed_branch, isPassed_value)

    quantities_value = {}
    
    for q in quantities:
        quantity_branch = "HGamEventInfoAuxDyn.%s" % q
        root_type = tree.GetBranch(quantity_branch).GetTitle().split("/")[1].strip()
        quantity_type = {'F': 'f', 'I': 'l'}[root_type]
        quantities_value[q] = array(quantity_type, [0])

        tree.SetBranchStatus(quantity_branch, True)
        tree.SetBranchAddress(quantity_branch, quantities_value[q])

    if n_events is None:
        n_events = tree.GetEntries()

    logging.info("events to be processed: %d" % n_events)

    start = datetime.datetime.now()
    for i in range(n_events):
        if i % 5000 == 0 and i != 0:
            speed = i / (datetime.datetime.now() - start).total_seconds()
            eta = datetime.timedelta(seconds=(n_events - i) / speed)
            logging.info("event processed: %d (%.0f evt/s eta: %s)", i, speed, eta)
        tree.GetEvent(i)
        if blind:
            if (120E3 < m_yy_value[0] < 130E3):
                continue
        if not isPassed_value[0]:
            continue
        for q in quantities:
            ibin = int(np.digitize(quantities_value[q][0], all_binnings[q]))
            all_out_files[(q, ibin)].write("%s\n" % (m_yy_value[0] / 1000))

    logging.info("all events processed")

    return n_events

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="scan data for workspace",
                                     formatter_class=RawTextHelpFormatter,
                                     epilog="Example: ./read_data.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/data15/runs/data15_13TeV.00276262.physics_Main.MxAODDetailed.p3704.h024.root")

    parser.add_argument('filenames', nargs='+')
    parser.add_argument("--quantities", nargs='*', help="quantities name")
    parser.add_argument("--output-folder", default='output')
    parser.add_argument("--split-folders", action="store_true")
    parser.add_argument("--nevents", type=int)
    parser.add_argument("--blind", action="store_true")
    args = parser.parse_args()

    all_observables_list = [
        'pT_yy',
        'yAbs_yy',
        'Dphi_j_j_30_signed',
        'pT_j1_30',
        'N_j_30',
        'm_jj_30'
    ]

    if args.quantities is None:
        logging.info("observables not speficied: using all of them: %s" % ','.join(all_observables_list))
        args.quantities = all_observables_list
    logging.info("quantities: %s" % args.quantities)

    all_binnings = {quantity: binnings[quantity] for quantity in args.quantities}

    for quantity in all_binnings:
        nedges = len(all_binnings[quantity])
        logging.info("quantity %s binning (%d): %s", quantity, nedges - 1, list(all_binnings[quantity]))

    nprocessed = 0
    start = datetime.datetime.now()
    for i, filename in enumerate(args.filenames, 1):
        logging.info("runnig on file (%d/%d): %s", i, len(args.filenames), filename)

        try:
            nprocessed += read_file(filename,
                                    all_binnings,
                                    args.nevents,
                                    args.output_folder,
                                    args.split_folders,
                                    args.blind
                                )
        except KeyboardInterrupt:
            logging.warning("interrupted by the user")
            break

    logging.info("process nevents %d (%f evt/sec)", nprocessed, nprocessed / (datetime.datetime.now() - start).total_seconds())

