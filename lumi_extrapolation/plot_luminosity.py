from matplotlib import pyplot as plt
from glob import glob
import re
import os
import argparse
import ROOT
import pandas as pd
import sys
sys.path.append('..')
from common import common

def loop_iterator(iterator):
  object = iterator.Next()
  while object:
    yield object
    object = iterator.Next()

def iter_collection(rooAbsCollection):
  iterator = rooAbsCollection.createIterator()
  return loop_iterator(iterator)

def get_data(folder):
    all_fn = glob(os.path.join(folder, '*fitresult.root'))
    r_lumi = re.compile(r"_lumi([0-9\.]+)\.root")
    results = []
    for fn in all_fn:
        m = r_lumi.search(fn)
        if not m:
            raise ValueError("cannot understand file %s with regex %s" % (fn, r_lumi.pattern))
        try:
            lumi_value = float(m.group(1))
        except ValueError:
            print("cannot understand luminosity from file %f" % fn)
            raise

        f = ROOT.TFile.Open(fn)
        fr = f.Get("fitResult")
        roofit_vars = fr.floatParsFinal().selectByName("mu_xsec*")
        for v in iter_collection(roofit_vars):
            results.append([lumi_value, v.GetName(), v.getVal(), v.getError()])

    df = pd.DataFrame(results, columns=['lumi', 'bin_name', 'value', 'error'])
    df['bin_number'] = df['bin_name'].str.extract(r'_([0-9]+)').astype(int)
    df['quantity'] = df['bin_name'].str.extract(r'mu_xsec_(.+?)_[0-9]+$')

    if len(df['quantity'].unique()) > 1:
        raise ValueError('more than one quantity found: %s', df['quantity'].unique())

    return df


parser = argparse.ArgumentParser(description='Read fit result and produce plot')
parser.add_argument('directory')
parser.add_argument('directory_stat_only', nargs='?', default=None)
parser.add_argument('--bin', type=int, nargs='*')
args = parser.parse_args()
print(args)

df = get_data(args.directory)
df_statonly = None
if args.directory_stat_only:
    df_statonly = get_data(args.directory_stat_only)

quantity = df['quantity'].unique()[0]

fig, ax = plt.subplots()
for bin_number, values in df.groupby('bin_number'):
    if args.bin is not None and bin_number not in args.bin:
        continue
    values = values.sort_values('lumi')
    ax.plot(values['lumi'] / 1000., values['error'], '-o', markersize=2, label=('bin %d' % bin_number))

plt.gca().set_prop_cycle(None)

if df_statonly is not None:
    for bin_number, values in df_statonly.groupby('bin_number'):
        if args.bin is not None and bin_number not in args.bin:
            continue
        values = values.sort_values('lumi')
        ax.plot(values['lumi'] / 1000., values['error'], '--o', markersize=2, label=('bin %d stat-only' % bin_number))


ax.set_title(r'%s' % common.latex_labels[quantity].replace('[GeV]', ''))
ax.legend(ncol=int(len(df['bin_number'].unique()) / 3), loc='upper center', bbox_to_anchor=(0.5, -0.2))
ax.set_ylabel('relative error')
ax.set_xlabel('luminosity [$fb^{-1}$]')
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlim(60, None)
ax.axvline(3000, color='k', ls=':')
ax.axvline(139, color='k', ls=':')
ax.annotate('HL-LHC', xy=(3000, 0.8), xycoords=('data', 'axes fraction'), rotation=90, ha='right')
ax.annotate('Run2-LHC', xy=(139, 0.15), xycoords=('data', 'axes fraction'), rotation=90, ha='right')
fig.savefig('{quantity}_HI_LUMI_LHC_extrapolation.png'.format(quantity=quantity), bbox_inches='tight')
fig.savefig('{quantity}_HI_LUMI_LHC_extrapolation.pdf'.format(quantity=quantity), bbox_inches='tight')

