import ROOT
import os
import argparse

def loop_iterator(iterator):
  object = iterator.Next()
  while object:
    yield object
    object = iterator.Next()

def iter_collection(rooAbsCollection):
  iterator = rooAbsCollection.createIterator()
  return loop_iterator(iterator)


def create_new_workspace(fn, lumi_value, output_name):
    f = ROOT.TFile.Open(fn)
    ws = f.Get('combWS')

    lumi_var = ws.var("lumi_run2")
    lumi_var.setVal(lumi_value)

    model_config = ws.obj("ModelConfig")
    new_asimov = ROOT.RooStats.AsymptoticCalculator.GenerateAsimovData(model_config.GetPdf(), model_config.GetObservables())
    getattr(ws, 'import')(new_asimov, ROOT.RooFit.Rename("AsimovSB_HL"))

    ws.writeToFile(output_name)

parser = argparse.ArgumentParser(description='Run fit with a scan on the luminosity')

parser.add_argument("workspace")
parser.add_argument("min_luminosity", type=float)
parser.add_argument("max_luminosity", type=float)
parser.add_argument("steps_luminosity", type=int)
parser.add_argument("--stat-only", action='store_true')
parser.add_argument("--output-directory", default='.')
parser.add_argument("--simulate", action='store_true', help='do not execute the fit, just write the command')
args = parser.parse_args()

if args.output_directory != ".":
    if not os.path.exists(args.output_directory):
        os.makedirs(args.output_directory)


f = ROOT.TFile.Open(args.workspace)
ws = f.Get('combWS')

pois_to_fit = ws.obj("ModelConfig").GetParametersOfInterest().selectByName("mu_xsec*")
pois_to_fit = [p.GetName() for p in iter_collection(pois_to_fit)]

all_np_to_fix = []
if args.stat_only:
  all_np_to_fix = ws.obj("ModelConfig").GetNuisanceParameters().selectByName("ATLAS*")
  all_np_to_fix = [n.GetName() for n in iter_collection(all_np_to_fix)]

step = (args.max_luminosity - args.min_luminosity) / (args.steps_luminosity - 1)
for istep in range(args.steps_luminosity):
    new_lumi = args.min_luminosity + istep * step
    fname_output = os.path.splitext(os.path.basename(args.workspace))[0] + "_lumi%f" % new_lumi + '.root'
    fname_output = os.path.join(args.output_directory, fname_output)
    create_new_workspace(args.workspace, new_lumi, fname_output)

    fn_fit_result = os.path.basename(fname_output) + "_fitresult.root"
    fn_fit_result = os.path.join(args.output_directory, fn_fit_result)

    command = "quickFit -f {ws} -w combWS -d AsimovSB_HL -p {pois} -o {output}".format(
        ws=fname_output, pois=','.join(pois_to_fit),
        output=fn_fit_result)
    if all_np_to_fix:
      command += " --fixNP {NP}".format(NP=','.join(all_np_to_fix))

    if args.simulate:
      print(command)
    else:
      os.system(command)

    



