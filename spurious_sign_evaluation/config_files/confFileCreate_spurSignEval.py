#!/usr/bin/env python
import argparse
from configparser import ConfigParser
import numpy as np
import os

import sys
sys.path.append('../..')
from common import common

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('quantity')
args = parser.parse_args()

def create_dir_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

observable = args.quantity
do_underflow = common.show_underflows[observable]
do_overflow = common.show_overflows[observable]

not_in_templates = 'Dphi_j_j_30_signed', 'm_jj_30'

if observable in not_in_templates:
    print('WARNING: Template for "{quantity}" is not present, no config_files will be created'.format(quantity=observable))
    exit()

if do_underflow and not do_overflow:
    bins_range = range(len(common.binnings[observable]))
if not do_underflow and do_overflow:
    bins_range = range(1, len(common.binnings[observable])+1)
if not do_underflow and not do_overflow:
    bins_range = range(1, len(common.binnings[observable]))

template_obs_name = {
    'pT_yy': 'ptH',
    'yAbs_yy': 'yH',
    'N_j_30': 'njets',
    'pT_j1_30': 'ptj1'
    }
    
for nbin in bins_range:

    output_basedir = '%s' % observable
    create_dir_if_not_exists(output_basedir)
    OUTPUT_FILE = os.path.join(output_basedir, 'config_file_%s_bin%s.h024.config' % (observable, nbin))

    config = ConfigParser()

    config['Histograms'] = {
        'Histo.data': 'hyydataSR_%s_bin%s_nominal_All' % (template_obs_name[observable], nbin),
        'Histo.mc': 'hyymcSR_%s_bin%s_nominal_All' % (template_obs_name[observable], nbin)
    }

    config['Parameters'] = {
        'Param.path': '../output/signalParametrisation/%s/%s_bin_%d/resonance_paramList.txt' % (observable, observable, nbin),
        'Dataset.path': '../data_template/bkg.h024.root'
    }

    config['Settings'] = {
        'Observable.name': 'mass',
        'Observable.min': 105,
        'Observable.max': 160,
        'Range.min': 121,
        'Range.max': 129,
        'Range.scan': 0.5,
        'Bins.number': 220,
        #'Plot.perPoint' inputs:
        #   On
        #   Off
        'Plots.perPoint': 'Off',
        #Normalization.method inputs:
        #   from SideBands: normalization from data SideBands
        #   from MonteCarlo: histo_mc.Integral()
        'Normalization.method': 'from MonteCarlo',
        #'Asimov.choice' inputs:
        #   direct: just rdh
        #   via_rdhPDF: rdh -> rdhpdf -> asimov
        'Asimov.choice': 'direct'
    }

    config['bkgFunctionsPDFs'] = {
        'Names.list': ['exp',
                       'expPoly2',
                       #'Bern3',
                       #'Bern4',
                       #'Bern5',
                       #'dijet',
                       'pow'
        ]
    }

    config['exp'] = {
        'PDF.type': 'EXPR',
        'PDF.name': 'exp',
        'PDF.expression': 'exp(-@0 * @1)',
        'PDF.parameters': 'par1_bkg_exp[0.1,0.001,1]'
    }

    config['expPoly2'] = {
        'PDF.type': 'EXPR',
        'PDF.name': 'expPoly2',
        'PDF.expression': 'exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))',
        'PDF.parameters': 'par0_Poly2[0.1,-10,10], par1_Poly2[0.1,-10,10]'
    }

    config['Bern3'] = {
        'PDF.type': 'RooBernstein',
        'PDF.name': 'Bern3',
        'PDF.parameters': 'par1_Bern3[0.001,-10,10], par2_Bern3[0.001,-10,10], par3_Bern3[0.001,-10,10]'
    }

    config['Bern4'] = {
        'PDF.type': 'RooBernstein',
        'PDF.name': 'Bern4',
        'PDF.parameters': 'par1_Bern4[5,0.001,10], par2_Bern4[2.5,0.001,5], par3_Bern4[2.5,0.001,5], par4_Bern4[2.5,0.001,5]'
    }

    config['Bern5'] = {
        'PDF.type': 'RooBernstein',
        'PDF.name': 'Bern5',
        'PDF.parameters': 'par1_Bern5[5,0.001,10], par2_Bern5[2.5,0.001,5], par3_Bern5[2.5,0.001,5], par4_Bern5[2.5,0.001,5], par5_Bern5[2.5,0.001,5]'
    }

    config['pow'] = {
        'PDF.type': 'EXPR',
        'PDF.name': 'pow',
        'PDF.expression': 'pow(@0, @1)',
        'PDF.parameters': 'par1_pow[0.1,-10,10]'
    }

    config['dijet'] = {
        'PDF.type': 'EXPR',
        'PDF.name': 'dijet',
        'PDF.expression': 'pow(@0/13E3, @1)*pow(1 - @0/13E3, @2)',
        'PDF.parameters': 'par1_dijet[-5,-10,-0.1], par2_dijet[15,10,20]'
    }


    with open('./{output_file_name}'.format(output_file_name=OUTPUT_FILE), 'w') as f:
        config.write(f)

print('INFO: %d config_files created' % np.max(bins_range))
