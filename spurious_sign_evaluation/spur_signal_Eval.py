#!/usr/bin/env python
import os
import sys
sys.path.append('../libraries')

import ROOT
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm as tqdm
import definitions as defn
import functions as fnc
import smoothing as smt
import warnings
import ast
from configparser import ConfigParser

#Usage Instructions
if len(sys.argv) != 3 :
    sys.exit('Usage: ./spur_signal_Eval.py <config_files/config_file.config> <output_dir>')

#Remove Warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)
ROOT.gErrorIgnoreLevel = 1001
ROOT.gROOT.SetBatch()

def create_output_folder(name, bkgList, plotsPerPoint):
    if not os.path.exists('%s' % name):
        os.mkdir('%s' % name)
    for bkg_name in bkgList:
        if not os.path.exists('%s/%s' % (name, bkg_name)):
            os.mkdir('%s/%s' % (name, bkg_name))
        if not os.path.exists('%s/%s/BkgOnly' % (name, bkg_name)):
            os.mkdir('%s/%s/BkgOnly' % (name, bkg_name))
        if (plotsPerPoint == 'On'):
            if not os.path.exists('%s/%s/plotsPerPoint' % (name, bkg_name)):
                os.mkdir('%s/%s/plotsPerPoint' % (name, bkg_name))

def create_workspace(ws_name, bkgList, observable_name, observable_min, observable_max, n_bins, config_file):
    parser = ConfigParser()
    parser.read(config_file)
    ws = ROOT.RooWorkspace(ws_name)
    mass = ws.factory("%s[%s, %s]" % (observable_name, observable_min, observable_max))
    mass.setBins(n_bins)

    #Create Bkg Functions
    for bkg_name in bkgList:
        type = parser[bkg_name]['PDF.type'].encode('ascii')
        name = parser[bkg_name]['PDF.name'].encode('ascii')

        if(type == 'RooBernstein'):
            parameters = parser[bkg_name]['PDF.parameters'].encode('ascii')
            ws.factory('%s::bkg_%s(mass, {%s, 1})' % (type, name, parameters))
            print("INFO: Setting up PDF '%s'" % name)

        if(type == 'EXPR'):
            expression = parser[bkg_name]['PDF.expression'].encode('ascii')
            parameters = parser[bkg_name]['PDF.parameters'].encode('ascii')
            ws.factory('%s::bkg_%s("%s", mass, %s)' % (type, name, expression, parameters))
            print("INFO: Setting up PDF '%s'" % name)

    #Create Signal
    ws.factory("mH[125, 110, 150]")
    ws.factory('mu_CB125[125]')
    ws.factory('expr:mu_CB("@0 - 125 + @1", mu_CB125, mH)')
    ws.factory('sigma_CB125[1]')
    ws.factory('alpha_low_CB125[1]')
    ws.factory('alpha_hi_CB125[1]')
    ws.factory('n_low_CB125[10]')
    ws.factory('n_hi_CB125[10]')
    ws.factory('HggTwoSidedCBPdf::signal(%s, mu_CB, sigma_CB125, alpha_low_CB125, n_low_CB125, alpha_hi_CB125, n_hi_CB125)' % observable_name)
    ws.obj('mH').setConstant()

    #Create Signal + Bkg PDFs
    for bkg_name in bkgList:
        name = parser[bkg_name]['PDF.name'].encode('ascii')
        ws.factory('SUM:model_sb_bkg_%s(nsig[0, -10000, 10000] * signal, nbkg[150E3, 1, 1E6] * bkg_%s)' % (name, name))

    #Save General Workspace
    ws.SaveAs('%s/workspace.root' % sys.argv[2])

    #Save BkgOnly Workspaces for each Bkg Function
    for bkg_name in bkgList:
        ws_bkgOnly = ROOT.RooWorkspace('%sWorkspace' % bkg_name)
        mass = ws_bkgOnly.factory("%s[%s, %s]" % (observable_name, observable_min, observable_max))
        mass.setBins(n_bins)


        type = parser[bkg_name]['PDF.type'].encode('ascii')
        name = parser[bkg_name]['PDF.name'].encode('ascii')

        if(type == 'RooBernstein'):
            parameters = parser[bkg_name]['PDF.parameters'].encode('ascii')
            ws_bkgOnly.factory('%s::bkg_%s(mass, {%s, 1})' % (type, name, parameters))
            ws_bkgOnly.SaveAs(' %s/%s/BkgOnly/%s_Workspace.root' % (sys.argv[2], bkg_name, bkg_name))

        if(type == 'EXPR'):
            expression = parser[bkg_name]['PDF.expression'].encode('ascii')
            parameters = parser[bkg_name]['PDF.parameters'].encode('ascii')
            ws.factory('%s::bkg_%s("%s", mass, %s)' % (type, name, expression, parameters))
            ws_bkgOnly.SaveAs(' %s/%s/BkgOnly/%s_Workspace.root' % (sys.argv[2], bkg_name, bkg_name))

    return ws

def bkg_norm_from_datasb(observable_name, observable_min, observable_max, n_bins, histo_data, histo_mc):

    ws = ROOT.RooWorkspace()
    mass = ws.factory("%s[%s, %s]" % (observable_name, observable_min, observable_max))
    mass.setBins(n_bins)
    mass.setRange('left', 105, 121)
    mass.setRange('right', 128, 160)

    b = histo_data.GetArray()
    b.SetSize(histo_data.GetNbinsX() + 2)
    b = np.array(b)
    histo_entries = b[b>0].sum()

    #Fit from SideBands
    rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(ws.obj('mass')), histo_data)
    ws.factory('EXPR::bkg_exp("exp(-@0 * @1)", mass, par0_bkg_exp[0.1, 0.001, 1])')
    ws.factory('SUM:model(nbkg[%s, %s, %s] * bkg_exp)' % (histo_entries, histo_entries / 5., histo_entries * 3))

    fr = ws.obj('model').fitTo(rdh,
			       ROOT.RooFit.Range("left, right"),
			       ROOT.RooFit.PrintLevel(-1),
			       ROOT.RooFit.Save())

    nbkg_from_datasb = ws.obj('nbkg').getVal()

    return nbkg_from_datasb

def spur_sign(ws, n_bins, bkgList, histo_mc, bkg_norm, parameters, plotsPerPoint, asimov_choice, save_all_plots=True):

    #Define all variables
    all_spur_sign_True = {}
    all_spur_sign_False = {}
    all_spur_sign_err_True = {}
    all_spur_sign_err_False = {}
    all_ss_delta_ss_True = {}
    all_ss_delta_ss_False = {}
    all_ss_s_ref_True = {}
    all_ss_s_ref_False = {}

    #Define CB_parameters
    mu_CB = parameters.loc['mu_CB_Nom']['value']
    sigma_CB = parameters.loc['sigma_CB_Nom']['value']
    alpha_low_CB = parameters.loc['alpha_CB_Lo']['value']
    alpha_hi_CB = parameters.loc['alpha_CB_Hi']['value']
    n_low_CB = parameters.loc['n_CB_Lo']['value']
    n_hi_CB = parameters.loc['n_CB_Hi']['value']
    yield_CB = parameters.loc['yield_CB']['value']

    #Define signal shape
    ws.obj('mu_CB125').setVal(mu_CB)
    ws.obj('sigma_CB125').setVal(sigma_CB)
    ws.obj('alpha_low_CB125').setVal(alpha_low_CB)
    ws.obj('alpha_hi_CB125').setVal(alpha_hi_CB)
    ws.obj('n_low_CB125').setVal(n_low_CB)
    ws.obj('n_hi_CB125').setVal(n_hi_CB)
    ws.obj('mH').setConstant()
    ws.obj('mass').setBins(n_bins)

    ws.saveSnapshot('initial', ws.allVars())

    #Choose Creation Method for Asimov
    print("INFO: Asimov choice '%s'" % asimov_choice)
    if(asimov_choice == 'direct'):
        ws.loadSnapshot('initial')
        ws.obj('nbkg').setVal(bkg_norm)
        data_asimov = ROOT.RooDataHist('root_data_hist', 'root_data_hist',
                                ROOT.RooArgList(ws.obj('mass')),
                                histo_mc, 1)

    if(asimov_choice == 'via_rdhPDF'):
        rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist',
                                ROOT.RooArgList(ws.obj('mass')),
                                histo_mc, 1)
        rhpdf = ROOT.RooHistPdf('root_hist_pdf',
                                'root_hist_pdf',
                                ROOT.RooArgSet(ws.obj('mass')),
                                rdh)

        ws.loadSnapshot('initial')
        ws.obj('nbkg').setVal(bkg_norm)

        data_asimov = rhpdf.generateBinned(ROOT.RooArgSet(ws.obj('mass')),
                                        ROOT.RooFit.NumEvents(bkg_norm),
                                        ROOT.RooFit.ExpectedData())

    for bkg_name in tqdm(bkgList):
        #BkgOnly Fit
        ws.loadSnapshot('initial')
        ws.obj('nbkg').setVal(bkg_norm)
        nSignal = ws.obj('nsig').getVal()
        ws.obj('nsig').setConstant(True)
        ws.obj('nsig').setVal(0)

        pdf_bkgOnly = ws.obj('model_sb_bkg_%s' % bkg_name)
        fr_bkgOnly = pdf_bkgOnly.fitTo(data_asimov,
                                ROOT.RooFit.Minimizer("Minuit2", "migrad"),
                                ROOT.RooFit.Save(True),
                                ROOT.RooFit.Offset(),
                                ROOT.RooFit.PrintLevel(-1),
                                ROOT.RooFit.SumW2Error(False))

        ws.obj('nsig').setConstant(False)
        ws.obj('nsig').setVal(nSignal)

        ws.saveSnapshot('after_bkgOnly_fit', ws.allVars())

        #Save Fit Results and Fit Plot for each Bkg Function (NO Signal injected)
        fr_bkgOnly.SaveAs('%s/%s/BkgOnly/fitResults.root' % (sys.argv[2], bkg_name))

        canvas = ROOT.TCanvas()
        pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
        pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0, 1.0, 0.3)
        pad1.Draw()
        pad2.Draw()

        pad1.cd()
        frame = ws.obj('mass').frame(ROOT.RooFit.Title(" "))
        data_asimov.plotOn(frame, ROOT.RooFit.Name('data_asimov'))
        pdf_bkgOnly.plotOn(frame, ROOT.RooFit.Name('pdf_bkgOnly'), ROOT.RooFit.LineColor(ROOT.kRed-4))
        txt = ROOT.TLatex(0.5, 0.7, r'\sqrt{s} = 13 TeV: L = 140 fb^{-1}')
        txt.SetNDC()
        txt.SetTextSize(0.045)
        frame.GetXaxis().SetTitle("m_{#gamma#gamma} [GeV]")
        frame.addObject(txt)
        frame.Draw()

        curve_pdf = frame.findObject('pdf_bkgOnly')
        histo_data = frame.findObject('data_asimov')
        xs = histo_data.GetX()
        xs.SetSize(histo_data.GetN())
        xs = list(xs)
        ys = histo_data.GetY()
        ys.SetSize(histo_data.GetN())
        ys = list(ys)
        eys_high = histo_data.GetEYhigh()
        eys_high.SetSize(histo_data.GetN())
        eys_high = list(eys_high)
        eys_low = histo_data.GetEYlow()
        eys_low.SetSize(histo_data.GetN())
        eys_low = list(eys_low)


        ratios = [y / curve_pdf.Eval(x) - 1 for (x, y) in zip(xs, ys)]
        ratio_errors = [0.5 * (ey_high + ey_low) / curve_pdf.Eval(x) for (x, ey_high, ey_low) in zip(xs, eys_high, eys_low)]

        pad2.cd()
        gr = ROOT.TGraphErrors()
        for i in range(len(xs)):
            gr.SetPoint(i, xs[i], ratios[i])
            gr.SetPointError(i, 0, ratio_errors[i])

        gr.SetMarkerStyle(20)
        gr.SetMarkerSize(0.4)
        gr.GetXaxis().SetRangeUser(105, 160)
        gr.Draw("AP0")

        canvas.SaveAs('%s/%s/BkgOnly/fitPlot.pdf' % (sys.argv[2], bkg_name))

        for mH_test in all_test_mH:
            pdf =  ws.obj('model_sb_bkg_%s' % bkg_name)

            #Signal + Bkg Fit [SumW2Error: False]
            ws.loadSnapshot('after_bkgOnly_fit')
            ws.obj('mH').setVal(mH_test)
            ws.obj('mH').setConstant(True)

            fr_False = pdf.fitTo(data_asimov,
                                    ROOT.RooFit.Minimizer("Minuit2", "migrad"),
                                    ROOT.RooFit.Save(True),
                                    ROOT.RooFit.Offset(),
                                    ROOT.RooFit.Extended(),
                                    ROOT.RooFit.PrintLevel(-1),
                                    ROOT.RooFit.SumW2Error(False))

            if fr_False.status() != 0:
                print("ERROR: Problem with fit mH=%f, function=%s, status=%d [SumW2Error: False]" % (mH_test, bkg_name, fr_False.status()))

            all_spur_sign_False[(bkg_name, mH_test)] = ws.obj('nsig').getVal()
            all_spur_sign_err_False[(bkg_name, mH_test)] = ws.obj('nsig').getError()
            all_ss_delta_ss_False[(bkg_name, mH_test)] = all_spur_sign_False[(bkg_name, mH_test)] / all_spur_sign_err_False[(bkg_name, mH_test)]
            all_ss_s_ref_False[(bkg_name, mH_test)] = all_spur_sign_False[(bkg_name, mH_test)] / yield_CB

            #Save Fit Plot for all Scan Points
            if(plotsPerPoint == 'On'):
                os.mkdir('%s/%s/plotsPerPoint/mH_%.2f' % (sys.argv[2], bkg_name, ws.obj('mH').getVal()))
                if save_all_plots:
                    canvas = ROOT.TCanvas()
                    pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
                    pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0, 1.0, 0.3)
                    pad1.Draw()
                    pad2.Draw()

                    pad1.cd()
                    frame = ws.obj('mass').frame()
                    data_asimov.plotOn(frame, ROOT.RooFit.Name('data_asimov'))
                    pdf.plotOn(frame, ROOT.RooFit.Name('pdf'))
                    frame.Draw()

                    curve_pdf = frame.findObject('pdf')
                    histo_data = frame.findObject('data_asimov')
                    xs = histo_data.GetX()
                    xs.SetSize(histo_data.GetN())
                    xs = list(xs)
                    ys = histo_data.GetY()
                    ys.SetSize(histo_data.GetN())
                    ys = list(ys)
                    eys_high = histo_data.GetEYhigh()
                    eys_high.SetSize(histo_data.GetN())
                    eys_high = list(eys_high)
                    eys_low = histo_data.GetEYlow()
                    eys_low.SetSize(histo_data.GetN())
                    eys_low = list(eys_low)


                    ratios = [y / curve_pdf.Eval(x) - 1 for (x, y) in zip(xs, ys)]
                    ratio_errors = [0.5 * (ey_high + ey_low) / curve_pdf.Eval(x) for (x, ey_high, ey_low) in zip(xs, eys_high, eys_low)]

                    pad2.cd()
                    gr = ROOT.TGraphErrors()
                    for i in range(len(xs)):
                        gr.SetPoint(i, xs[i], ratios[i])
                        gr.SetPointError(i, 0, ratio_errors[i])

                    gr.SetMarkerStyle(20)
                    gr.SetMarkerSize(0.4)
                    gr.GetXaxis().SetRangeUser(105, 160)
                    gr.Draw("AP0")

                    canvas.SaveAs("%s/%s/plotsPerPoint/mH_%.2f/%s_fitPlot_false_mH%.2f.pdf" % (sys.argv[2], bkg_name, ws.obj('mH').getVal(), bkg_name, mH_test))
                    results_file = ROOT.TFile("%s/%s/plotsPerPoint/mH_%.2f/fits_results_mH%.2f.root" % (sys.argv[2], bkg_name, ws.obj('mH').getVal(), ws.obj('mH').getVal()), 'recreate')
                    fr_False.Write('fitResult_False')

            #Signal + Bkg Fit [SumW2Error: True]
            #ws.loadSnapshot('after_bkgOnly_fit')

            #ws.obj('mH').setVal(mH_test)
            #ws.obj('mH').getVal()
            #ws.obj('mH').setConstant(True)

            fr_True = pdf.fitTo(data_asimov,
                                ROOT.RooFit.Minimizer("Minuit2", "migrad"),
                                ROOT.RooFit.Save(),
                                ROOT.RooFit.Offset(),
                                ROOT.RooFit.Extended(),
                                ROOT.RooFit.PrintLevel(-1),
                                ROOT.RooFit.SumW2Error(True))

            if fr_True.status() != 0:
                print("ERROR: Problem with fit mH=%f, function=%s, status=%d [SumW2Error: True]" % (mH_test, bkg_name, fr_True.status()))

            all_spur_sign_True[(bkg_name, mH_test)] = ws.obj('nsig').getVal()
            all_spur_sign_err_True[(bkg_name, mH_test)] = ws.obj('nsig').getError()
            all_ss_delta_ss_True[(bkg_name, mH_test)] = all_spur_sign_True[(bkg_name, mH_test)] / all_spur_sign_err_True[(bkg_name, mH_test)]
            all_ss_s_ref_True[(bkg_name, mH_test)] = all_spur_sign_True[(bkg_name, mH_test)] / yield_CB

            #Save Fit Plot for all Scan Points
            if(plotsPerPoint == 'On'):
                #os.mkdir('%s/%s/plotsPerPoint/mH_%.2f' % (sys.argv[2], bkg_name, ws.obj('mH').getVal()))
                if save_all_plots:
                    canvas = ROOT.TCanvas()
                    pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
                    pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0, 1.0, 0.3)
                    pad1.Draw()
                    pad2.Draw()

                    pad1.cd()
                    frame = ws.obj('mass').frame()
                    data_asimov.plotOn(frame, ROOT.RooFit.Name('data_asimov'))
                    pdf.plotOn(frame, ROOT.RooFit.Name('pdf'))
                    frame.Draw()

                    curve_pdf = frame.findObject('pdf')
                    histo_data = frame.findObject('data_asimov')
                    xs = histo_data.GetX()
                    xs.SetSize(histo_data.GetN())
                    xs = list(xs)
                    ys = histo_data.GetY()
                    ys.SetSize(histo_data.GetN())
                    ys = list(ys)
                    eys_high = histo_data.GetEYhigh()
                    eys_high.SetSize(histo_data.GetN())
                    eys_high = list(eys_high)
                    eys_low = histo_data.GetEYlow()
                    eys_low.SetSize(histo_data.GetN())
                    eys_low = list(eys_low)


                    ratios = [y / curve_pdf.Eval(x) - 1 for (x, y) in zip(xs, ys)]
                    ratio_errors = [0.5 * (ey_high + ey_low) / curve_pdf.Eval(x) for (x, ey_high, ey_low) in zip(xs, eys_high, eys_low)]

                    pad2.cd()
                    gr = ROOT.TGraphErrors()
                    for i in range(len(xs)):
                        gr.SetPoint(i, xs[i], ratios[i])
                        gr.SetPointError(i, 0, ratio_errors[i])

                    gr.SetMarkerStyle(20)
                    gr.SetMarkerSize(0.4)
                    gr.GetXaxis().SetRangeUser(105, 160)
                    gr.Draw("AP0")

                    canvas.SaveAs("%s/%s/plotsPerPoint/mH_%.2f/%s_fitPlot_true_mH%.2f.pdf" % (sys.argv[2], bkg_name, ws.obj('mH').getVal(), bkg_name, mH_test))
                    fr_True.Write('fitResult_True')

            #Fixed Parameters Fit
            param_set = ws.obj('bkg_%s' % bkg_name).getParameters(ROOT.RooArgSet())
            param_list = ROOT.RooArgList(param_set)
            nParam = len(param_list)
            for i in range(0, nParam):
                param_list[i].setConstant()
            fr_fixedParam = pdf.fitTo(data_asimov,
                                        ROOT.RooFit.Minimizer("Minuit2", "migrad"),
                                        ROOT.RooFit.Save(),
                                        ROOT.RooFit.Offset(),
                                        ROOT.RooFit.Extended(),
                                        ROOT.RooFit.PrintLevel(-1),
                                        ROOT.RooFit.SumW2Error(True))

            if fr_fixedParam.status() != 0:
                print("ERROR: Problem with fit mH=%f, function=%s, status=%d [SumW2Error: True]" % (mH_test, bkg_name, fr_fixedParam.status()))

            #Save Fit Plot for all Scan Points
            if(plotsPerPoint == 'On'):
                #os.mkdir('%s/%s/plotsPerPoint/mH_%.2f' % (sys.argv[2], bkg_name, ws.obj('mH').getVal()))
                fr_fixedParam.Write('fitResult_fixedParam')
                results_file.Close()
            for i in range(0, nParam):
                param_list[i].setConstant(False)

            df_spurious_True = pd.Series(all_spur_sign_True)
            df_spurious_False = pd.Series(all_spur_sign_False)
            df_err_True = pd.Series(all_spur_sign_err_True)
            df_err_False = pd.Series(all_spur_sign_err_False)
            df_ss_delta_ss_True = pd.Series(all_ss_delta_ss_True)
            df_ss_delta_ss_False = pd.Series(all_ss_delta_ss_False)
            df_ss_s_ref_True = pd.Series(all_ss_s_ref_True)
            df_ss_s_ref_False = pd.Series(all_ss_s_ref_False)

    df_all = pd.DataFrame({'ss[True]': df_spurious_True,
                        'ss[False]': df_spurious_False,
                        'ss_err[True]': df_err_True,
                        'ss_err[False]': df_err_False,
                        'ss_delta_ss[True]': df_ss_delta_ss_True,
                        'ss_delta_ss[False]': df_ss_delta_ss_False,
                        'ss_s_ref[True]': df_ss_s_ref_True,
                        'ss_s_ref[False]': df_ss_s_ref_False})

    return df_all


def best_bkg_Eval(mH_values, bkgList, observable_name, observable_min, observable_max, dataHistoName, mcHistoName, \
                    paramPath, plotsPerPoint, normalization_method, n_bins, asimovChoice):

    #Define all Variables
    max_array = {}
    sign_max_array = {}
    best_bkgs = {}
    best_values = {}
    best_signs = {}
    all_mH_max_ss = {}
    n_param_array = {}
    max_ss_delta_ss_array = {}
    max_ss_s_ref_array = {}
    pass_fail_array = {}


    #Define Scan Range
    all_test_mH = mH_values

    #Creating List of Parameters
    all_dfs_cat = {}

    for i in range(7):
        param = pd.read_csv(paramPath, sep=' ', index_col=0, header=None)[1][i]
        all_dfs_cat[defn.parameters_names[i]] = param
	all_dfs = pd.Series(all_dfs_cat)
	df_parameters = pd.DataFrame({'value' :all_dfs})
	df_parameters.index.names = ['parameters']
    print("INFO: CB_Parameters array created")

    #Bkg Normalization
    dataHisto = f.Get('%s' % dataHistoName)
    print('INFO: data histogram file: %s' % dataHisto)
    mcHisto = f.Get('%s' % mcHistoName)
    print('INFO: MonteCarlo histogram file: %s' % mcHisto)

    if(normalization_method == 'from SideBands'):
        bkg_norm = bkg_norm_from_datasb(observable_name, observable_min, observable_max, n_bins, dataHisto, mcHisto)
    if(normalization_method == 'from MonteCarlo'):
        bkg_norm = mcHisto.Integral()

    print("INFO: Bkg normalization %s" % normalization_method)
    print("INFO: Plots per point '%s'" % plotsPerPoint)

    #General DataFrame Creation
    print("INFO: Performing analysis")
    spur_sign_array = {}

    df_spur_sign = spur_sign(ws, n_bins, bkgList, mcHisto, bkg_norm, df_parameters, plotsPerPoint, asimovChoice)
    df_spur_sign.index.names = ['bkg_name', 'mH']

    #Max Value for Spurious Signal and Error for each Bin
    for bkg_name in bkgList:

        spur_abs = np.abs(df_spur_sign.loc[bkg_name].unstack('mH')['ss[True]'])
        mH_max_spurious_signal = spur_abs.idxmax()
        all_mH_max_ss[bkg_name] = mH_max_spurious_signal
        #sign_max_array[bkg_name] = np.sign(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss[True]'])
        max_array[bkg_name] = '%.1f' % np.abs(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss[True]'])
        max_ss_delta_ss_array[bkg_name] = '%.1f' % np.multiply(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss_delta_ss[False]'], 100)
        max_ss_s_ref_array[bkg_name] = '%.1f' % np.multiply(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss_s_ref[True]'], 100)

        if np.abs(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss_delta_ss[False]']) < 0.2 \
                                        or np.abs(df_spur_sign.loc[bkg_name, mH_max_spurious_signal]['ss_s_ref[True]']) < 0.1:
            pass_fail_array[bkg_name] = 'PASS'
        else:
            pass_fail_array[bkg_name] = 'FAIL'

        #Get Number of Parameters for each Bkg Function
        for bkg_name in bkgList:
            param_list = ws.obj('bkg_%s' % bkg_name).getParameters(ROOT.RooArgSet())
            param_list.remove(ws.obj('mass'))
            n_param_array[bkg_name] = len(param_list)

    df_max = pd.DataFrame({'|max(S)|' : max_array,
                            #'sign_max' : sign_max_array,
                            'mH': all_mH_max_ss,
                            'nParam': n_param_array,
                            'max(S/deltaS)[False] [%]': max_ss_delta_ss_array,
                            'max(S/Sref)[True] [%]': max_ss_s_ref_array,
                            'Result': pass_fail_array})
    df_max = df_max.reindex(['mH', 'max(S/deltaS)[False] [%]', 'max(S/Sref)[True] [%]', '|max(S)|', 'nParam', 'Result'], axis=1)
    df_max.index.names = ['bkg_name']
    df_max = df_max.sort_values(by=['nParam'])

    #Save Plots
    print('INFO: Writing files on disk')
    fig, ax = plt.subplots()
    df_spur_sign.unstack('bkg_name')['ss[True]'].plot(ax=ax)
    ax.set_ylabel('Spurious Signal')
    fig.savefig('%s/total_nSignal.pdf' % sys.argv[2])
    plt.close(fig)

    fig, ax = plt.subplots()
    df_spur_sign.unstack('bkg_name')['ss_delta_ss[False]'].plot(ax=ax)
    ax.set_ylabel('S / $\delta$ S')
    ax.axhline(y=0.2, color='r', linestyle='--', alpha=0.6)
    ax.axhline(y=-0.2, color='r', linestyle='--', alpha=0.6)
    fig.savefig('%s/total_Z.pdf' % sys.argv[2])
    plt.close(fig)

    fig, ax = plt.subplots()
    df_spur_sign.unstack('bkg_name')['ss_s_ref[True]'].plot(ax=ax)
    ax.set_ylabel('$S / S_{REF}$')
    ax.axhline(y=0.1, color='r', linestyle='--', alpha=0.6)
    ax.axhline(y=-0.1, color='r', linestyle='--', alpha=0.6)
    fig.savefig('%s/total_Mu.pdf' % sys.argv[2])
    plt.close(fig)

    for bkg_name in tqdm(bkgList):

        fig, ax = plt.subplots()
    	ax.plot(all_test_mH, df_spur_sign.loc[bkg_name].unstack('mH')['ss[True]'], label = '%s' % bkg_name)
    	ax.fill_between(all_test_mH,
                        df_spur_sign.loc[bkg_name].unstack('mH')['ss[True]'] - df_spur_sign.loc[bkg_name].unstack('mH')['ss_err[True]'],
                        df_spur_sign.loc[bkg_name].unstack('mH')['ss[True]'] + df_spur_sign.loc[bkg_name].unstack('mH')['ss_err[True]'],
                        alpha=0.3)
    	ax.set_xlabel('$m_{H}$')
    	ax.set_ylabel('Spurious Signal')
    	fig.savefig('%s/%s/%s_nSignal.pdf' % (sys.argv[2], bkg_name, bkg_name))
    	plt.close(fig)

    	fig, ax = plt.subplots()
    	ax.plot(all_test_mH, df_spur_sign.loc[bkg_name].unstack('mH')['ss_delta_ss[False]'])
        ax.axhline(y=0.2, color='r', linestyle='--', alpha=0.6)
    	ax.axhline(y=-0.2, color='r', linestyle='--', alpha=0.6)
    	ax.set_xlabel('$m_{H}$')
    	ax.set_ylabel('S / $\\delta$ S')
    	fig.savefig('%s/%s/%s_Z.pdf' % (sys.argv[2], bkg_name, bkg_name))
        plt.close(fig)

    	fig, ax = plt.subplots()
    	ax.plot(all_test_mH, df_spur_sign.loc[bkg_name].unstack('mH')['ss_s_ref[True]'])
    	ax.axhline(y=0.1, color='r', linestyle='--', alpha=0.6)
    	ax.axhline(y=-0.1, color='r', linestyle='--', alpha=0.6)
        ax.set_xlabel('$m_{H}$')
        ax.set_ylabel('$S / S_{REF}$')
        fig.savefig('%s/%s/%s_Mu.pdf' % (sys.argv[2], bkg_name, bkg_name))
        plt.close(fig)

    return df_max


if __name__ == '__main__':
    from configparser import ConfigParser

    parser = ConfigParser()
    parser.read(sys.argv[1])
    bkgList = ast.literal_eval(parser.get('bkgFunctionsPDFs', 'Names.list'))

    #Open Templates File
    fname = parser['Parameters']['Dataset.path']
    f = ROOT.TFile.Open(fname)

    create_output_folder(sys.argv[2],
                            bkgList,
                            parser['Settings']['Plots.perPoint'].encode('ascii'))
    ws = create_workspace('workspace',
                            bkgList,
                            parser['Settings']['Observable.name'],
                            parser['Settings']['Observable.min'],
                            parser['Settings']['Observable.max'],
                            parser.getint('Settings', 'Bins.number'),
                            sys.argv[1])
    print('INFO: Workspace generated')
    all_test_mH = np.arange(parser.getfloat('Settings', 'Range.min'),
                            parser.getfloat('Settings', 'Range.max') + 0.01,
                            parser.getfloat('Settings', 'Range.scan'))
    print('INFO: Histograms divided in %s bins' % parser.getint('Settings', 'Bins.number'))
    print('INFO: Will perform fits in range [%s, %s] in steps of %s' % (parser.getfloat('Settings', 'Range.min'),
                                                                    parser.getfloat('Settings', 'Range.max'),
                                                                    parser.getfloat('Settings', 'Range.scan')))

    df_max = best_bkg_Eval(all_test_mH,
                            bkgList,
                            parser['Settings']['Observable.name'],
                            parser['Settings']['Observable.min'],
                            parser['Settings']['Observable.max'],
                            parser['Histograms']['Histo.data'].encode('ascii'),
                            parser['Histograms']['Histo.mc'].encode('ascii'),
                            parser['Parameters']['Param.path'].encode('ascii'),
                            parser['Settings']['Plots.perPoint'].encode('ascii'),
                            parser['Settings']['Normalization.method'].encode('ascii'),
                            parser.getint('Settings', 'Bins.number'),
                            parser['Settings']['Asimov.choice'].encode('ascii'))
    print('')
    print('== Final Results ==')

    df_max = df_max.sort_values(by=['Result', 'nParam'], ascending=[False, True])
    print(df_max)

    if(df_max.iloc[0]['Result'] == 'PASS'):
        print('--> PDF %s selected with Max(ss) = %s' % (df_max.index[0], df_max.iloc[0]["|max(S)|"]))
        #print("best category, function = %s, ss = %s" % (df_max.iloc[0].index, df_max.iloc[0]["max(S)"]))

        df_max.to_csv("%s/bkg_results_dataframe.txt" % sys.argv[2], header=True, index=True, sep="\t")
        #headers= "mH\tmax(S/deltaS)[False] [%]\tmax(S/Sref)[True] [%]\t|max(S)|\tnParam\tResult"
        #np.savetxt("%s/bkg_results_dataframe.txt" % sys.argv[2], df_max.values, fmt="%d", delimiter="\t", header=headers)
    else:
        print('--> No PDF passes the Selection')
        #sort_index = np.argsort(df_max["|max(S)|"])
        #df_max = df_max.reindex(sort_index)
        df_max = df_max.sort_values(by=["|max(S)|"], ascending=False)
        #print("best category, function = %s, ss = %s" % (df_max.iloc[0].index, df_max.iloc[0]["max(S)"]))

        df_max.to_csv("%s/bkg_results_dataframe.txt" % sys.argv[2], header=True, index=True, sep="\t")
        #headers= "mH\tmax(S/deltaS)[False] [%]\tmax(S/Sref)[True] [%]\t|max(S)|\tnParam\tResult"
        #np.savetxt("%s/bkg_results_dataframe.txt" % sys.argv[2], df_max.values, fmt="%d", delimiter="\t", header=headers)
