import os
from glob import glob
import logging

logging.basicConfig(level=logging.DEBUG)

def create_dir_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

for elem in glob('config_files/*/*'):
    
    cfg_name = elem.split('/')[-1]
    bin_name = cfg_name.split('_')[-1].split('.')[0]
    quantity = elem.split('/')[-2]
    
    output_basedir = '../output/spurious_signal_evaluation'
    create_dir_if_not_exists(output_basedir)
    output_dir = os.path.join(output_basedir, quantity)
    create_dir_if_not_exists(output_dir)
    output_dir_cfg = os.path.join(output_dir, bin_name)
    
    logging.info('performing spurious signal for file %s' % cfg_name)
    os.system('./spur_signal_Eval.py {config_file} {output_dir}'.format(config_file=elem, output_dir=output_dir_cfg))
