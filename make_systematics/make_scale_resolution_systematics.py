import ROOT
import os
import numpy as np
import logging

import sys
sys.path.append('..')
from common import common

syst_names = ["EG_SCALE_ALL__1up",
              "EG_RESOLUTION_ALL__1up"
]
BASEDIR = "../output/scale_resolution_systematics"

if not os.path.exists(BASEDIR):
    os.mkdir(BASEDIR)

logging.basicConfig(level=logging.DEBUG)
  
for observable in common.quantities:
    logging.info('scale systematics for quantity: %s:' % observable)

    if not os.path.exists("/".join([BASEDIR, observable])):
        os.mkdir("/".join([BASEDIR, observable]))

    for sys_name in syst_names:
        logging.info("______%s" % sys_name)
        fname_sys = "~/tesi_lamberti/merged_normalized_final/PhotonSys/mcAll_prodAll_MxAODPhotonSys._sysHGamEventInfo_%sAuxDyn.merged.norm.root" % sys_name
        fname_nom = "~/tesi_lamberti/merged_normalized_final/mcAll_prodAll_MxAODJetSys._sysNominal.merged.norm.root"

        f_sys = ROOT.TFile.Open(fname_sys)
        f_nom = ROOT.TFile.Open(fname_nom)

        sys_array = []

        histo2d_m_yy_sys = f_sys.Get("histo2d_m_yy_%s__All_isPassed" % observable)
        histo2d_m_yy_nom = f_nom.Get("histo2d_m_yy_%s__All_isPassed" % observable)
        
        nbins = histo2d_m_yy_nom.GetNbinsX()
        
        print("Bin |    mean_sys/mean_nom |    std_sys/std_nom")
        for ibin in range(nbins+2):
            h_sys = histo2d_m_yy_sys.ProjectionY("projection_sys_bin%d" % ibin, ibin, ibin)
            h_nom = histo2d_m_yy_nom.ProjectionY("projection_nom", ibin, ibin)
            std_nom = h_nom.GetStdDev()
            std_sys = h_sys.GetStdDev()
            mean_nom = h_nom.GetMean()
            mean_sys = h_sys.GetMean()

            if not (std_nom == 0 or mean_nom == 0):
                ratio_std = std_sys / std_nom - 1
                ratio_mean = mean_sys / mean_nom - 1
                print(ibin, ratio_mean, ratio_std)
            else:
                ratio_std = 0
                ratio_mean = 0
                print(ibin, ratio_mean, ratio_std)
            
            if 'SCALE' in sys_name:
                ratio = ratio_mean
            elif 'RESOLUTION' in sys_name:
                ratio = ratio_std
            else:
                raise ValueError
    
            sys_array.append(ratio)
            #print("ibin = %d, %f" % (ibin, ratio))

        np.save("%s/%s/ATLAS_%s_systematic" % (BASEDIR, observable, sys_name), sys_array)
    
