import argparse
from glob import glob
import os
import re
from matplotlib import pyplot as plt
import numpy as np

def parse_file(fn):
    n = os.path.basename(fn)
    m = re.search(r'mass_points_(.+?)_bin([0-9]+)\.txt', n)
    return m.group(1), int(m.group(2))

def do_root(data, quantity, ibin):
    import ROOT
    f = ROOT.TFile.Open("histogram_{}_bin{}.root".format(quantity, ibin), "recreate")
    h = ROOT.TH1F("histogram_{}_bin{}.root".format(quantity, ibin), "{} bin: {}.root".format(quantity, ibin), 105, 160, 55)
    for d in data:
        h.Fill(float(d))
    h.Write()

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('folder')
parser.add_argument('--do-root', action='store_true')
args = parser.parse_args()


all_files = glob('{}/**/*.txt'.format(args.folder))
if (not all_files):
    print("no files in {}/**/*.txt".format(args.folder))

step = 1
binning = np.arange(105, 160 + step, step)
for fn in sorted(all_files):
    quantity, ibin = parse_file(fn)
    fig, ax = plt.subplots()
    data = np.array([float(x) for x in open(fn).read().split('\n') if x])
    c, _, __ = ax.hist(data, bins=binning)
    print("first bin {} {} = {}".format(quantity, ibin, c[0]))
    ax.set_xlabel(r'$m_{\gamma\gamma}$ [GeV]')
    ax.set_ylabel('yield / %.2f GeV' % step)
    img_name = "histogram_{}_bin{}".format(quantity, ibin)
    fig.savefig(img_name + ".png")
    fig.savefig(img_name + ".pdf")
    plt.close(fig)

    if args.do_root:
        do_root(data, quantity, ibin)


