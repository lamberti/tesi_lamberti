#!/usr/bin/env python
import sys
sys.path.append('../libraries')

import ROOT
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
from tqdm import tqdm as tqdm_exe
import definitions as defn
import functions as fnc
import smoothing as smt
import warnings

#Remove Runtime Warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

fname = defn.inFileName
f = ROOT.TFile.Open(fname)

#MOVING AVERAGE
def mov_avg(histo, nbin):
    new = []
    n_binx = histo.GetNbinsX()
    h_smooth = ROOT.TH1D('hyymcSR_ptH_bin%d_nominal_All' % nbin, 'hyymcSR_ptH_bin%d_nominal_All' % nbin, n_binx, 105, 160)
    h_smooth.AddBinContent(1, histo.GetBinContent(1))

    for nbinx in range(2, n_binx):
        vec = []
        for i in range(-1, 1):
            vec.append(histo.GetBinContent(nbinx + i))
        mean = np.mean(vec)
        new.append(mean)
    for i in range(2, n_binx):
        h_smooth.AddBinContent(i, new[i - 2])
    h_smooth.AddBinContent(n_binx, histo.GetBinContent(n_binx))

    return h_smooth

def moving(times):
    h_smooth_array = {}
    for nbin in tqdm_exe(defn.n_bins):
        h_smooth = {}
        h_smooth[(nbin, 0)] = f.Get('hyymcSR_ptH_bin%d_nominal_All' % nbin)

        for i in range(1, times+1):
            h_smooth[(nbin, i)] = mov_avg(h_smooth[(nbin, i-1)], nbin)

        h_smooth_array[nbin] = h_smooth[(nbin, times)]
        del h_smooth
    return h_smooth_array


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Smoothing histograms with moving average method.')
    parser.add_argument('--times', type=int, help='number of iterations')
    args = parser.parse_args()

    h_smooth_array = moving(args.times)
    file = ROOT.TFile('output/%d_times_smoothed_histograms[mov_avg].root' % args.times, 'recreate')
    for i in range(1, len(h_smooth_array)):
        h_smooth_array[i].Write()
    file.Close()
