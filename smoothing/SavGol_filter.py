#!/usr/bin/env python
import sys
sys.path.append('../libraries')

import ROOT
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
from tqdm import tqdm as tqdm_exe
import definitions as defn
import functions as fnc
import smoothing as smt
import warnings

#Remove Runtime Warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

fname = defn.inFileName
f = ROOT.TFile.Open(fname)

#SAVITZKY-GOLAV FILTER
def SavGol_filter(ws, histo, nbin):
	n_binsX = histo.GetNbinsX()
	h_smooth = ROOT.TH1D('hyymcSR_ptH_bin%d_nominal_All' % nbin, 'hyymcSR_ptH_bin%d_nominal_All' % nbin, n_binsX, 105, 160)
	h_smooth.AddBinContent(1, histo.GetBinContent(1))
	h_smooth.AddBinContent(2, histo.GetBinContent(2))
	for nbin in range(3, n_binsX-1):

		ws.factory('mass_fit_%s[%f, %f]' % (nbin, histo.GetBinCenter(nbin -2), histo.GetBinCenter(nbin + 2)))
		ws.factory('RooPolynomial::Poly2_%s(mass_fit_%s, {par_0_Pol2[0.01, -10, 10], par_1_Pol2[0.01, -10, 10]})' % (nbin, nbin))
		ws.factory('EXPR::bkg_exp_%s("exp(-@0 * @1)", mass_fit_%s, par0_bkg_exp[0.1, 0.001, 1])' % (nbin, nbin))

		rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(ws.obj('mass')), histo)

		#Create histogram and RooDataHist for smoothing process
		h_fit = ROOT.TH1D('histo_fit_%s' % nbin, 'h_fit_%s' % nbin, 5, histo.GetBinCenter(nbin -2), histo.GetBinCenter(nbin + 2))
		h_fit.AddBinContent(1, histo.GetBinContent(nbin -2))
		h_fit.AddBinContent(2, histo.GetBinContent(nbin -1))
		h_fit.AddBinContent(3, histo.GetBinContent(nbin))
		h_fit.AddBinContent(4, histo.GetBinContent(nbin +1))
		h_fit.AddBinContent(5, histo.GetBinContent(nbin +2))
		rdh_fit = ROOT.RooDataHist('root_data_hist_fit', 'root_data_hist_fit', ROOT.RooArgList(ws.obj('mass_fit_%s' % nbin)), h_fit)

		#Fitting process
		fr = ws.obj('Poly2_%s' % nbin).fitTo(rdh_fit,
		        ROOT.RooFit.PrintLevel(-1),
		        ROOT.RooFit.Save())

		frame = ws.obj('mass_fit_%s' % nbin).frame()
		rdh_fit.plotOn(frame)
		ws.obj('Poly2_%s' % nbin).plotOn(frame)
		resid_hist = frame.residHist()

		index = h_fit.GetBinCenter(3)
		bin_val = h_fit.GetBinContent(3)
		fit_val = h_fit.GetBinContent(3) - resid_hist.Eval(index)

		h_smooth.AddBinContent(nbin, fit_val)

	h_smooth.AddBinContent(n_binsX-1, histo.GetBinContent(n_binsX-1))
	h_smooth.AddBinContent(n_binsX, histo.GetBinContent(n_binsX))

	return h_smooth

def moving(ws, times):
    ws = fnc.create_workspace('ws')
    h_smooth_array = {}
    for nbin in tqdm_exe(defn.n_bins):
        h_smooth = {}
        h_smooth[(nbin, 0)] = f.Get('hyymcSR_ptH_bin%d_nominal_All' % nbin)

        for i in range(1, times+1):
            h_smooth[(nbin, i)] = SavGol_filter(ws, h_smooth[(nbin, i-1)], nbin)

        h_smooth_array[nbin] = h_smooth[(nbin, times)]
        del h_smooth
    return h_smooth_array


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Smoothing histograms with moving average method.')
    parser.add_argument('--ws', help='workspace')
    parser.add_argument('--times', type=int, help='number of iterations')
    args = parser.parse_args()


    h_smooth_array = moving(args.ws, args.times)
    file = ROOT.TFile('output/%d_times_smoothed_histograms[SavGol_filter].root' % args.times, 'recreate')
    for i in range(1, len(h_smooth_array)):
        h_smooth_array[i].Write()
    file.Close()
