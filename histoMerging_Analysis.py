import condorConfig_Analysis_Run__Lib as confLib
import os
from glob import glob

mc_samples = confLib.mcNames_list
productions_list = confLib.productionMode_list
flavours_list = confLib.systNames_list
flavours_dict = confLib.systNames_dict

def get_key(val, my_dict): 
    for key, value in my_dict.items(): 
         if val == value: 
             return key

base_dir = "merged"
if not os.path.exists(base_dir):
    os.mkdir(base_dir)

for mc_sample in mc_samples:
    print("____%s" % mc_sample)
    for flav_name in flavours_list:
        print("________%s" % flav_name)
        systematics_list = confLib.systNames_dict[flav_name]

        for prod_mode in productions_list:
            print("____________%s" % prod_mode)
            
            for syst_name in flavours_dict[flav_name]:
                print("________________%s" % syst_name)
                histo_list = [", ".join(glob("condorOutput_Analysis/%s*%s*%s*%s*.root" % (mc_sample, prod_mode, flav_name, syst_name)))]
                print("condorOutput_Analysis/%s*%s*%s*%s*.root" % (mc_sample, prod_mode, flav_name, syst_name))
                histo_list_for_hadd = histo_list[0].replace(", ", " ")
                if (len(histo_list[0]) == 0):
                    print("WARNING: No files for merging in the sample")
                    continue
                else:
                    os.system("hadd merged/%s%sMxAOD%s._sys%s.merged.root %s" % (mc_sample, get_key(prod_mode, confLib.productionNames_dict), flav_name, syst_name, histo_list_for_hadd))
                        
