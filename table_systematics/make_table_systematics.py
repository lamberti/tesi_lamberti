from glob import glob
import os
import numpy as np
import pandas as pd



template_table = r"""
\begin{table}[htbp]
\centering
\resizebox{\textwidth}{!}{
%s
}
\caption{Systematic values (in \%%) on the c-factors for each bin of the %s quantity.}
\end{table}
"""

def beautify_quantity(quantity):
    # TODO: complete here
    return {'pT_yy': r'$p_{T}^{\gamma\gamma}$',
            'Dphi_j_j_30_signed': r'$\Delta\phi_{jj}$',
            'pT_j1_30': r'$p_T^{j_1}$',
            'yAbs_yy': r'$|y_{\gamma\gamma}|$'
            }.get(quantity, quantity)




def get_systematic_from_fn(fn):
    return fn.split('_LUMI')[0].split('_sys')[1].replace('HGamEventInfo_', '').replace('__1upAuxDyn', '')

def format_float(val):
    return "%.1f" % (val * 100) if abs(val) > 0.5E-2 else '-'



import argparse

parser = argparse.ArgumentParser(description='Produce latex table for cfactor systematics.')
parser.add_argument('quantity')
args = parser.parse_args()

quantity = args.quantity
BASE_FOLDER = "../output/OUTPUT_plot_to_c_factor_array/%s/c_factors/np_arrays" % quantity

all_files = glob(os.path.join(BASE_FOLDER, '*'))
if not all_files:
    print("no files found")

all_data = {}

for fn in all_files:
    basename = os.path.basename(fn)
    sys = get_systematic_from_fn(basename)
    all_data[sys] = np.load(fn)

df = pd.DataFrame(all_data)
df_sys = df.T / df['Nominal'] - 1
df_sys.drop('Nominal', inplace=True)
to_be_removed = 'EG_RESOLUTION_ALL', 'EG_SCALE_ALL', 'EG_SCALE_AF2'
for c in to_be_removed:
    if c in df_sys.index:
        df_sys.drop(c, inplace=True)

df_sys.sort_index(inplace=True)
df_sys_str = df_sys.applymap(format_float)
tabular_latex = df_sys_str.to_latex()
table = template_table % (tabular_latex, beautify_quantity(quantity))

outf = open('sys_cfactor_%s.tex' % quantity, 'w')
outf.write(table)


