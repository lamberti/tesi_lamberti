# Tesi_Lamberti

Tesi Mario Lamberti 2019

The xml to build the workspaces are in `make_workspace` folder, for example in `make_workspace/binbybin_method/workspace_binbybin_pT_yy`.

## Evaluate the spurious signal and choose background model

First of all, generate the configuration files for the selected quantity

    cd spurious_sign_evaluation/config_files
    ./confFileCreate_spurSignEval.py pT_yy

Once the configuration files has been created, run the main spurious signal code

    ./spur_signal_Eval.py $config_file_name$ $output_folder$

To run spurious signal for all the configuration files:

    python run_spur_sign_all_cfg.py

The output folder is saved as `output/spurious_signal_evaluation`.

## Create signal model

Create signal model using inclusive nominal dataset for single quantity and for the single bin (to create the signal model for all quantity's bins, remove the flag `--bin`):

    cd make_signal
    python generateSignal_parameters.py ../merged_normalized_final/mcAll_prodAll_MxAODJetSys._sysNominal.merged.norm.root --quantity pT_yy --bin 1

To create the signal model for all the quantities

    python generateSignal_parameters.py ../merged_normalized_final/mcAll_prodAll_MxAODJetSys._sysNominal.merged.norm.root

The CB parameters just created are saved in `output/signalParametrisation`.

## Compute folding matrices and cfactors

To compute the xsections, acceptances, yields, folding matrices and cfactors for a specific quantity and for a specific variation (inclusing the nominal case) run the notebook `unfolding/plot_to_unfolding_matrix.ipynb`.

If you want to run the same notebook for all the quantities and all the variations:

    cd unfolding; python run_efficiency_all_sys.py

It will take around 1 hour. The output is saved in `output/acceptances`, `output/cfactors`, `output/folding_matrices`, `output/plot1D`, `output/xsections/`, `output/unfolding-papermill-notebooks`.

## Compute the systematic variation on folding matrices

Once you have computed all the folding matrices you can compute their ratio with the nominal one with the notebook `unfolding/matrix_method_unfolding.ipynb`. If you want to run for all the quantities:

    cd unfolding; python run_matrix_vars_all_sys.py

The output is saved in `output/folding_matrices_sysvar`.

## Compute the systematic variation on cfactors

Similarly you can run the variations for the cfactors running the notebook `unfolding/bin_by_bin_unfolding.ipynb`. If you want to run for all the quantities:

    cd unfolding; python run_cfactor_vars_all_sys.py

## Setup to work with workspaces

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    setupATLAS
    lsetup "root 6.04.16-HiggsComb-x86_64-slc6-gcc49-opt"

Setup xmlAnaBuilder (`/xmlAnaBuilder; source setup.sh`)

## Create the xml and workspace for bin-by-bin method

To create the workspace, first you have to create the xml. Go in the directory with the xml (`cd make_workspace/binbybin_method`)

    python make_workspace.py pT_yy --blinded

if you want the unblinded, remove the option. To generate all the workspaces you can do

    python make_workspace.py ALL --blinded
    python make_workspace.py ALL

The you have to generate the workspace from the xmls. For example:

    cd workspace_pT_yy_blinded/
    XMLReader -x combWS.xml

This second step must be done after you have setup xmlAnaBuilder. Check the produced pdf. You can generate all of them:

    ./run_all.sh

## Create the xml and workspace for matrix method

To create the workspace, first you have to create the xml. Go in the directory with the xml (`cd make_workspace/matrix_method`)

    python make_workspace.py pT_yy --blinded

if you want the unblinded, remove the option. To generate all the workspaces you can do

    python make_workspace.py ALL --blinded
    python make_workspace.py ALL

The you have to generate the workspace from the xmls. For example:

    cd workspace_pT_yy_blinded/
    XMLReader -x combWS.xml

This second step must be done after you have setup xmlAnaBuilder. Check the produced pdf. You can generate all of them:

    ./run_all.sh

## Fit the workspace

To fit the workspace, setup quickFit, then:

    quickFit -f XSectionWS_pT_yy.root -w combWS -d AsimovSB -p mu_xsec_pT_yy_1,mu_xsec_pT_yy_2,mu_xsec_pT_yy_3,mu_xsec_pT_yy_4,mu_xsec_pT_yy_5,mu_xsec_pT_yy_6,mu_xsec_pT_yy_7,mu_xsec_pT_yy_8,mu_xsec_pT_yy_9,mu_xsec_pT_yy_10,mu_xsec_pT_yy_11,mu_xsec_pT_yy_12,mu_xsec_pT_yy_13,mu_xsec_pT_yy_14,mu_xsec_pT_yy_15,mu_xsec_pT_yy_16,mu_xsec_pT_yy_17 --saveWS 1 -o XSectionWS_pT_yy-fitted_AsimovSB.root

To fit  all the workspaces and scan on the systematics, go in the directory (`cd scan_syst_variations`)

    python run_quickFit_all_sys.py --method binbybin
    python run_quickFit_all_sys.py --method matrix

in order to obtain the workspaces useful to evaluate the systematic variations for each quantity.
Outputs are stored in `output/quickFit_binbybin` and `output/quickFit_matrix`.

To plot the result after the fit:

    cd plot_workspace
    python plot_workspace.py XSectionWS_pT_yy-fitted_AsimovSB.root

## Fit all the workspace with loop on systematics

This will perform fits for the matrix method and for the pT_yy quantity, looping on blinded/unblinded and all the systematics (turning off one by one):

    cd perform_analysis
    python run_quickFit_all_sys.py matrix --quantity pT_yy

if you don't specify the quantity all the quantites are looped.

## Extrapolate to high-lumi

Once you have a workspace generated from full Run2 you can extrapolate it to various luminosities. For example:

    cd lumi_extrapolation
    python change_workspace_luminosity.py ../make_workspace/matrix_method/workspace_pT_yy_blinded/workspace_pT_yy_blinded/XSectionWS_pT_yy.root 70000 5000000 10 --output-dir ws_hl_pT_yy
    python change_workspace_luminosity.py ../make_workspace/matrix_method/workspace_pT_yy_blinded/workspace_pT_yy_blinded/XSectionWS_pT_yy.root 70000 5000000 10 --stat-only --output-dir ws_hl_pT_yy_stat_only
    python plot_luminosity.py ws_hl_pT_yy ws_hl_pT_yy_stat_only --bin 1 2 3

## Obtain final plots

As the last thing, obtain the final plots for the measured expected and observed cross sections compared with the SM predictions, the systematic pulls and the correlation matrix for each quantity of interest. Go to the directory (`cd create_final_plots`)

    python run_plots_all_obs.py --output-dir ../output

Outputs are stored in `output/plot-correlation`, `output/pulls` and `output/xsection`.