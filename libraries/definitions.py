inFileName = "../data/bkg.h024.root"

categories_names = [
		"ggH_0J_Cen",
		"ggH_0J_Fwd",
		"ggH_1J_BSM",
		"ggH_1J_HIGH",
		"ggH_1J_LOW",
		"ggH_1J_MED",
		"ggH_2J_BSM",
		"ggH_2J_HIGH",
		"ggH_2J_LOW",
		"ggH_2J_MED",
		"qqH_BSM",
		"VHlep_LOW",
		"VHlep_HIGH",
		"VHhad_tight",
		"VHhad_loose",
		"VHdilep",
		"VHMET_LOW",
		"VHMET_HIGH",
		"VBF_HjjLOW_tight",
		"VBF_HjjLOW_loose",
		"VBF_HjjHIGH_tight",
		"VBF_HjjHIGH_loose"]

bkg_names = [
	'exp',
	'expPoly2',
	'Bern3',
	'Bern4',
	'Bern5',
	'pow',
	'dijet']

parameters_names = [
	'mu_CB_Nom',
	'sigma_CB_Nom',
	'alpha_CB_Lo',
	'alpha_CB_Hi',
	'n_CB_Lo',
	'n_CB_Hi',
        'yield_CB']

n_bins = range(1, 19)

efficiencies = {
	"ggH_0J_Cen": 0.08,
	"ggH_0J_Fwd": 0.12,
	"ggH_1J_BSM": 0.00221,
	"ggH_1J_HIGH": 0.0082,
	"ggH_1J_LOW": 0.069,
	"ggH_1J_MED": 0.411,
	"ggH_2J_BSM": 0.0017,
	"ggH_2J_HIGH": 0.0072,
	"ggH_2J_LOW": 0.0148,
	"ggH_2J_MED": 0.0173,
	"qqH_BSM": 0.0055,
	"VHlep_LOW": 0.0013,
	"VHlep_HIGH": 0.00031,
	"VHhad_tight": 0.0027,
	"VHhad_loose": 0.0036,
	"VHdilep": 0.00018,
	"VHMET_LOW": 0.000146,
	"VHMET_HIGH": 0.00027,
	"VBF_HjjLOW_tight": 0.0055,
	"VBF_HjjLOW_loose": 0.00689,
	"VBF_HjjHIGH_tight": 0.0123,
	"VBF_HjjHIGH_loose": 0.00473}
