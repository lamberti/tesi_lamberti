import ROOT
import os
import numpy as np
import matplotlib.pyplot as plt

TH1DList = {}

def TH1F2np(histo, overflow=False):
    content = histo.GetArray()
    content.SetSize(histo.GetNbinsX() + 2)
    if overflow:
        content = np.array(content)[1:]
    else:
        content = np.array(content)[1:-1]
    binning = np.array(histo.GetXaxis().GetXbins())
    if overflow:
        binning = np.append(binning, [np.inf])
        
    return content, binning

def TH2F2np(histo, overflow=False):
    binningx = np.array(histo.GetXaxis().GetXbins())
    binningy = np.array(histo.GetYaxis().GetXbins())
    if overflow:
        binningx = np.append(binningx, [np.inf])
        binningy = np.append(binningy, [np.inf])
    if overflow:
        content = np.zeros((histo.GetNbinsX() + 1, histo.GetNbinsY() + 1))
    else:
        content = np.zeros((histo.GetNbinsX(), histo.GetNbinsY()))
    for xbin in range(1, histo.GetNbinsX() + (2 if overflow else 1)):
        for ybin in range(1, histo.GetNbinsY() + (2 if overflow else 1)):
            content[xbin - 1, ybin - 1] = histo.GetBinContent(xbin, ybin)
    return content, binningx, binningy

def createHisto1D(name, title, nbinsX, xMin, xMax):
	TH1DList[name] = ROOT.TH1D(name, title, nbinsX, xMin, xMax)

def getHisto1D(name):
	return TH1DList[name]

def getListOfHistograms():
	return TH1DList

def makeArrayFromHisto( inputHisto ):
	nBins = inputHisto.GetNbinsX()
	outputArray = np.zeros( nBins , 'd' )
	for iBin in range(nBins):
		outputArray[iBin] = inputHisto.GetBinContent( iBin+1 )
	return outputArray

def makeHistoFromArray( inputArray , outputHisto , inputError = np.array('i') ):
	nBins = outputHisto.GetNbinsX()
	if( len(inputArray) != nBins ):
		print("WARNING in makeHistoFromArray")
		print("Number of bins in histogram does not match length of input array!")
		print("Returning empty histogram.")
		return
	for iBin in range(nBins):
		outputHisto.SetBinContent( iBin+1 , inputArray[iBin] )
		if( inputError.shape ): outputHisto.SetBinError( iBin+1 , inputError[iBin] )
'''
def TH1F2np(histoPath, histoName):
	fname = os.path.join(histoPath, histoName)
	f = ROOT.TFile(fname)
	histo = f.Get('%s' % histoName)

    content = histo.GetArray()
    content.SetSize(histo.GetNbinsX() + 2)
    content = np.array(content)[1:-1]
    binning = np.array(histo.GetXaxis().GetXbins())
    return content, binning

def TH2F2np(histoPath, histoName):
	fname = os.path.join(histoPath, histoName)
	f = ROOT.TFile(fname)
	histo = f.Get('%s' % histoName)

    binning_x = np.array(histo.GetXaxis().GetXbins())
    binning_y = np.array(histo.GetYaxis().GetXbins())
    content = np.zeros((histo.GetNbinsX(), histo.GetNbinsY()))
    for xbin in range(1, histo.GetNbinsX() + 1):
        for ybin in range(1, histo.GetNbinsY() + 1):
            content[xbin - 1, ybin - 1] = histo.GetBinContent(xbin, ybin)
    return content, binning_x, binning_y
'''

def TH1F2plt(histoPath, histoName, xLabel, yLabel, histoTitle):
	fname = os.path.join(histoPath, histoName)
	f = ROOT.TFile(fname)
	histo = f.Get('%s' % histoName)

	content, binning = TH1F2np(histo)
	bincenters = 0.5 * (binning[1:] + binning[:-1])
	binwidths = (binning[1:] - binning[:-1])

	fig, ax = plt.subplots()
	ax.bar(bincenters, content / binwidths, binwidths)
	ax.set_xlabel('%s' % xLabel)
	ax.set_ylabel('%s' % yLabel)
	ax.set_title('%s' % histoTitle)

	plt.savefig('%s.png' % histoName)
	print('%s saved' % histoName)

def TH2F2plt(histoPath, histoName, xLabel, yLabel, histoTitle):
	fname = os.path.join(histoPath, histoName)
	f = ROOT.TFile(fname)
	histo = f.Get('%s' % histoName)

	content, binning_x, binning_y = hul.TH2F2np(histo)
	true = content.sum(axis=1)

	fig, ax = plt.subplots(figsize=(10, 10))
	sns.heatmap(content / true * 100, annot=True, ax=ax, square=True, vmin=0, vmax=100)
	ax.set_xlabel('%s' % xLabel)
	ax.set_ylabel('%s' % yLabel)
	ax.set_title('%s' % histoTitle)

	plt.savefig('%s.png' % histoName)
	print('%s saved' % histoName)
