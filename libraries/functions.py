import ROOT
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import definitions as defn
import smoothing as smt


def safe_factory(func):
    def wrapper(self, *args):
        result = func(self, *args)
        if not result:
            raise ValueError('invalid factory input "%s"' % args)
        return result
    return wrapper

ROOT.RooWorkspace.factory = safe_factory(ROOT.RooWorkspace.factory)

def safe_decorator(func):
    def wrapper(self, *args):
        result = func(self, *args)
        if not result:
            raise ValueError('cannot find %s' % args[0])
        return result
    return wrapper

ROOT.RooWorkspace.data = safe_decorator(ROOT.RooWorkspace.data)
ROOT.RooWorkspace.obj = safe_decorator(ROOT.RooWorkspace.obj)
ROOT.RooWorkspace.var = safe_decorator(ROOT.RooWorkspace.var)
ROOT.RooWorkspace.pdf = safe_decorator(ROOT.RooWorkspace.pdf)


#LIST OF FUNCTIONS:
#create_workspace(ws_name): create a workspace with signal and backgrounds functions
#parameters(): get Crystal Ball parameters values
#generate_asimov(ws, histo, bkg_norm): generate asimov from an histogram
#bkg_norm(histo_data, histo_mc, nbin): return the normalisation number from a data histogram
#spur_sign(histo_mc, bkg_norm, parameters, nbin): return spurious signals and errors for all bkg functions

#Remove messages from RooFit
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.DataHandling)
ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.Plotting)

#Compile library for signal shape
ROOT.gROOT.ProcessLine(".L HggTwoSidedCBPdf.cxx+")

bkg_names = ['exp',
             'expPoly2',
             'Bern3', 'Bern4', 'Bern5', 'pow'
            ]

def create_workspace(ws_name):
	ws = ROOT.RooWorkspace(ws_name)
	mass = ws.factory("mass[105, 160]")

	#Define all the background functions
	ws.factory('EXPR::bkg_exp("exp(-@0 * @1)", mass, par0_bkg_exp[0.1,0.001,1])')
	ws.factory('EXPR::bkg_expPoly2("exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))", mass, {par0_Poly2[0.1,-10,10], par1_Poly2[0.1,-10,10]})')
	ws.factory('RooBernstein::bkg_Bern3(mass, {par0_Bern3[0.001,-10,10], par1_Bern3[0.001,-10,10], par2_Bern3[0.001,-10,10], 1})')
	ws.factory('RooBernstein::bkg_Bern4(mass, {par0_Bern4[5,0.001,10], par1_Bern4[2.5,0.001,5], par2_Bern4[2.5,0.001,5], par3_Bern4[2.5,0.001,5], 1})')
	ws.factory('RooBernstein::bkg_Bern5(mass, {par0_Bern5[5,0.001,10], par1_Bern5[2.5,0.001,5], par2_Bern5[2.5,0.001,5], par3_Bern5[2.5,0.001,5], par4_Bern5[2.5,0.001,5], 1})')
	ws.factory('EXPR::bkg_dijet("pow(@0/13E3, @1)*pow(1 - @0/13E3, @2)", mass, par0_dijet[-5,-10,-0.1], par1_dijet[15,10,20])')
	ws.factory('EXPR::bkg_pow("pow(@0, @1)", mass, par0_pow[0.1,-10,10])')

	# Create signal
	ws.factory("mH[125, 110, 150]")
	ws.factory('mu_CB125[125]')
	ws.factory('expr:mu_CB("@0 - 125 + @1", mu_CB125, mH)')
	ws.factory('sigma_CB125[1]')
	ws.factory('alpha_low_CB125[1]')
	ws.factory('alpha_hi_CB125[1]')
	ws.factory('n_low_CB125[10]')
	ws.factory('n_hi_CB125[10]')
	ws.factory('HggTwoSidedCBPdf::signal(mass, mu_CB, sigma_CB125, alpha_low_CB125, n_low_CB125, alpha_hi_CB125, n_hi_CB125)')
	ws.obj('mH').setConstant()

	#Define signal + background PDF
	ws.factory('SUM:model_sb_bkg_exp(nsig[0, -10000, 10000] * signal, nbkg[150E3, 1, 1E6] * bkg_exp)')
	ws.factory('SUM:model_sb_bkg_expPoly2(nsig * signal, nbkg * bkg_expPoly2)')
	ws.factory('SUM:model_sb_bkg_Bern3(nsig * signal, nbkg * bkg_Bern3)')
	ws.factory('SUM:model_sb_bkg_Bern4(nsig * signal, nbkg * bkg_Bern4)')
	ws.factory('SUM:model_sb_bkg_Bern5(nsig * signal, nbkg * bkg_Bern5)')
	ws.factory('SUM:model_sb_bkg_dijet(nsig * signal, nbkg * bkg_dijet)')
	ws.factory('SUM:model_sb_bkg_pow(nsig * signal, nbkg * bkg_pow)')

	return ws

def parameters():
	all_dfs_cat = {}

	for nbin in range(1,19):
	    for i in range(4,11):
		param = pd.read_csv('/home/mario/tesi_lamberti/xsection/SharedArea2019CONF-master/signalParametrisation_h024_april12/SM_pT_%d/resonance_paramList.txt' % nbin, sep=' ', index_col=0, header=None)[1][i]
		all_dfs_cat[(nbin, defn.parameters_names[i-4])] = param
	    all_dfs = pd.Series(all_dfs_cat)
	    df_parameters = pd.DataFrame({'value' :all_dfs})
	    df_parameters.index.names = ['bin', 'parameters']

	return df_parameters

def generate_asimov(ws, histo, bkg_norm):
	rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(ws.obj('mass')), histo)
	rhpdf = ROOT.RooHistPdf('root_hist_pdf', 'root_hist_pdf', ROOT.RooArgSet(ws.obj('mass')), rdh)
	asimov = rhpdf.generateBinned(ROOT.RooArgSet(ws.obj('mass')), ROOT.RooFit.NumEvents(bkg_norm), ROOT.RooFit.ExpectedData())

	return asimov

def bkg_norm(histo_data, histo_mc, nbin, save_plots):

	nbkg_from_datasb_bin = {}

	h = histo_data

	ws = ROOT.RooWorkspace()
	mass = ws.factory("mass[105, 160]")
	mass.setRange('left', 105, 121)
	mass.setRange('right', 128, 160)


	b = h.GetArray()
	b.SetSize(h.GetNbinsX() + 2)
	b = np.array(b)
	histo_entries = b[b>0].sum()

	rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(ws.obj('mass')), h)
	ws.factory('EXPR::bkg_exp("exp(-@0 * @1)", mass, par0_bkg_exp[0.1, 0.001, 1])')
	ws.factory('SUM:model(nbkg[%s, %s, %s] * bkg_exp)' % (histo_entries, histo_entries / 5., histo_entries * 3))

	fr = ws.obj('model').fitTo(rdh,
		               ROOT.RooFit.Range("left,right"),
		               ROOT.RooFit.PrintLevel(-1),
		               ROOT.RooFit.Save())

	nbkg_from_datasb_bin[nbin] = ws.obj('nbkg').getVal()

	hmc_template = histo_mc

	#hmc_template.Scale(220. / (160 - 105))
	hmc_template.Rebin(4)

	if save_plots != 0:
		canvas = ROOT.TCanvas()
		frame = ws.obj("mass").frame()
		rdh.plotOn(frame)
		ws.obj('model').plotOn(frame)
		#rdhmc_template.plotOn(frame)

		frame.addTH1(hmc_template, "hist")

		#frame.SetMaximum(100)
		frame.Draw()
		canvas.SaveAs("./output/bkg_fit_sidebands_bin%d.png" % nbin)

	return nbkg_from_datasb_bin[nbin]


def spur_sign(ws, histo_mc, bkg_norm, parameters, nbin):

	mu_CB = parameters.loc[nbin, 'mu_CB_Nom']
	sigma_CB = parameters.loc[nbin, 'sigma_CB_Nom']
	alpha_low_CB = parameters.loc[nbin, 'alpha_CB_Lo']
	alpha_hi_CB = parameters.loc[nbin, 'alpha_CB_Hi']
	n_low_CB = parameters.loc[nbin, 'n_CB_Lo']
	n_hi_CB = parameters.loc[nbin, 'n_CB_Hi']
	yield_CB = parameters.loc[nbin, 'yield_CB']

	#Define signal shape
	ws.obj('mu_CB125').setVal(mu_CB)
	ws.obj('sigma_CB125').setVal(sigma_CB)
	ws.obj('alpha_low_CB125').setVal(alpha_low_CB)
	ws.obj('alpha_hi_CB125').setVal(alpha_hi_CB)
	ws.obj('n_low_CB125').setVal(n_low_CB)
	ws.obj('n_hi_CB125').setVal(n_hi_CB)
	ws.obj('mH').setConstant()

	ws.saveSnapshot('initial', ws.allVars())

	all_spur_sign = {}
	all_spur_sign_err = {}
	all_test_mH = np.arange(120, 130, 0.5)

	rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist',
		           ROOT.RooArgList(ws.obj('mass')), histo_mc)
	rhpdf = ROOT.RooHistPdf('root_hist_pdf', 'root_hist_pdf', ROOT.RooArgSet(ws.obj('mass')), rdh)
	ws.loadSnapshot('initial')
	ws.obj('nbkg').setVal(bkg_norm)
	data_asimov = rhpdf.generateBinned(ROOT.RooArgSet(ws.obj('mass')),
		                       ROOT.RooFit.NumEvents(bkg_norm),
		                       ROOT.RooFit.ExpectedData())
	for bkg_name in bkg_names:
		#print(bkg_name)
		for mH_test in all_test_mH:
		    ws.loadSnapshot('initial')
		    ws.obj('nbkg').setVal(bkg_norm)
		    ws.obj('mH').setVal(mH_test)
		    ws.obj('mH').setConstant(True)

		    fr = ws.obj('model_sb_bkg_%s' % bkg_name).fitTo(data_asimov, ROOT.RooFit.Save(True),
				                                    ROOT.RooFit.PrintLevel(-1),
				                                    ROOT.RooFit.SumW2Error(True))
		    if fr.status() != 0:
			print("problem with fit mH=%f, function=%s, bin=%d, status=%d" % (mH_test, bkg_name,
				                                                              nbin, fr.status()))

		    all_spur_sign[(bkg_name, mH_test)] = ws.obj('nsig').getVal()
		    all_spur_sign_err[(bkg_name, mH_test)] = ws.obj('nsig').getError()

	df_spurious = pd.Series(all_spur_sign)
	df_err = pd.Series(all_spur_sign_err)
	df_all = pd.DataFrame({'ss': df_spurious, 'ss_err': df_err})

	return df_all
