import ROOT
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook as tqdm
import definitions as defn

#MOVING AVERAGE
def mov_avg(histo):
    new = []
    n_binx = histo.GetNbinsX()
    h_smooth = ROOT.TH1D('h_smooth', 'h_s', n_binx, 105, 160)
    h_smooth.AddBinContent(1, histo.GetBinContent(1))

    for nbinx in range(2, n_binx):
        vec = []
        for i in range(-1, 1):
            vec.append(histo.GetBinContent(nbinx + i))
        mean = np.mean(vec)
        new.append(mean)
    for i in range(2, n_binx):
        h_smooth.AddBinContent(i, new[i - 2])
    h_smooth.AddBinContent(n_binx, histo.GetBinContent(n_binx))
    
    return h_smooth

#SAVITZKY-GOLAV FILTER
def SavGol_filter(ws, histo):
	n_binsX = histo.GetNbinsX()
	h_smooth = ROOT.TH1D('histo_smooth', 'h_smooth', n_binsX, 105, 160)
	h_smooth.AddBinContent(1, histo.GetBinContent(1))
	h_smooth.AddBinContent(2, histo.GetBinContent(2))
	for nbin in range(3, n_binsX-1):

		ws.factory('mass_fit_%s[%f, %f]' % (nbin, histo.GetBinCenter(nbin -2), histo.GetBinCenter(nbin + 2)))
		ws.factory('RooPolynomial::Poly2_%s(mass_fit_%s, {par_0_Pol2[0.01, -10, 10], par_1_Pol2[0.01, -10, 10]})' % (nbin, nbin))
		ws.factory('EXPR::bkg_exp_%s("exp(-@0 * @1)", mass_fit_%s, par0_bkg_exp[0.1, 0.001, 1])' % (nbin, nbin))

		rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(ws.obj('mass')), histo)

		#Create histogram and RooDataHist for smoothing process
		h_fit = ROOT.TH1D('histo_fit_%s' % nbin, 'h_fit_%s' % nbin, 5, histo.GetBinCenter(nbin -2), histo.GetBinCenter(nbin + 2))
		h_fit.AddBinContent(1, histo.GetBinContent(nbin -2))
		h_fit.AddBinContent(2, histo.GetBinContent(nbin -1))
		h_fit.AddBinContent(3, histo.GetBinContent(nbin))
		h_fit.AddBinContent(4, histo.GetBinContent(nbin +1))
		h_fit.AddBinContent(5, histo.GetBinContent(nbin +2))
		rdh_fit = ROOT.RooDataHist('root_data_hist_fit', 'root_data_hist_fit', ROOT.RooArgList(ws.obj('mass_fit_%s' % nbin)), h_fit)

		#Fitting process
		fr = ws.obj('Poly2_%s' % nbin).fitTo(rdh_fit,
		        ROOT.RooFit.PrintLevel(-1),
		        ROOT.RooFit.Save())

		frame = ws.obj('mass_fit_%s' % nbin).frame()
		rdh_fit.plotOn(frame)
		ws.obj('Poly2_%s' % nbin).plotOn(frame)
		resid_hist = frame.residHist()
	
		index = h_fit.GetBinCenter(3)
		bin_val = h_fit.GetBinContent(3)
		fit_val = h_fit.GetBinContent(3) - resid_hist.Eval(index)
		
		h_smooth.AddBinContent(nbin, fit_val)

	h_smooth.AddBinContent(n_binsX-1, histo.GetBinContent(n_binsX-1))
	h_smooth.AddBinContent(n_binsX, histo.GetBinContent(n_binsX))

	return h_smooth
