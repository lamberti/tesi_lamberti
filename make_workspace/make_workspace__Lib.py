spur_signal_best_functions = {
    "pT_yy": {
        "bin_1": {
            "function": "EXPR::bkg_pow_category_pT_yy_1('pow(@0, @1)', :observable:, par0_pow_category_pT_yy_1[-4.59,-10,10])",
            "SS_Sref_value": 13.6,
            "SS_value": 37.4
            },
        "bin_2": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_2[-4.34,-10,10], par1_Poly2_category_pT_yy_2[1.17,-10,10])",
            "SS_Sref_value": 15.1,
            "SS_value": 88.9
            },
        "bin_3": {
            "function": "EXPR::bkg_pow_category_pT_yy_3('pow(@0, @1)', :observable:, par0_pow_category_pT_yy_3[-4.69,-10,10])",
            "SS_Sref_value": 34.6,
            "SS_value": 219.3
            },
        "bin_4": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_4('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_4[-3.97,-10,10], par1_Poly2_category_pT_yy_4[0.80,-10,10])",
            "SS_Sref_value": 11.6,
            "SS_value": 67.5
            },
        "bin_5":{
            "function": "EXPR::bkg_expPoly2_category_pT_yy_5('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_5[-3.75,-10,10], par1_Poly2_category_pT_yy_5[0.66,-10,10])",
            "SS_Sref_value": 23.5,
            "SS_value": 120.1
            },
        "bin_6": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_6('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_6[-3.70,-10,10], par1_Poly2_category_pT_yy_6[0.69,-10,10])",
            "SS_Sref_value": 5.9,
            "SS_value": 25.9
            },
        "bin_7": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_7('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_7[-3.55,-10,10], par1_Poly2_category_pT_yy_7[0.64,-10,10])",
            "SS_Sref_value": 18.9,
            "SS_value": 71.1
            },
        "bin_8": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_8('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_8[-3.29,-10,10], par1_Poly2_category_pT_yy_8[0.49,-10,10])",
            "SS_Sref_value": 10.3,
            "SS_value": 61.6
            },
        "bin_9": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_9('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_9[-2.96,-10,10], par1_Poly2_category_pT_yy_9[0.32,-10,10])",
            "SS_Sref_value": 13.1,
            "SS_value": 82.1
            },
        "bin_10": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_10('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_10[-2.48,-10,10], par1_Poly2_category_pT_yy_10[0.02,-10,10])",
            "SS_Sref_value": 7.4,
            "SS_value": 39.0
            },
        "bin_11": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_11('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_11[-1.00,-10,10], par1_Poly2_category_pT_yy_11[-1.18,-10,10])",
            "SS_Sref_value": 18.2,
            "SS_value": 59.1
            },
        "bin_12": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_12('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_12[-1.12,-10,10], par1_Poly2_category_pT_yy_12[0.11,-10,10])",
            "SS_Sref_value": 22.7,
            "SS_value": 47.6
            },
        "bin_13": {
            "function": "EXPR::bkg_expPoly2_category_pT_yy_13('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_category_pT_yy_13[-1.36,-10,10], par1_Poly2_category_pT_yy_13[0.21,-10,10])",
            "SS_Sref_value": 9.0,
            "SS_value": 13.1
            },
        "bin_14": {
            "function": "EXPR::bkg_pow_category_pT_yy_14('pow(@0, @1)', :observable:, par1_pow_category_pT_yy_14[-1.64,-10,10])",
            "SS_Sref_value": 16.1,
            "SS_value": 23.3
            },
        "bin_15": {
            "function": "EXPR::bkg_pow_category_pT_yy_15('pow(@0, @1)', :observable:, par1_pow_category_pT_yy_15[-1.34,-10,10])",
            "SS_Sref_value": 17.3,
            "SS_value": 15.3
            },
        "bin_16": {
            "function": "EXPR::bkg_pow_category_pT_yy_16('pow(@0, @1)', :observable:, par1_pow_category_pT_yy_16[-1.44,-10,10])",
            "SS_Sref_value": 7.4,
            "SS_value": 6.0
            },
        "bin_17": {
            "function": "EXPR::bkg_pow_category_pT_yy_17('pow(@0, @1)', :observable:, par1_pow_category_pT_yy_17[-1.04,-10,10])",
            "SS_Sref_value": 8.0,
            "SS_value": 4.5
            },
        "bin_18": {
            "function": "EXPR::bkg_pow_category_pT_yy_18('pow(@0, @1)', :observable:, par1_pow_category_pT_yy_18[-0.65,-10,10])",
            "SS_Sref_value": 26.1,
            "SS_value": 5.8
            }
        },
    "yAbs_yy": {
        "bin_1": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_1('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_1[-3.6,-10,5], par1_Poly2_category_yAbs_yy_1[0.9,5,5])",
            "SS_Sref_value": 5.6,
            "SS_value": 36.9
            },
        "bin_2": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_2[-3.7,-10,5], par1_Poly2_category_yAbs_yy_2[1,-5,5])",
            "SS_Sref_value": 8.5,
            "SS_value": 36.9
            },
        "bin_3": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_3[-3.5,-10,5], par1_Poly2_category_yAbs_yy_3[0.6,-5,5])",
            "SS_Sref_value": 12.1,
            "SS_value": 76.5
            },
        "bin_4": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_4('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_4[-3.6,-10,5], par1_Poly2_category_yAbs_yy_4[0.8,-5,5])",
            "SS_Sref_value": 11.4,
            "SS_value": 68.2
            },
        "bin_5": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_5('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_5[-3.8,-10,5], par1_Poly2_category_yAbs_yy_5[1.3,-5,5])",
            "SS_Sref_value": 20.0,
            "SS_value": 112.8
            },
        "bin_6": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_6('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_6[-3.7,-10,5], par1_Poly2_category_yAbs_yy_6[1,-5,5])",
            "SS_Sref_value": 15.9,
            "SS_value": 83.1
            },
        "bin_7": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_7('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_7[-3.7,-10,5], par1_Poly2_category_yAbs_yy_7[1,-5,5])",
            "SS_Sref_value": 9.2,
            "SS_value": 84.5
            },
        "bin_8": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_8('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_8[-3.9,-5,5], par1_Poly2_category_yAbs_yy_8[1.5,-5,5])",
            "SS_Sref_value": 14.8,
            "SS_value": 140.1
            },
        "bin_9": {
            "function": "EXPR::bkg_expPoly2_category_yAbs_yy_9('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_yAbs_yy_9[-3.7,-10,5], par1_Poly2_category_yAbs_yy_9[1,-5,5])",
            "SS_Sref_value": 34.1,
            "SS_value": 252.7
            }
        },
    "Dphi_j_j_30_signed": {
        "bin_0": {
            "function": "EXPR::bkg_expPoly2_category_Dphi_j_j_30_signed_0('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_Dphi_j_j_30_signed_0[-4.31,-10,10], par1_Poly2_category_Dphi_j_j_30_signed_0[1.24,-5,5])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_1": {
            "function": "EXPR::bkg_expPoly2_category_Dphi_j_j_30_signed_1('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_Dphi_j_j_30_signed_1[-4.34,-10,10], par1_Poly2_category_Dphi_j_j_30_signed_1[1.17,-5,5])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_2": {
            "function": "EXPR::bkg_pow_category_Dphi_j_j_30_signed_2('pow(@0, @1)', :observable:, par1_pow_category_Dphi_j_j_30_signed_2[-4.69,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_3": {
            "function": "EXPR::bkg_pow_category_Dphi_j_j_30_signed_3('pow(@0, @1)', :observable:, par1_pow_category_Dphi_j_j_30_signed_3[-4.75,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_4": {
            "function": "EXPR::bkg_expPoly2_category_Dphi_j_j_30_signed_4('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_Dphi_j_j_30_signed_4[-3.97,-10,10], par1_Poly2_category_Dphi_j_j_30_signed_4[0.80,-5,5])",
            "SS_Sref_value": 1,
            "SS_value": 0
            }
        },
    "pT_j1_30": {
        "bin_0": {
            "function": "EXPR::bkg_expPoly2_category_pT_j1_30_0('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_pT_j1_30_0[-4,-10,5], par1_Poly2_category_pT_j1_30_0[1.1,-5,5])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_1": {
            "function": "EXPR::bkg_expPoly2_category_pT_j1_30_1('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_pT_j1_30_1[-3.90,-10,10], par1_Poly2_category_pT_j1_30_1[0.87,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_2": {
            "function": "EXPR::bkg_expPoly2_category_pT_j1_30_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_pT_j1_30_2[-3.5,-10,5], par1_Poly2_category_pT_j1_30_2[0.87,-5,5])",
            "SS_Sref_value": 6.3,
            "SS_value": 105.5
            },
        "bin_3": {
            "function": "EXPR::bkg_expPoly2_category_pT_j1_30_3('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_pT_j1_30_3[-2.7,-10,5], par1_Poly2_category_pT_j1_30_3[0.3,-10,10])",
            "SS_Sref_value": 7.0,
            "SS_value": 41.4
            },
        "bin_4": {
            "function": "EXPR::bkg_exp_category_pT_j1_30_4('exp(-@0 * @1)', :observable:, par1_bkg_exp_category_pT_j1_30_4[0.02,0.001,1])",
            "SS_Sref_value": 16.5,
            "SS_value": 47.1
            },
        "bin_5": {
            "function": "EXPR::bkg_expPoly2_category_pT_j1_30_5('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_pT_j1_30_5[-1,-10,5], par1_Poly2_category_pT_j1_30_5[0.3,-5,5])",
            "SS_Sref_value": 9.2,
            "SS_value": 35.2
            },
        "bin_6": {
            "function": "EXPR::bkg_pow_category_pT_j1_30_6('pow(@0, @1)', :observable:, par1_pow_category_pT_j1_30_6[-1.7,-10,10])",
            "SS_Sref_value": 34.7,
            "SS_value": 5.9
            }
        },
    "N_j_30": {
        "bin_1": {
            "function": "EXPR::bkg_expPoly2_category_N_j_30_1('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_N_j_30_1[-3.90,-10,10], par1_Poly2_category_N_j_30_1[0.87,-10,10])",
            "SS_Sref_value": 9.4,
            "SS_value": 308.4
            },
        "bin_2": {
            "function": "EXPR::bkg_expPoly2_category_N_j_30_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_N_j_30_2[-3.19,-10,10], par1_Poly2_category_N_j_30_2[0.57,-10,10])",
            "SS_Sref_value": 3.0,
            "SS_value": 52.6
            },
        "bin_3":{
            "function": "EXPR::bkg_expPoly2_category_N_j_30_3('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_N_j_30_3[-2.79,-10,10], par1_Poly2_category_N_j_30_3[0.43,-10,10])",
            "SS_Sref_value": 3.2,
            "SS_value": 25.2
            },
        "bin_4": {
            "function": "EXPR::bkg_expPoly2_category_N_j_30_4('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_N_j_30_4[-2.48,-10,10], par1_Poly2_category_N_j_30_4[0.34,-10,10])",
            "SS_Sref_value": 18.2,
            "SS_value": 65.9
            }
        },
    "m_jj_30": {
        "bin_0": {
            "function": "EXPR::bkg_expPoly2_category_m_j_30_0('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_m_jj_30_0[-3,-10,10], par1_Poly2_category_m_jj_30_0[1,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_1": {
            "function": "EXPR::bkg_pow_category_m_jj_30_1('pow(@0, @1)', :observable:, par1_pow_category_m_jj_30_1[-4.59,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_2": {
            "function": "EXPR::bkg_expPoly2_category_m_j_30_2('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_m_jj_30_2[-4.3,-10,10], par1_Poly2_category_m_jj_30_2[1.17,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_3": {
            "function": "EXPR::bkg_pow_category_m_jj_30_3('pow(@0, @1)', :observable:, par1_pow_category_m_jj_30_3[-4.69,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            },
        "bin_4": {
            "function": "EXPR::bkg_expPoly2_category_m_j_30_4('exp((@0 - 100)/100*(@1 + @2*(@0 - 100)/100))', :observable:, par0_Poly2_cateogry_m_jj_30_4[-3.97,-10,10], par1_Poly2_category_m_jj_30_4[0.80,-10,10])",
            "SS_Sref_value": 1,
            "SS_value": 0
            }
        }
    }
