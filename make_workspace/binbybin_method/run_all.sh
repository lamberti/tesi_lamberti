#!/bin/bash

for d in ./*/
do
    echo $d
    (cd $d; XMLReader -x combWS.xml)
done
