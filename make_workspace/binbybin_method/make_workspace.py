from lxml import etree
from lxml.etree import Element
import numpy as np
import pandas as pd
from glob import glob
import os
import logging
from collections import OrderedDict
import argparse
from argparse import RawTextHelpFormatter

import sys
sys.path.append('..')
import make_workspace__Lib as confLib
sys.path.append('../..')
from libraries import definitions
from common import common

logging.basicConfig(level=logging.DEBUG)

doctype_combination = "<!DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'>"
doctype_channel = "<!DOCTYPE Channel SYSTEM 'AnaWSBuilder.dtd'>"
doctype_model = "<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>"
doctype_SampleItems = "<!DOCTYPE SampleItems SYSTEM 'AnaWSBuilder.dtd'>"


def make_combination(cfactors, xsections, reco_names, output_ws_name, base_dir, blind):
    root = Element('Combination')
    root.set('DataName', 'combData')
    root.set('OutputFile', output_ws_name)
    root.set('WorkspaceName', 'combWS')
    root.set('ModelConfigName', 'ModelConfig')
    if blind:
        root.set('Blind', 'True')

    #Inputs & POI definitions
    #If xsection = 0 in a single bin, the bin is considered empty and then removed from POI array
    mu_list = ['mu', 'mu_yy', 'mu_BR_yy']
    for xsection, bin_reco in zip(xsections, reco_names):
        if not xsection == 0:
            input_element = etree.SubElement(root, 'Input').text = 'config/channel/%s.xml' % bin_reco
            mu_list.append('mu_xsec_%s' % bin_reco.replace('category_', ''))
        else: continue
    logging.info('POI: %s' % mu_list)
    etree.SubElement(root, 'POI').text = ','.join(mu_list)
    root.append(etree.Comment(" <Correlate>mu,mH,lumi_run2</Correlate> "))

    #Asimov & AsimovSB definitions
    AsimovB = etree.SubElement(root, 'Asimov')
    AsimovB.set('Name', 'AsimovB')
    AsimovB.set('Setup', 'mu=0')
    AsimovB.set('Action', 'fixsyst:fit:genasimov:float')
    AsimovSB = etree.SubElement(root, 'Asimov')
    AsimovSB.set('Name', 'AsimovSB')
    AsimovSB.set('Setup', 'mu=1')
    AsimovSB.set('Action', 'genasimov:savesnapshot')
    AsimovSB.set('SnapshotAll', 'prefit')
    
    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_combination)
    #print(text)
    f = open(os.path.join(base_dir, 'combWS.xml'), 'wb')
    f.write(text)

def make_channel(category_name, reco_bins_name, reco_category, ireco, lumi_run2, xsections, efficiencies, base_dir, blind, nmass_bins=220):
    root = Element('Channel')
    root.set('Name', category_name)
    root.set('Type', 'shape')
    root.set('Lumi', '1.00')

    #Define dataset, observable and eventual blinded region
    data = etree.SubElement(root, 'Data')
    data.set('Observable', 'atlas_invMass_{category_name}[105,160]'.format(category_name=category_name))
    data.set('InjectGhost', '1')
    data.set('Binning', '%d' % nmass_bins)
    data.set('InputFile', 'data/mass_points_%s_bin%s.txt' % (category_name.replace("category_", "").replace("%s_%s" % (quantity, category_name.split("_")[-1]), "%s" % quantity), category_name.split("_")[-1]))
    if blind:
        data.set('BlindRange', '120,130')
        
    item_mass = etree.SubElement(root, 'Item')
    item_mass.set('Name', 'mH[125.09]')

    #Global yield systematics definition
    global_yield_systematics = {'ATLAS_lumi_run2_corr': [0.0151, 'logn', 'yield'],
                               'ATLAS_lumi_run2_uncorr': [0.0127, 'logn', 'yield'],
                               'ATLAS_Hgg_Trigger': [0.004, 'logn', 'yield']}

    root.append(etree.Comment('Global Yield Systematics'))
    for syst_name in global_yield_systematics:
        syst_element = etree.SubElement(root, 'Systematic')
        syst_element.set('Name', syst_name)
        syst_element.set('Constr', global_yield_systematics[syst_name][1])
        syst_element.set('CentralValue', '1')
        syst_element.set('Mag', str(global_yield_systematics[syst_name][0]))
        syst_element.set('WhereTo', 'yield')

    #Global shape systematics definition
    global_shape_systematics = {'ATLAS_MSS_lhchcMass': [0.00192, 'gaus']}
    for syst_name, syst_mag, syst_constr in shape_systematics_parameters:
        global_shape_systematics['ATLAS_%s' % syst_name.replace('__1up', '')] = [syst_mag, syst_constr]
        
    root.append(etree.Comment("Global Shape Systematics"))
    for syst_name in global_shape_systematics:
        syst_element = etree.SubElement(root, 'Systematic')
        syst_element.set('Name', syst_name)
        syst_element.set('Constr', global_shape_systematics[syst_name][1])
        syst_element.set('CentralValue', '1')
        syst_element.set('Mag', str(global_shape_systematics[syst_name][0]))
        syst_element.set('WhereTo', 'shape')

    #Final product shape & yield systematic definition
    MSS_prod_string = 'prod::resp_MSS('
    for syst_name in global_shape_systematics.keys():
        if 'EG_RESOLUTION' in syst_name:
            continue
        else:
            MSS_prod_string += 'response::%s,' % syst_name
    MSS_prod_string += ')'
    MSS_prod_string = MSS_prod_string.replace(',)', ')')
    item_scale_response = etree.SubElement(root, 'Item')
    item_scale_response.set('Name', MSS_prod_string)

    MRS_prod_string = 'prod::resp_MRS('
    for syst_name in global_shape_systematics.keys():
        if 'EG_RESOLUTION' in syst_name:
            MRS_prod_string += 'response::%s,' % syst_name
    MRS_prod_string += ')'
    MRS_prod_string = MRS_prod_string.replace(',)', ')')
    item_scale_response = etree.SubElement(root, 'Item')
    item_scale_response.set('Name', MRS_prod_string)

    #Sample definition
    sample = etree.SubElement(root, 'Sample')
    sample.set('Name', category_name.split('category_')[1])
    sample.set('InputFile', 'config/model/signal_%s.xml' % category_name)
    sample.set('ImportSyst', ':common:')
    sample.set('SharePdf', 'commonSig')
    sample.set('XSection', '1')
    sample.set('SelectionEff', '1')
    sample.set('MultiplyLumi', '1')

    sampleItems = etree.SubElement(sample, 'ImportItems')
    sampleItems.set('FileName', 'config/syst/scale/SampleItems_%s.xml' % (reco_category))

    #NormFactors for Samples definition
    norm_factors = OrderedDict([
            ('lumi_run2', lumi_run2),
            ('mu', 1.0), ('mu_BR_yy', 1.0), ('mu_yy', 1.0), ('mu_xsec_%s' % reco_category.split('category_')[-1], 1.0),
            ('c_factor_{reco}'.format(reco=category_name.split('category_')[-1]), cfactors[ireco]),
            ('XS_{bin}'.format(bin=category_name.split('category_')[-1]), xsections[ireco]),
            ('BR_yy', 2.27E-3)
            ])
    for name, value in norm_factors.items():
        nf = etree.SubElement(sample, 'NormFactor')
        nf.set('Name', '%s[%.10f]' % (name, value))
        nf.set('Correlate', '1')

    #Spurious signal
    best_bkg_dict = confLib.spur_signal_best_functions
    SS_Sref_value = best_bkg_dict[quantity]["bin_%s" % category_name.split("_")[-1]]["SS_Sref_value"]
    SS_value = best_bkg_dict[quantity]["bin_%s" % category_name.split("_")[-1]]["SS_value"]
    NsigScaled_value = (float(SS_value) / float(SS_Sref_value)) / float(lumi_run2)
    sample = etree.SubElement(root, 'Sample')
    sample.set('Name', 'spurious')
    sample.set('InputFile', 'config/model/signal_%s.xml' % category_name)
    sample.set('ImportSyst', ':self:')
    sample.set('SharePdf', 'commonSig')
    sample.set('XSection', '1')
    sample.set('SelectionEff', '1')
    sample.set('MultiplyLumi', '1')
    syst_element = etree.SubElement(sample, 'Systematic')
    syst_element.set('Name', 'ATLAS_Hgg_Bias_%s' % category_name)
    syst_element.set('Constr', 'gaus')
    syst_element.set('CentralValue', '0')
    syst_element.set('Mag', '%s' % SS_Sref_value)
    syst_element.set('WhereTo', 'yield')
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'NsigScaled_%s[%.10f]' % (category_name, NsigScaled_value))
    nf.set('Correlate', '1')
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'lumi_run2[%.10f]' % lumi_run2)
    nf.set('Correlate', '1')

    #Background
    sample = etree.SubElement(root, 'Sample')
    sample.set('Name', 'background')
    sample.set('InputFile', 'config/model/background_%s.xml' % category_name)
    #sample.set('InportSyst', ':self:')
    sample.set('XSection', '1')
    sample.set('SelectionEff', '1')
    sample.set('MultiplyLumi', '1')
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'bkg_%s[3655.22,0,1000000]' % category_name)
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'expr::adjust_LUMI("@0/@1",lumi_run2[%.10f],ref_lumi[140000])' % lumi_run2)
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'NFcontBkg[1.0]')
    nf.set('Correlate', '1')

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_channel)
    f = open(os.path.join(base_dir, 'config/channel/%s.xml' % category_name), 'wb')
    f.write(text)

def make_SampleItems(observable, base_dir, reco_bin):
    root = Element('SampleItems')

    #Experimental yield systematics definitions
    root.append(etree.Comment('Experimental Yield Systematics'))
    for syst_name in glob('../../output/cfactors_sysvar/%s/np_arrays/*' % observable):
        f_syst_file = np.load(syst_name)
        if (np.abs(f_syst_file).sum() < 0.007):
            logging.info('Skipping systematic %s --> too small' % syst_name.split('/')[-1].replace('cfactor_sys', 'ATLAS'))
            continue
        else:
            syst_value = f_syst_file[reco_bin]
            if "Theoretical" in syst_name:
                systematic = etree.SubElement(root, "Systematic")
                systematic.set("Name", "%s" % syst_name.split("/")[6].split("_reco_bin")[0].split("__")[0].replace("cfactor_sys", "ATLAS"))
                systematic.set("Constr", "asym")
                systematic.set("Mag", "%f, %f" % (-syst_value, syst_value))
                systematic.set("CentralValue", "1")
                systematic.set("WhereTo", "yield")
            else:
                systematic = etree.SubElement(root, "Systematic")
                systematic.set("Name", "%s" % syst_name.split("/")[6].split("__1upAuxDyn")[0].split("__")[0].replace("cfactor_sys", "ATLAS"))
                systematic.set("Constr", "asym")
                systematic.set("Mag", "%f, %f" % (-syst_value, syst_value))
                systematic.set("CentralValue", "1")
                systematic.set("WhereTo", "yield")
    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_SampleItems)
    #print(text)
    f = open(os.path.join(base_dir, 'config/syst/scale/SampleItems_category_%s_%d.xml' % (observable, reco_bin)), 'wb')
    f.write(text)

def make_signal(observable, category, base_dir):
    #Create list of parameters
    paramPath =  '../../output/signalParametrisation/%s/%s_bin_%s/resonance_paramList.txt' % (quantity, quantity, category)
    all_dfs_cat = {}
    for i in range(0, 6):
        param = pd.read_csv(paramPath, sep=' ', index_col=0, header=None)[1][i]
        all_dfs_cat[definitions.parameters_names[i]] = param
        all_dfs = pd.Series(all_dfs_cat)
        df_parameters = pd.DataFrame({'value' :all_dfs})
        df_parameters.index.names = ["parameters"]

    root = Element("Model")
    root.set("Type", "UserDef")
    root.set("CacheBinning", "100000")
    etree.SubElement(root, "Item").set("Name", "expr::muCBFn( '(@0 + (@1-125.0))', muCBNom[%.3f], mH)" % df_parameters.loc["mu_CB_Nom"]["value"])
    etree.SubElement(root, "Item").set("Name", "sigmaCBFn[%.5f]" % df_parameters.loc["sigma_CB_Nom"]["value"])
    etree.SubElement(root, "Item").set("Name", "alphaCBLo[%.4f]" % df_parameters.loc["alpha_CB_Lo"]["value"])
    etree.SubElement(root, "Item").set("Name", "alphaCBHi[%.5f]" % df_parameters.loc["alpha_CB_Hi"]["value"])
    etree.SubElement(root, "Item").set("Name", "nCBLo[%.5f]" % df_parameters.loc["n_CB_Lo"]["value"])
    etree.SubElement(root, "Item").set("Name", "nCBHi[%.4f]" % df_parameters.loc["n_CB_Hi"]["value"])
    etree.SubElement(root, "ModelItem").set("Name", "RooTwoSidedCBShape::signal(:observable:, prod::muCB(muCBFn, resp_MSS), prod::sigmaCB(sigmaCBFn, resp_MRS), alphaCBLo, nCBLo, alphaCBHi, nCBHi)")

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_model)
    f = open(os.path.join(base_dir, 'config/model/signal_category_%s_%s.xml' % (observable, category)), 'wb')
    f.write(text)

def make_background(observable, reco_category, ibin, base_dir):
    best_bkg_dict = confLib.spur_signal_best_functions
    etree.Comment("DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'")
    root = Element("Model")
    root.set("Type", "UserDef")
    etree.SubElement(root, "ModelItem").set("Name", "%s" % best_bkg_dict[observable]["bin_%s" % reco_category.split("_")[-1]]["function"])
    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_model)
    f = open(os.path.join(base_dir, 'config/model/background_category_%s_%s.xml' % (observable, ibin)), 'wb')
    f.write(text)

def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dump yield for response matrix',
                                 formatter_class=RawTextHelpFormatter,
                                 epilog='''
                                 Example: python make_workspace.py --blind True/False''')
    parser.add_argument('quantity', type=str, help='create xml for quantity')
    parser.add_argument('--blinded', action='store_true', help='generate for fitting blinded or unblinded dataset')
    args = parser.parse_args()

    
    if args.quantity == 'ALL':
        observablesList = common.quantities
    else:
        observablesList = [args.quantity]
    
    systematicsList_shape = ['EG_SCALE_ALL__1up', 'EG_RESOLUTION_ALL__1up']
    systematicsList_shape_constr = ['gaus', 'gaus']

    output_basedir = '../../../output/binbybin_unfolding'
    create_folder_if_not_exists(output_basedir)
    
    for quantity in observablesList:
        logging.info('Creating xml files for %s observable' % quantity)
        
        #Create output directory
        if args.blinded:
            directory = 'workspace_%s_blinded' % quantity
            blinding = 'blinded'
        else:
            directory = 'workspace_%s_unblinded' % quantity
            blinding = 'unblinded'
        logging.info('output directory: %s' % directory)
        create_folder_if_not_exists(directory)
        os.system('cp AnaWSBuilder.dtd %s' % directory)
        create_folder_if_not_exists(os.path.join(directory, 'config'))
        create_folder_if_not_exists(os.path.join(directory, 'config/channel'))
        os.system('cp AnaWSBuilder.dtd %s' % os.path.join(directory, 'config/channel'))
        create_folder_if_not_exists(os.path.join(directory, 'config/model'))
        os.system('cp AnaWSBuilder.dtd %s' % os.path.join(directory, 'config/model'))
        create_folder_if_not_exists(os.path.join(directory, 'config/syst/scale'))
        os.system('cp AnaWSBuilder.dtd %s' % os.path.join(directory, 'config/syst/scale'))
        data_directory = '../../../output/data/{blind}/{quantity}'.format(blind=blinding, quantity=quantity)
        try:
            os.unlink(os.path.join(directory, 'data'))
        except:
            pass
        os.symlink(data_directory, os.path.join(directory, 'data'))

        lumi_run2 = 138972
        logging.info('Luminosity %d pb^-1' % lumi_run2)

        #Get cfactors array 
        cfactors_path = '../../output/cfactors/%s' % quantity
        cfactors_name = 'cfactors_isFiducial_notDalitz_%s_mcAll_prodAll_Nominal.npy' % quantity
        cfactors = np.load(os.path.join(cfactors_path, cfactors_name))
        logging.info('cfactors taken from file: %s' % os.path.join(cfactors_path, cfactors_name))
        logging.info('cfactors array: %s' % cfactors)

        #Get reco categories
        nreco_bins = len(cfactors)
        reco_bins_name = ['category_%s_%d' % (quantity, ibin) for ibin in range(nreco_bins)]
        #logging.info('reco categories: %s' % reco_bins_name)

        #Get xsections array
        xsections_path = '../../output/xsections/%s' % quantity
        xsections_name = "XSections_isFiducial_notDalitz_%s_mcAll_prodAll_Nominal.npy" % quantity
        xsections = np.load(os.path.join(xsections_path, xsections_name))
        logging.info('xsections taken from file: %s' % os.path.join(xsections_path, xsections_name))
        logging.info('xsections array: %s' %  xsections)
        
        output_ws_name = os.path.join(directory, "XSectionWS_%s.root" % quantity)

        make_combination(cfactors,
                         xsections,
                         reco_bins_name,
                         output_ws_name=output_ws_name,
                         base_dir=directory,
                         blind=args.blinded)
        logging.info('CombCard.xml created')

        shape_systematics_parameters = []
        for ireco, reco_category in enumerate(reco_bins_name):
            if xsections[ireco] == 0:
                continue
            else:
                for syst_name, syst_constr in zip(systematicsList_shape, systematicsList_shape_constr):
                    
                    #Get shape systematic array
                    f_syst_path = '../../output/scale_resolution_systematics/%s' % quantity
                    f_syst_name = 'ATLAS_%s_systematic.npy' % syst_name
                    f_syst = np.load(os.path.join(f_syst_path, f_syst_name))
                    shape_systematics_parameters.append([syst_name, f_syst[ireco], syst_constr])
                    
                    make_channel(reco_category,
                                 reco_bins_name,
                                 reco_category,
                                 ireco,
                                 xsections=xsections,
                                 efficiencies=cfactors[ireco],
                                 base_dir=directory,
                                 lumi_run2=lumi_run2,
                                 blind=args.blinded)
        logging.info('Channels created')

        for ireco, reco_category in enumerate(reco_bins_name):
            if xsections[ireco] == 0:
                continue
            else:
                logging.info('Creating sampleItem for bin %d' % ireco)
                make_SampleItems(quantity,
                                 directory,
                                 ireco)
        logging.info('SampleItems created')
        
        for ireco, reco_category in enumerate(reco_bins_name):
            if xsections[ireco] == 0:
                continue
            else:
                make_signal(quantity,
                            ireco,
                            base_dir=directory)
        logging.info('Signals created')
        
        for ireco, reco_category in enumerate(reco_bins_name):
            if xsections[ireco] == 0:
                continue
            else:
                make_background(quantity,
                                reco_category,
                                ireco,
                                base_dir=directory)
        logging.info('Backgrounds created')
