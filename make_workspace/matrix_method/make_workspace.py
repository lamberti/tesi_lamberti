from lxml import etree
from lxml.etree import Element
import numpy as np
import pandas as pd
import os
from glob import glob
import sys
sys.path.append("../../libraries")
sys.path.append("..")
sys.path.append("../../")
from common import common
import make_workspace__Lib as confLib

import logging
logging.basicConfig(level=logging.DEBUG)

doctype_combination = "<!DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'>"
doctype_channel = "<!DOCTYPE Channel SYSTEM 'AnaWSBuilder.dtd'>"
doctype_model = "<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>"
doctype_SampleItems = "<!DOCTYPE SampleItems SYSTEM 'AnaWSBuilder.dtd'>"


def make_combination(reco_names, true_names, output_ws_name, base_dir, blind):

    root = Element("Combination")

    root.set("DataName", "combData")
    root.set("OutputFile", output_ws_name)
    root.set("WorkspaceName", "combWS")
    root.set("ModelConfigName", "ModelConfig")
    root.set("Blind", str(blind))

    for bin_reco in reco_names:
        etree.SubElement(root, "Input").text = "config/channel/%s.xml" % bin_reco

    mu_list = ['mu', 'mu_yy', 'mu_BR_yy']
    for bin_true in true_names:
        mu_list.append('mu_xsec_%s' % bin_true)
    etree.SubElement(root, "POI").text = ','.join(mu_list)

    root.append(etree.Comment(" <Correlate>mu,mH,lumi_run2</Correlate> "))
    if blind:
        AsimovB = etree.SubElement(root, "Asimov")
        AsimovB.set("Name", "AsimovB")
        AsimovB.set("Setup", "mu=0")
        AsimovB.set("Action", "fixsyst:fit:genasimov:float")
        AsimovSB = etree.SubElement(root, "Asimov")
        AsimovSB.set("Name", "AsimovSB")
        AsimovSB.set("Setup", "mu=1")
        AsimovSB.set("Action", "genasimov:savesnapshot")
        AsimovSB.set("SnapshotAll", "prefit")
    else:
        AsimovSB = etree.SubElement(root, "Asimov")
        AsimovSB.set("Name", "AsimovSB")
        AsimovSB.set("Setup", "mu=1")
        AsimovSB.set("Action", "fixsyst:fit:genasimov:float")
        AsimovSB.set("SnapshotAll", "prefit")
        AsimovB = etree.SubElement(root, "Asimov")
        AsimovB.set("Name", "AsimovB")
        AsimovB.set("Setup", "mu=0")
        AsimovB.set("Action", "genasimov:savesnapshot:reset")

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_combination)
    f = open(os.path.join(base_dir, 'combWS.xml'), 'wb')
    f.write(text)


def make_channel(quantity, category_name, true_bins_name, data_path,
                 lumi_run2, xsections, efficiencies, br_dalitz,
                 global_yield_systematics,
                 global_mss_systematics, global_msr_systematics,
                 base_dir, nmass_bins=220):

    assert len(true_bins_name) == len(xsections)
    assert len(true_bins_name) == len(efficiencies)

    channel_el = Element("Channel")
    channel_el.set("Name", category_name)
    channel_el.set("Type", "shape")
    channel_el.set("Lumi", "1.00")

    data = etree.SubElement(channel_el, "Data")
    data.set("Observable", "atlas_invMass_{category_name}[105,160]".format(category_name=category_name))
    data.set("InjectGhost", "1")
    data.set("Binning", "%d" % nmass_bins)
    data.set("InputFile", data_path)
    data.set("BlindRange", "120,130")

    etree.SubElement(channel_el, "Item").set("Name", "mH[125.09]")

    channel_el.append(etree.Comment("Global Yield Systematics"))

    for syst_name in global_yield_systematics:
        syst_element = etree.SubElement(channel_el, "Systematic")
        syst_element.set("Name", syst_name)
        syst_element.set("Constr", global_yield_systematics[syst_name][1])
        syst_element.set("CentralValue", "1")
        syst_element.set("Mag", str(global_yield_systematics[syst_name][0]))
        syst_element.set("WhereTo", "yield")

    channel_el.append(etree.Comment("Global Shape Systematics"))

    for syst_name in global_mss_systematics:
        syst_element = etree.SubElement(channel_el, "Systematic")
        syst_element.set("Name", syst_name)
        syst_element.set("Constr", global_mss_systematics[syst_name][1])
        syst_element.set("CentralValue", "1")
        syst_element.set("Mag", str(global_mss_systematics[syst_name][0]))
        syst_element.set("WhereTo", "shape")

    for syst_name in global_msr_systematics:
        syst_element = etree.SubElement(channel_el, "Systematic")
        syst_element.set("Name", syst_name)
        syst_element.set("Constr", global_msr_systematics[syst_name][1])
        syst_element.set("CentralValue", "1")
        syst_element.set("Mag", str(global_msr_systematics[syst_name][0]))
        syst_element.set("WhereTo", "shape")

    rest_mss_el = etree.SubElement(channel_el, "Item")
    expression_mss = ','.join(['response::{sysname}'.format(sysname=sysname) for sysname in global_mss_systematics])
    expression_mss = "prod::resp_MSS(%s)" % expression_mss
    rest_mss_el.set("Name", expression_mss)

    rest_msr_el = etree.SubElement(channel_el, "Item")
    expression_msr = ','.join(['response::{sysname}'.format(sysname=sysname) for sysname in global_msr_systematics])
    expression_msr = "prod::resp_MRS(%s)" % expression_msr
    rest_msr_el.set("Name", expression_msr)

    for itrue, true_bin_name in enumerate(true_bins_name):
        efficiency = efficiencies[itrue]
        if efficiency < 0:
            logging.warning("efficiency for reco={reco}, true={true} is negative: {value}".format(
                reco=category_name, true=true_bin_name, value=efficiency
            ))
            channel_el.append(etree.Comment("Skipping true-bin %s, eff:%f" % (true_bin_name, efficiency)))
            continue
        if efficiency < 0.000003:
            channel_el.append(etree.Comment("Skipping true-bin %s, eff:%f" % (true_bin_name, efficiency)))
            continue

        if xsections[itrue] == 0:
            logging.info("cross section for true=%s is 0, skipping" % true_bin_name)
            channel_el.append(etree.Comment("Skipping true-bin %s, xsection:0" % true_bin_name))
            continue

        sample = etree.SubElement(channel_el, "Sample")
        sample.set("Name", true_bin_name)
        sample.set("InputFile", "config/model/signal_%s.xml" % category_name)
        sample.set("ImportSyst", ":common:")
        sample.set("SharePdf", "commonSig")
        sample.set("XSection", "1")
        sample.set("SelectionEff", "1")
        sample.set("MultiplyLumi", "1")

        sys_include = etree.SubElement(sample, "ImportItems")
        sys_include.set("FileName", "config/sys_yield/true_{true_name}__reco_{reco_name}.xml".format(true_name=true_bin_name, reco_name=category_name))

        norm_factors = [('mu', 1.), ('mu_yy', 1.),
                        ('lumi_run2', lumi_run2),
                        ('XS_%s' % true_bin_name, xsections[itrue]),
                        ('eff_reco_%s__true_%s' % (category_name, true_bin_name), efficiency)
                        ]
        if itrue == 0:  # not fiducial
            norm_factors += [('mu_BR_yy', 1.)]
            norm_factors += [('Br', common.Br)]
        elif itrue == 1:  # dalitz
            norm_factors += [('Br_dalitz', br_dalitz)]
        else:  # fiducial
            norm_factors += [('mu_BR_yy', 1.)]
            norm_factors += [('Br', common.Br)]
            norm_factors += [('mu_xsec_%s' % true_bin_name, 1.)]
            pass

        for name, value in norm_factors:
            nf_element = etree.SubElement(sample, "NormFactor")
            nf_element.set("Name", "%s[%.10f]" % (name, value))
            nf_element.set("Correlate", "1")

    f_data = open(os.path.join(base_dir, data_path))
    ndata = len(f_data.readlines())

    #Spurious signal
    best_bkg_dict = confLib.spur_signal_best_functions
    SS_Sref_value = best_bkg_dict[quantity]["bin_%s" % category_name.split("_")[-1]]["SS_Sref_value"]
    SS_value = best_bkg_dict[quantity]["bin_%s" % category_name.split("_")[-1]]["SS_value"]
    NsigScaled_value = (float(SS_value) / float(SS_Sref_value)) / float(lumi_run2)
    sample = etree.SubElement(channel_el, 'Sample')
    sample.set('Name', 'spurious')
    sample.set('InputFile', 'config/model/signal_%s.xml' % category_name)
    sample.set('ImportSyst', ':self:')
    sample.set('SharePdf', 'commonSig')
    sample.set('XSection', '1')
    sample.set('SelectionEff', '1')
    sample.set('MultiplyLumi', '1')
    syst_element = etree.SubElement(sample, 'Systematic')
    syst_element.set('Name', 'ATLAS_Hgg_Bias_%s' % category_name)
    syst_element.set('Constr', 'gaus')
    syst_element.set('CentralValue', '0')
    syst_element.set('Mag', '%s' % SS_Sref_value)
    syst_element.set('WhereTo', 'yield')
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'NsigScaled_%s[%.10f]' % (category_name, NsigScaled_value))
    nf.set('Correlate', '1')
    nf = etree.SubElement(sample, 'NormFactor')
    nf.set('Name', 'lumi_run2[%.10f]' % lumi_run2)
    nf.set('Correlate', '1')

    sample = etree.SubElement(channel_el, "Sample")
    sample.set("Name", "background")
    sample.set("InputFile", "config/model/background_%s.xml" % category_name)
    #sample.set("InportSyst", ":self:")
    sample.set("XSection", "1")
    sample.set("SelectionEff", "1")
    sample.set("MultiplyLumi", "1")
    bkg_normfactors = [
        "n_bkg_{name}[{value},0,{value_max}]".format(name=category_name, value=ndata, value_max=max(200000, ndata * 5)),
        "expr::adjust_LUMI('@0/@1',lumi_run2,ref_lumi[{lumi}])".format(lumi=lumi_run2),
    ]

    for nf in bkg_normfactors:
        nf_el = etree.SubElement(sample, "NormFactor")
        nf_el.set("Name", nf)
    nf_el = etree.SubElement(sample, "NormFactor")
    nf_el.set("Name", 'NFcontBkg[1.0]')
    nf_el.set("Correlate", "1")

    text = etree.tostring(channel_el, pretty_print=True, xml_declaration=False, doctype=doctype_channel)
    f = open(os.path.join(base_dir, 'config/channel/%s.xml' % category_name), 'wb')
    f.write(text)


def make_signal(reco_category, params_CB, base_dir):
    root = Element("Model")
    root.set("Type", "UserDef")
    root.set("CacheBinning", "100000")

    etree.SubElement(root, "Item").set("Name", "muCBNom[%.5f]" % params_CB[0])
    etree.SubElement(root, "Item").set("Name", "sigmaCBFn[%.5f]" % params_CB[1])
    etree.SubElement(root, "Item").set("Name", "alphaCBLo[%.4f]" % params_CB[2])
    etree.SubElement(root, "Item").set("Name", "alphaCBHi[%.5f]" % params_CB[3])
    etree.SubElement(root, "Item").set("Name", "nCBLo[%.5f]" % params_CB[4])
    etree.SubElement(root, "Item").set("Name", "nCBHi[%.4f]" % params_CB[5])
    etree.SubElement(root, "Item").set("Name", "expr::muCBFn( '(@0 + (@1-125.0))', muCBNom, mH)")
    etree.SubElement(root, "ModelItem").set("Name", "RooTwoSidedCBShape::signal(:observable:, prod::muCB(muCBFn, resp_MSS), prod::sigmaCB(sigmaCBFn, resp_MRS), alphaCBLo, nCBLo, alphaCBHi, nCBHi)")

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_model)
    f = open(os.path.join(base_dir, 'config/model/signal_{category}.xml'.format(category=reco_category)), 'wb')
    f.write(text)


def make_background(reco_category, expression, base_dir):
    etree.Comment("DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'")
    root = Element("Model")
    root.set("Type", "UserDef")
    etree.SubElement(root, "ModelItem").set("Name", expression)

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_model)
    f = open(os.path.join(base_dir, 'config/model/background_{category}.xml'.format(category=reco_category)), 'wb')
    f.write(text)


def make_yield_systematic(observable, base_dir, reco_name, true_name, sys_dict):
    root = Element("SampleItems")
    root.append(etree.Comment("Experimental Yield Systematics"))

    for syst_name in sys_dict:
        syst_value = sys_dict[syst_name]
        if (syst_value == 0):
            continue

        systematic = etree.SubElement(root, "Systematic")
        systematic.set("Name", syst_name)
        systematic.set("Constr", "asym")
        systematic.set("Mag", "%f, %f" % (-syst_value, syst_value))
        systematic.set("CentralValue", "1")
        systematic.set("WhereTo", "yield")

    text = etree.tostring(root, pretty_print=True, xml_declaration=False, doctype=doctype_SampleItems)
    f = open(os.path.join(base_dir, "config/sys_yield/true_{true_name}__reco_{reco_name}.xml".format(true_name=true_name, reco_name=reco_name)), 'wb')
    f.write(text)


def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def create_directory_structure(directory, directory_data):
    create_folder_if_not_exists(directory)
    os.system("cp AnaWSBuilder.dtd %s" % directory)
    create_folder_if_not_exists(os.path.join(directory, 'config'))
    create_folder_if_not_exists(os.path.join(directory, 'config/channel'))
    os.system("cp AnaWSBuilder.dtd %s" % os.path.join(directory, 'config/channel'))
    create_folder_if_not_exists(os.path.join(directory, 'config/model'))
    os.system("cp AnaWSBuilder.dtd %s" % os.path.join(directory, 'config/model'))
    create_folder_if_not_exists(os.path.join(directory, 'config/sys_yield'))
    os.system("cp AnaWSBuilder.dtd %s" % os.path.join(directory, 'config/sys_yield'))
    try:
        os.unlink(os.path.join(directory, 'data'))
    except FileNotFoundError:
        pass
    os.symlink(os.path.join('..', directory_data), os.path.join(directory, 'data'))


def prune_vector(x, idx_to_remove):
    if not idx_to_remove:
        return x
    return [x[i] for i in range(len(x)) if i not in idx_to_remove]


def prune_matrix(matrix, idx_to_remove):
    if not idx_to_remove:
        return matrix
    result = np.copy(matrix)
    idx_to_remove = np.array(idx_to_remove)
    result = np.delete(result, idx_to_remove + 2, 0)     # true axis
    result = np.delete(result, idx_to_remove, 1)         # reco axis
    return result


def make_xmls(quantity, blinded):
    blinded_name = {True: 'blinded', False: 'unblinded'}[blinded]

    logging.info("Creating xml files for %s observable, blinded %s", quantity, blinded)

    directory = 'workspace_{quantity}_{blinded}'.format(quantity=quantity, blinded=blinded_name)
    directory_data = '../../output/data/{blinded}/{quantity}'.format(quantity=quantity,
                                                                     blinded=blinded_name)
    #directory_data = os.path.abspath(directory_data)
    create_directory_structure(directory, directory_data)

    # load the inputs
    lumi_run2 = common.luminosity_pb['mcAll']
    logging.info("Luminosity %f /pb", lumi_run2)

    xsections = np.load("../../output/xsections/{quantity}/XSections_isFiducial_notDalitz_{quantity}_mcAll_prodAll_Nominal.npy".format(quantity=quantity))
    logging.info("original xsections (%d): %s", len(xsections), xsections)

    br_yy = common.Br
    logging.info("branching ratio yy: %f", br_yy)

    br_dalitz = np.loadtxt("../../output/dalitz/{quantity}/br_dalitz_Nominal.txt".format(quantity=quantity))
    logging.info("branching ratio dalitz: %f", br_dalitz)

    acceptances = np.load("../../output/acceptances/{quantity}/acceptances_isFiducial_notDalitz_{quantity}_mcAll_prodAll_Nominal.npy".format(quantity=quantity))
    logging.info("acceptances (%d): %s", len(acceptances), acceptances)
    acceptance_fiducial = np.sum(acceptances)
    logging.info("fiducial acceptance: %f", acceptance_fiducial)

    migration_matrix = np.load("../../output/folding_matrices/{quantity}/np_array/folding_matrix_isPassed_for_workspace_{quantity}_mcAll_prodAll_Nominal.npy".format(quantity=quantity))

    logging.info("original matrix has shape %s", migration_matrix.shape)

    migration_matrix_sys_fn = glob("../../output/folding_matrices_sysvar/pT_yy/np_arrays/*")
    if not migration_matrix_sys_fn:
        logging.error("no systematics on the matrix")

    all_sys_matrices = {}
    for fn in migration_matrix_sys_fn:
        basename = os.path.basename(fn)
        sysname = basename.replace("variation_sys_", "").replace("__1up.npy", "")
        if 'SCALE_AF2' in sysname:
            continue
        if 'ATLAS' not in sysname:
            sysname = 'ATLAS_' + sysname
        values = np.load(fn)
        if np.mean(np.abs(values)) < 0.001:
            logging.info("skipping %s systematics, too small" % sysname)
            continue
        all_sys_matrices[sysname] = values

    all_scale_sys = np.load("../../output/scale_resolution_systematics/{quantity}/ATLAS_EG_SCALE_ALL__1up_systematic.npy".format(quantity=quantity))
    all_resolution_sys = np.load("../../output/scale_resolution_systematics/{quantity}/ATLAS_EG_RESOLUTION_ALL__1up_systematic.npy".format(quantity=quantity))

    nbins_noextraflow = common.nbins_noextraflow[quantity]

    true_bins_name = ['%s_%d' % (quantity, ibin) for ibin in range(nbins_noextraflow + 2)]  # +2 to add under/over-flow
    reco_bins_name = ['category_%s_%d' % (quantity, ibin) for ibin in range(nbins_noextraflow + 2)]
    reco_bins_number = list(range(nbins_noextraflow + 2))

    logging.info("true bins (%d): %s", len(true_bins_name), true_bins_name)
    logging.info("reco bins (%d): %s", len(reco_bins_name), reco_bins_name)

    output_ws_name = os.path.join(directory, "XSectionWS_%s.root" % quantity)
    logging.info("output root workspace: %s", output_ws_name)

    # normalize input (remove under/overflow and true bins with null cross section when needed)
    index_to_remove = [i for i in range(len(xsections)) if xsections[i] == 0]
    logging.info("removing true bin with index: %s" % index_to_remove)

    true_bins_name = prune_vector(true_bins_name, index_to_remove)
    reco_bins_name = prune_vector(reco_bins_name, index_to_remove)
    reco_bins_number = prune_vector(reco_bins_number, index_to_remove)
    xsections = prune_vector(xsections, index_to_remove)
    all_scale_sys = prune_vector(all_scale_sys, index_to_remove)
    all_resolution_sys = prune_vector(all_resolution_sys, index_to_remove)

    true_bins_name = ['%s_notFiducialnotDalitz' % quantity, "%s_isDalitz" % quantity] + true_bins_name

    migration_matrix = prune_matrix(migration_matrix, index_to_remove)

    for sys_name in all_sys_matrices:
        all_sys_matrices[sys_name] = prune_matrix(all_sys_matrices[sys_name], index_to_remove)

    logging.info("after pruning xsections (%d): %s", len(xsections), xsections)
    logging.info("after pruning matrix has shape %s", migration_matrix.shape)
    logging.info("after pruning true bins (%d): %s", len(true_bins_name), true_bins_name)
    logging.info("after pruning reco bins (%d): %s", len(reco_bins_name), reco_bins_name)
    logging.info("systematics on the matrices: %s", ','.join(all_sys_matrices.keys()))
    logging.info("scale sys: %s", all_scale_sys)
    logging.info("resolution sys: %s", all_resolution_sys)

    xsection_non_fiducial = np.sum(xsections) * (1 - acceptance_fiducial) / acceptance_fiducial
    xsection_dalitz = np.sum(xsections) + xsection_non_fiducial
    xsections = np.array([xsection_non_fiducial, xsection_dalitz] + list(xsections))
    logging.info("adding cross section for non-fiducial and dalitz: %s", xsections)

    make_combination(reco_bins_name, true_bins_name[2:], output_ws_name, directory, blind=blinded)

    global_yield_systematics = {"ATLAS_lumi_run2_corr": [0.0151, 'logn'],
                                "ATLAS_lumi_run2_uncorr": [0.0127, 'logn'],
                                "ATLAS_Hgg_Trigger": [0.004, 'logn']}

    for ireco, reco_category in enumerate(reco_bins_name):
        global_mss_systematics = {"ATLAS_MSS_lhchcMass": [0.00192, 'gaus'],
                                  "ATLAS_EG_SCALE_ALL": [all_scale_sys[ireco], 'gaus']}
        global_msr_systematics = {"ATLAS_EG_RESOLUTION_ALL": [all_resolution_sys[ireco], 'gaus']}

        data_path = "data/mass_points_{quantity}_bin{bin_number}.txt".format(quantity=quantity, bin_number=reco_bins_number[ireco])

        make_channel(quantity,
                     reco_category,
                     true_bins_name,
                     data_path=data_path,
                     xsections=xsections,
                     efficiencies=migration_matrix[:, ireco],
                     br_dalitz=br_dalitz,
                     global_yield_systematics=global_yield_systematics,
                     global_mss_systematics=global_mss_systematics,
                     global_msr_systematics=global_msr_systematics,
                     base_dir=directory,
                     lumi_run2=lumi_run2)

        params_CB_fn = '../../output/signalParametrisation/{quantity}/{quantity}_bin_{category}/resonance_paramList.txt'.format(quantity=quantity, category=ireco)
        params_CB = pd.read_csv(params_CB_fn, sep=" ", header=None)[1].values

        make_signal(reco_category,
                    params_CB=params_CB,
                    base_dir=directory)

        bkg_functions = confLib.spur_signal_best_functions[quantity]
        bkg_function = bkg_functions['bin_%d' % reco_bins_number[ireco]]['function']

        make_background(reco_category,
                        expression=bkg_function,
                        base_dir=directory)

    # yield systematic
    for ireco, reco_category in enumerate(reco_bins_name):
        for itrue, true_category in enumerate(true_bins_name):
            sys_dict = {}
            for sys_name in all_sys_matrices:
                sys_dict[sys_name] = all_sys_matrices[sys_name][itrue, ireco]
            make_yield_systematic(quantity, directory, reco_category, true_category, sys_dict)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Create xml for matrix method')
    parser.add_argument('quantity')
    parser.add_argument('--blinded', action='store_true')
    args = parser.parse_args()
    if args.quantity != "ALL":
        make_xmls(args.quantity, args.blinded)
    else:
        logging.info("doing all the workspaces")
        for quantity in common.quantities:
            logging.info("doing quantity %s" % quantity)
            make_xmls(quantity, args.blinded)
