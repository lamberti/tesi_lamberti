#ifndef HGamTools_DumpNTUP_H
#define HGamTools_DumpNTUP_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include <EventLoopAlgs/NTupleSvc.h>

class DumpNTUP : public HgammaAnalysis {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  std::vector<TString> m_sysComps;
  std::vector<TString> m_contToSave;

  void clear_variables();

  TH1 *nph_hist; //!
  TH1 *pileup_hist; //!

  bool accepted_nominal; //!
  bool pass_egamma_selection; //!

  // pass flags
  bool isPassedLowMyy; //!
  bool isPassedHighMyy; //!
  bool isPassedIsolationLowMyy; //!
  bool isPassedIsolationHighMyy; //!
  bool isPassedPtCutsLowHighMyy; //!
  bool isPassedPID; //!

  // other variables
  float averageIntPerXing; //!
  float actualIntPerXing; //!
  int run_number; //!
  unsigned long long event_number; //!
  int lumiblock_number; //!
  int time_stamp; //!
  int mc_channel_number; //!
  unsigned long long mc_event_number; //!
  bool one_photon; //!
  bool two_photons; //!

  // bcid variables
  int bcid; //!
  bool is_filled; //!
  bool in_train; //!
  int from_beam_1or2; //!
  float dist_front; //!
  float dist_tail; //!
  unsigned int bc_type; //!
  float gap_before_train; //!
  float gap_after_train; //!
  float gap_before_bunch; //!
  float gap_after_bunch; //!
  float bunch_spacing; //!

  float sumpt2_tracks; //!
  //std::vector<TLorentzVector> tracks_p4s; //!

  int npvs; //!
  float xs; //!
  float xs_ami; //!
  float filter_eff; //!
  float filter_eff_ami; //!
  float pileup_weight; //!
  float vertex_weight; //!
  float MC_weight; //!
  float xs_weight; //!
  float event_weight; //!
  float prel_weight; //!
  float total_weight; //!
  float total_weight_nopu; //!

  float SF_leading; //!
  float SF_unc_leading; //!
  float SF_subleading; //!
  float SF_unc_subleading; //!
  float SF_diphoton; //!
  float SF_1UP_diphoton; //!
  float SF_1DOWN_diphoton; //!

  int total_events; //!
  int non_derived_total_events; //!

  double ED_central; //!
  double ED_forward; //!

  float raw_energy_leading; //!
  float raw_energy_subleading; //!

  float raw_et_leading; //!
  float raw_et_subleading; //!

  float e_on_eraw_leading; //!
  float e_on_eraw_subleading; //!

  bool pass_2g50_loose_trigger; //!
  bool pass_g35_g25_loose_trigger; //!

  float mass_raw; //!

  float deltaR; //!

  int max_cell_gain_leading; //!
  float max_cell_time_leading; //!
  float max_cell_e_leading; //!

  int max_cell_gain_subleading; //!
  float max_cell_time_subleading; //!
  float max_cell_e_subleading; //!

  float Rconv_leading; //!
  float Rconv_subleading; //!

  float E0_leading; //!
  float E1_leading; //!
  float E2_leading; //!
  float E3_leading; //!
  float E0_subleading; //!
  float E1_subleading; //!
  float E2_subleading; //!
  float E3_subleading; //!

  float Zconv_leading; //!
  float Zconv_subleading; //!

  int photonOQ_leading; //!
  int photonOQ_subleading; //!

  float DD_corr_40_leading; //!
  float DD_corr_40_subleading; //!
  float DD_corr_20_leading; //!
  float DD_corr_20_subleading; //!

  float PVz; //!

  int category; //!

  // pass flags
  std::vector< std::pair<std::string, bool> > pass_flags; //!

  // isEM flags
  std::vector< std::pair<std::string, int> > isEM_flags_lead; //!
  std::vector< std::pair<std::string, int> > isEM_flags_sublead; //!

  std::vector< std::pair<std::string, int> > isEM_flags_nofudge_lead; //!
  std::vector< std::pair<std::string, int> > isEM_flags_nofudge_sublead; //!

  // is MonteCarlo
  bool is_mc; //!

  // list of systematics
  std::vector<CP::SystematicSet> sysList; //!

  // cinematic variables
  float mass; //!
  float mgg; //!
  float mass_gev; //!
  float mass_MVA; //!
  float mass_egamma; //!
  float costhetastar; //!
  double deltaphi; //!

  TLorentzVector LV_leading; //!
  TLorentzVector LV_subleading; //!
  TLorentzVector LV_diphoton; //!

  TLorentzVector LV_egamma; //!

  float rho_median; //!
  float pt_subleading; //!
  float pt_leading; //!
  float phi_subleading; //!
  float phi_leading; //!
  float eta_subleading; //!
  float eta_leading; //!
  int conv_subleading; //!
  int conv_leading; //!
  unsigned int isEM_subleading; //!
  unsigned int isEM_leading; //!
  unsigned int isEM_nofudge_subleading; //!
  unsigned int isEM_nofudge_leading; //!

  int author_leading; //!
  int author_subleading; //!

  float topoetcone40_leading; //!
  float topoetcone40_DDcorrected_leading; //!
  float topoetcone40_rel17_leading; //!

  float topoetcone40_rel17_electron_leading; //!
  float topoetcone40_trouble_electron_leading; //!
  float topoetcone40_electron_leading; //!
  int author_electron_leading; //!

  float topoetcone40_subleading; //!
  float topoetcone40_DDcorrected_subleading; //!
  float topoetcone20_leading; //!
  float topoetcone20_subleading; //!

  float truth_etcone40_leading; //!
  float truth_etcone40_subleading; //!
  float truth_etcone20_leading; //!
  float truth_etcone20_subleading; //!

  float truth_etcone40_PUcorr_leading; //!
  float truth_etcone40_PUcorr_subleading; //!
  float truth_etcone20_PUcorr_leading; //!
  float truth_etcone20_PUcorr_subleading; //!

  float truth_ptcone40_leading; //!
  float truth_ptcone40_subleading; //!
  float truth_ptcone20_leading; //!
  float truth_ptcone20_subleading; //!

  float truth_ptcone40_PUcorr_leading; //!
  float truth_ptcone40_PUcorr_subleading; //!
  float truth_ptcone20_PUcorr_leading; //!
  float truth_ptcone20_PUcorr_subleading; //!

  float truth_local_etcone40_leading; //!
  float truth_local_etcone40_subleading; //!
  float truth_local_etcone20_leading; //!
  float truth_local_etcone20_subleading; //!

  float truth_rho_central; //!
  float truth_rho_forward; //!

  int loose_leading; //!
  int loose_prime_leading; //!
  int tight_leading; //!
  int my_tight_leading; //!
  int tight_subleading; //!
  int loose_subleading; //!
  int loose_prime_subleading; //!
  int parent_pdgID_truth_ld; //!
  int parent_pdgID_truth_subld; //!
  int origin_truth_ld; //!
  int origin_truth_subld; //!

  bool bg_truth_match_leading; //!
  bool bg_truth_match_origin_leading; //!

  float Z_pointing_leading; //!
  float Z_pointing_subleading; //!

  float Rhad1_leading; //!
  float Rhad_leading; //!
  float e277_leading; //!
  float Reta_leading; //!
  float Rphi_leading; //!
  float weta2_leading; //!
  float f1_leading; //!
  float DeltaE_leading; //!
  float wtots1_leading; //!
  float weta1_leading; //!
  float fracs1_leading; //!
  float Eratio_leading; //!

  float Rhad1_subleading; //!
  float Rhad_subleading; //!
  float e277_subleading; //!
  float Reta_subleading; //!
  float Rphi_subleading; //!
  float weta2_subleading; //!
  float f1_subleading; //!
  float DeltaE_subleading; //!
  float wtots1_subleading; //!
  float weta1_subleading; //!
  float fracs1_subleading; //!
  float Eratio_subleading; //!

  float Rhad1_leading_nofudge; //!
  float Rhad_leading_nofudge; //!
  float e277_leading_nofudge; //!
  float Reta_leading_nofudge; //!
  float Rphi_leading_nofudge; //!
  float weta2_leading_nofudge; //!
  float f1_leading_nofudge; //!
  float DeltaE_leading_nofudge; //!
  float wtots1_leading_nofudge; //!
  float weta1_leading_nofudge; //!
  float fracs1_leading_nofudge; //!
  float Eratio_leading_nofudge; //!

  float Rhad1_subleading_nofudge; //!
  float Rhad_subleading_nofudge; //!
  float e277_subleading_nofudge; //!
  float Reta_subleading_nofudge; //!
  float Rphi_subleading_nofudge; //!
  float weta2_subleading_nofudge; //!
  float f1_subleading_nofudge; //!
  float DeltaE_subleading_nofudge; //!
  float wtots1_subleading_nofudge; //!
  float weta1_subleading_nofudge; //!
  float fracs1_subleading_nofudge; //!
  float Eratio_subleading_nofudge; //!

  float E1_E2_leading; //!
  float etaS2_leading; //!
  float etaS2_subleading; //!

  float ptvarcone20_leading; //!
  float ptcone20_leading; //!
  float ptvarcone40_leading; //!
  float ptcone40_leading; //!
  float topoetcone20_Pt_leading; //!
  float ptvarcone20_Pt_leading; //!

  float ptcone20_subleading; //!
  float ptcone40_subleading; //!

  int parent_pdgID_leading; //!
  int pdgID_leading; //!
  int origin_leading; //!
  int type_leading; //!
  bool truth_match_leading; //!

  int parent_pdgID_subleading; //!
  int pdgID_subleading; //!
  int origin_subleading; //!
  int type_subleading; //!
  bool truth_match_subleading; //!

  bool two_truth_photons; //!
  bool pass_eta_truth_analysis; //!
  bool truth_leading_matched_leading_ph; //!
  bool truth_subleading_matched_subleading_ph; //!
  TLorentzVector truth_leading_LV; //!
  TLorentzVector truth_subleading_LV; //!
  TLorentzVector truth_diphoton_LV; //!
  const xAOD::TruthParticle *truth_leading_photon; //!
  const xAOD::TruthParticle *truth_subleading_photon; //!

  // systematics vectors
  std::vector<float> event_weight_syst; //!
  std::vector<float> mass_gev_syst; //!
  std::vector<float> topoetcone40_leading_syst; //!
  std::vector<float> topoetcone40_subleading_syst; //!
  std::vector<int> accepted_syst; //!
  std::vector<TLorentzVector> leading_LV_syst; //!
  std::vector<TLorentzVector> subleading_LV_syst; //!
  std::vector<TLorentzVector> diphoton_LV_syst; //!

  // Isolation
  std::vector<float> topoetcone40; //!
  std::vector<float> topoetcone30; //!
  std::vector<float> topoetcone20; //!

  std::vector<float> ptcone40; //!
  std::vector<float> ptcone30; //!
  std::vector<float> ptcone20; //!

  std::vector<float> ptvarcone40; //!
  std::vector<float> ptvarcone30; //!
  std::vector<float> ptvarcone20; //!

  std::vector<int> ph_parent_pdgID; //!

  bool pass_truth_match; //!

  bool FixedCutTightCaloOnly_ld; //!
  bool FixedCutTight_ld; //!
  bool FixedCutLoose_ld; //!

  bool FixedCutTightCaloOnly_subld; //!
  bool FixedCutTight_subld; //!
  bool FixedCutLoose_subld; //!

  std::vector< std::string > ph_ss_names; //!
  std::vector< std::vector<float> > ph_ss; //!

  // output NTUPLES
  EL::NTupleSvc *sample_NTUP; //!

public:
  // this is a standard constructor
  DumpNTUP() { }
  DumpNTUP(const char *name);
  virtual ~DumpNTUP();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();


  // this is needed to distribute the algorithm to the workers
  ClassDef(DumpNTUP, 1);
};

#endif // HGamTools_DumpNTUP_H
