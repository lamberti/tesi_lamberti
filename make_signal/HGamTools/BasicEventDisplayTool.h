#ifndef AudreyPackage_BasicEventDisplayTool_H
#define AudreyPackage_BasicEventDisplayTool_H

#include "TH1.h"
#include "TCanvas.h"

#include "TMath.h"
#include "TLorentzVector.h"

#include "TArrow.h"
#include "TPolyLine.h"
#include "TEllipse.h"
#include "TMarker.h"
#include "TLatex.h"
#include "TBox.h"

#include "TEnv.h"
#include "TObjArray.h"
#include "TSystem.h"
#include "TCollection.h"
#include "THashList.h"
#include "TObjString.h"
#include "TStyle.h"



class InputSettings;


typedef std::vector<TLorentzVector> TLVs;
typedef std::vector<TString> StrV;


//**********************************************************************************
// struct for the drawings input
//**********************************************************************************
typedef struct EventDrawingInput {
  TLVs jets;
  TLVs photons;
  TLVs muons;
  TLVs electrons;
  TLorentzVector met;
  TLorentzVector objectImbalance;
  StrV textLines;
} EventDrawingInput;


//**********************************************************************************
// struct for the detector composition (eta values)
//**********************************************************************************
typedef struct DetectorGeometry {
  Double_t barrelEtaLeft, barrelEtaRight,
           endcapMinEtaLeft, endcapMaxEtaLeft, endcapMinEtaRight, endcapMaxEtaRight,
           hecMinEtaLeft, hecMaxEtaLeft, hecMinEtaRight, hecMaxEtaRight,
           fcalMinEtaLeft, fcalMaxEtaLeft, fcalMinEtaRight, fcalMaxEtaRight;
} DetectorGeometry;



class BasicEventDisplayTool {
  //**********************************************************************************
  // Constants
  //**********************************************************************************
public:
  //Jets angular parameter
  const Double_t R = 0.4;

  //1 GeV value
  const Double_t GeV = 1000;

  //default ranges of frames
  const Double_t TRANSVERSE_RANGE = 1000, SIDE_RANGE = 3500, ANGULAR_RANGE = 5;

  //Detector geometry constants
  const Double_t BARREL_ETA = 1.37, ENDCAP_MIN_ETA = 1.52, ENDCAP_MAX_ETA = 2.37,
                 HEC_MIN_ETA = 2.37, HEC_MAX_ETA = 3.1, FCAL_MIN_ETA = 3.1, FCAL_MAX_ETA = 4.9;


  //**********************************************************************************
  // Variables
  //**********************************************************************************
private:
  //Histograms and canvas for drawings
  TH1F *m_transverseFrame; //!
  TH1F *m_sideFrame; //!
  TH1F *m_angularFrame; //!
  TH1F *m_blankFrame; //!
  TCanvas *m_canvas; //!

  //Detector geometry
  DetectorGeometry m_detector;

  //Drawing options
  TString m_pdf;              //Output file
  Int_t m_numEventsToDraw;    //Number of events to draw
  Bool_t m_drawDetector;      //If the detector should be drawn
  Bool_t m_drawTransverse;    //Point of views to draw
  Bool_t m_drawSide;
  Bool_t m_drawAngular;
  Bool_t m_printText;
  Bool_t m_drawEventInfo;     //Informations to print
  Bool_t m_drawObjectsInfo;
  Bool_t m_useDynamicZoom;    //If the histograms should be dynamically zoomed in
  StrV m_referentials;        //Referential frames to draw


  //**********************************************************************************
  // Methods
  //**********************************************************************************
public:
  BasicEventDisplayTool();
  virtual ~BasicEventDisplayTool();
  void initialize(InputSettings *config);
  void initialize(const TString &configFileName);
  void initialize(const TEnv *env);
  void finalize();

  //draw() method
  void draw(EventDrawingInput &input, const StrV lines = StrV());
  void draw(EventDrawingInput &input, EventDrawingInput &truthinput, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, const StrV &textLines, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, const StrV &truthtextLines, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, const TLorentzVector &met, const StrV &textLines, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, const TLorentzVector &met, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, TLVs &electrons, TLVs &muons, const TLorentzVector &met, const StrV &textLines, const StrV lines = StrV());
  void draw(TLVs &photons, TLVs &jets, TLVs &electrons, TLVs &muons, const TLorentzVector &met, const StrV &textLines, TLVs &truthphotons, TLVs &truthjets, TLVs &truthelectrons, TLVs &truthmuons, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines = StrV());

private:
  //Apply boosts to particles and detector
  void applyZBoost(EventDrawingInput &input, const Double_t &zBoost);
  void applyFullBoost(EventDrawingInput &input, const TLorentzVector &boost);
  void resetDetector();

  //Getters
  TLorentzVector getObjectImbalance(const EventDrawingInput &input);
  Double_t getM_yy(const TLVs &photons);
  Double_t getM_4l(const TLVs &electrons, const TLVs &muons);

  //Drawing methods
  void drawTransverseEvent(const EventDrawingInput &input);
  void drawSideEvent(const EventDrawingInput &input, const Bool_t drawDetector);
  void drawAngularEvent(const EventDrawingInput &input);
  void printText(const StrV lines);

  void drawEventInfo(const StrV &textLines, const TString &referential, const TLVs &photons, const TLVs &electrons, const TLVs &muons);
  void drawVectorsInfo(const EventDrawingInput &input);

  void drawSideDetector();
  void drawAngularDetector();
  void drawAngularDetectorPart(const Double_t x_min, const Double_t x_max, const Color_t color);

  void drawTransversePhoton(const TLorentzVector &photon);
  void drawSidePhoton(const TLorentzVector &photon);
  void drawAngularPhoton(const TLorentzVector &photon);

  void drawTransverseJet(const TLorentzVector &jet);
  void drawSideJet(const TLorentzVector &jet);
  void drawAngularJet(const TLorentzVector &jet);

  void drawTransverseElectron(const TLorentzVector &electron);
  void drawSideElectron(const TLorentzVector &electron);
  void drawAngularElectron(const TLorentzVector &electron);

  void drawTransverseMuon(const TLorentzVector &muon);
  void drawSideMuon(const TLorentzVector &muon);
  void drawAngularMuon(const TLorentzVector &muon);

  void drawTransverseMet(const TLorentzVector &met);
  void drawSideMet(const TLorentzVector &met);
  void drawAngularMet(const TLorentzVector &met);

  void drawTransverseImbalance(const TLorentzVector &objImbalance);
  void drawSideImbalance(const TLorentzVector &objImbalance);
  void drawAngularImbalance(const TLorentzVector &objImbalance);

  void draw4vectorInfo(const Double_t x, const Double_t y, const TLorentzVector p4, const Color_t col, const TString type);
  void drawArrow(const Double_t x, const Double_t y, const Color_t color, const Style_t style);
  void drawText(const Double_t x, const Double_t y, TString txt, const Color_t col = kBlack, const Double_t size = 0.03, const Int_t align = 12, const Bool_t ndc = true);
  void drawMarker(const Double_t x, const Double_t y, const Color_t color, const Size_t size = 1, const Style_t style = 20);
  void drawHorizontalLine(const Double_t xrange, const Double_t y, const Color_t color = kBlack, const Style_t style = 1);
  void drawTrapezoid(const Double_t x_min, const Double_t x_max, const Double_t angle_min, const Double_t angle_max, const Color_t color = kBlack, const Bool_t horizontal = true);
  void drawRectangle(const Double_t x_min, const Double_t y_min, const Double_t x_max, const Double_t y_max, const Color_t color);

  //Useful support methods
  void setAxisRange(TH1 *histo, const Double_t max_x, const Double_t max_y);
  void redrawBorders();
  Double_t calculateMaxWidthDetector(const Double_t maxRange);
  static bool comparePt(TLorentzVector &a, TLorentzVector &b);



  //**********************************************************************************
  // Inline methods
  //**********************************************************************************
public:
  //Generic draw() methods
  template<class T, class U> void draw(const T &photons, const U &jets, const StrV &textLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    draw(vphotons, vjets, textLines, lines);
  }
  template<class T, class U, class V, class W> void draw(const T &photons, const U &jets, const StrV &textLines, const V &truthphotons, const W &truthjets, const StrV &truthtextLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    TLVs vtruthphotons = vectorWrapper(truthphotons);
    TLVs vtruthjets = vectorWrapper(truthjets);
    draw(vphotons, vjets, textLines, vtruthphotons, vtruthjets, truthtextLines, lines);
  }

  template<class T, class U> void draw(const T &photons, const U &jets, const TLorentzVector &met, const StrV &textLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    draw(vphotons, vjets, met, textLines, lines);
  }
  template<class T, class U, class V, class W> void draw(const T &photons, const U &jets, const TLorentzVector &met, const StrV &textLines, const V &truthphotons, const W &truthjets, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    TLVs vtruthphotons = vectorWrapper(truthphotons);
    TLVs vtruthjets = vectorWrapper(truthjets);
    draw(vphotons, vjets, met, textLines, vtruthphotons, vtruthjets, truthmet, truthtextLines, lines);
  }

  template<class T, class U, class V, class W> void draw(const T &photons, const U &jets, const V &electrons, const W &muons, const TLorentzVector &met, const StrV &textLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    TLVs velectrons = vectorWrapper(electrons);
    TLVs vmuons = vectorWrapper(muons);
    draw(vphotons, vjets, velectrons, vmuons, met, textLines, lines);
  }
  template<class T1, class U1, class V1, class W1, class T2, class U2, class V2, class W2> void draw(const T1 &photons, const U1 &jets, const V1 &electrons, const W1 &muons, const TLorentzVector &met, const StrV &textLines, const T2 &truthphotons, const U2 &truthjets, const V2 &truthelectrons, const W2 &truthmuons, const TLorentzVector &truthmet, const StrV &truthtextLines, const StrV lines = StrV())
  {
    TLVs vphotons = vectorWrapper(photons);
    TLVs vjets = vectorWrapper(jets);
    TLVs velectrons = vectorWrapper(electrons);
    TLVs vmuons = vectorWrapper(muons);
    TLVs vtruthphotons = vectorWrapper(truthphotons);
    TLVs vtruthjets = vectorWrapper(truthjets);
    TLVs vtruthelectrons = vectorWrapper(truthelectrons);
    TLVs vtruthmuons = vectorWrapper(truthmuons);
    draw(vphotons, vjets, velectrons, vmuons, met, textLines, vtruthphotons, vtruthjets, vtruthelectrons, vtruthmuons, truthmet, truthtextLines, lines);
  }

  // Converts the data from the container into TLorentzVectors
  //      IMPORTANT: Use pointer types that possess the method TLorentzVector p4()
  template<class T> TLVs vectorWrapper(const T &container)
  {
    TLVs vector;

    for (auto item : container)
    { vector.emplace_back(item->p4()); }

    return vector;
  }


};


//**********************************************************************************
// InputSettings class definition (Mostly copied from HG::Config class)
//**********************************************************************************
class InputSettings {
private:
  TEnv m_env;

public:
  //Constructors
  InputSettings() : m_env("env")
  {
    m_env.IgnoreDuplicates(true);
  }

  InputSettings(const TEnv *env)
  {
    env->Copy(m_env);
    THashList *hl = 0;

    if ((hl = env->GetTable())) {
      TIter next(hl);
      TEnvRec *env_rec = 0;

      while ((env_rec = (TEnvRec *)next.Next()))
      { m_env.SetValue(env_rec->GetName(), env_rec->GetValue()); }
    }
  }

  InputSettings(TString fileName) : InputSettings()
  {
    if (!fileExist(fileName)) {
      printf("\nFATAL\n  Cannot find settings file %s\n\n", fileName.Data());
      abort();
    }

    int status = m_env.ReadFile(fileName.Data(), EEnvLevel(0));

    if (status != 0) {
      printf("\nFATAL\n  Cannot read settings file %s\n\n", fileName.Data());
      abort();
    }
  }

  //Destructor
  virtual ~InputSettings() { }

  //Getters
  TString getStr(TString key, TString dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  StrV  getStrV(TString key, StrV dflt)
  {
    if (m_env.Defined(key)) { return vectorize(m_env.GetValue(key, ""), " "); }

    return dflt;
  }

  int getInt(TString key, int dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  bool getBool(TString key, bool dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  double getNum(TString key, double dflt)
  {
    return m_env.GetValue(key, dflt);
  }


private:
  //Support method converting a string into a vector<TString> based on a specified separator
  StrV vectorize(TString str, TString sep)
  {
    StrV result;
    TObjArray *strings = str.Tokenize(sep.Data());

    if (strings->GetEntries() == 0) { delete strings; return result; }

    TIter istr(strings);

    while (TObjString *os = (TObjString *)istr()) {
      // the number sign and everything after is treated as a comment
      if (os->GetString()[0] == '#') { break; }

      result.push_back(os->GetString());
    }

    delete strings;
    return result;
  }

  //Ensures the file exists
  bool fileExist(TString fn) { return !(gSystem->AccessPathName(fn.Data())); }
};

#endif //AudreyPackage_BasicEventDisplayTool_H
