#ifndef HGamTools_HGamSimpleExample_H
#define HGamTools_HGamSimpleExample_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"

class HGamSimpleExample : public HgammaAnalysis {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  std::vector<std::string> m_muNames; //!
  std::vector<double> m_muRanges; //!



public:
  // this is a standard constructor
  HGamSimpleExample() { }
  HGamSimpleExample(const char *name);
  virtual ~HGamSimpleExample();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();


  // this is needed to distribute the algorithm to the workers
  ClassDef(HGamSimpleExample, 1);
};

#endif // HGamTools_HGamSimpleExample_H
