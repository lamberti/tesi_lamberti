#ifndef HGamTools_XSecSkimAndSlim_H
#define HGamTools_XSecSkimAndSlim_H

#include "HGamTools/SkimAndSlim.h"



class XSecSkimAndSlim : public SkimAndSlim {
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  bool m_unfoldSkim; //!



  // helper functions defined by the user
private:
  EL::StatusCode skipXSecEvent(bool &skip);



  // public functions needed by EventLoop
public:
  // this is a standard constructor
  XSecSkimAndSlim() { }
  XSecSkimAndSlim(const char *name);
  virtual ~XSecSkimAndSlim() { }

  // these are the functions inherited from SkimAndSlim
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();



  // this is needed to distribute the algorithm to the workers
  ClassDef(XSecSkimAndSlim, 1);
};

#endif // HGamTools_XSecSkimAndSlim_H
