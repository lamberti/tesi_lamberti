from scipy.interpolate import make_interp_spline, BSpline
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl

import sys
sys.path.append('..')
from common import common



mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['figure.figsize'] = 8.75, 7.92

# font
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['mathtext.fontset'] = 'stixsans'
mpl.rcParams['mathtext.default'] = 'rm'

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['xtick.labelsize'] = 16
mpl.rcParams['ytick.labelsize'] = 16

mpl.rcParams['xtick.top'] = True
mpl.rcParams['ytick.right'] = True

mpl.rcParams['xtick.minor.visible'] = True
mpl.rcParams['ytick.minor.visible'] = True

mpl.rcParams['legend.frameon'] = False

mpl.rcParams['verbose.level'] = 'silent'


def ROOT_to_np_pfd(xaxis, pdf):
    pdf_array = []
    for x in xaxis:
        pdf_array.append(pdf.Eval(x))
    pdf_array = np.array(pdf_array)
    # Smoothing with BSpline
    xnew = np.linspace(np.min(xaxis), np.max(xaxis), len(xaxis)*10)
    spl = make_interp_spline(xaxis, pdf_array, k=3)
    smoothed_pdf = spl(xnew)
    return pdf_array, smoothed_pdf, xnew

def ROOT_to_np_histo(xaxis, histo):
    histo_array = []
    for x in xaxis:    
        histo_array.append(histo.Eval(x))
    histo_array = np.array(histo_array)
    return histo_array

def get_CB_parameters(ws):
    parameters_info = [
        (r'$\mu_{CB} = %.3f$' % (ws.var('m0').getVal()/1000), 0.7, 0.87),
        (r'$\sigma_{CB} = %.3f$' % (ws.var('sigma').getVal()/1000), 0.7, 0.78),
        (r'$\alpha_{Hi} = %.3f$' % (ws.var('alphaHi').getVal()), 0.7, 0.69),
        (r'$\alpha_{Lo} = %.3f$' % (ws.var('alphaLo').getVal()), 0.7, 0.60),
        (r'$n_{Hi} = %.3f$' % (ws.var('nHi').getVal()), 0.7, 0.51),
        (r'$n_{Lo} = %.3f$' % (ws.var('nLo').getVal()), 0.7, 0.42)
        ]
    return parameters_info


def signal_plot(quantity, ibin, ws, frame, pdf_name, rdh_name):
    latex_label = common.latex_labels[quantity]
    binning = common.binnings[quantity]
    parameters_info = get_CB_parameters(ws)
    curve_pdf = frame.findObject(pdf_name)
    histo_data = frame.findObject(rdh_name)
    xs = histo_data.GetX()
    xs.SetSize(histo_data.GetN())
    xs = np.array(xs)
    
    pdf_array, smoothed_pdf, pdf_xaxis = ROOT_to_np_pfd(xs, curve_pdf)
    np_histo = ROOT_to_np_histo(xs, histo_data)
    residue = np.array(pdf_array) - np_histo
    
    fig, ax = plt.subplots(sharex=True)
    
    # Main plot
    ax = fig.add_axes((0.,0.3, 1., 0.7))
    ax.errorbar(np.array(xs) / 1000, np_histo, xerr=0.5, fmt='o', color='k')
    ax.plot(pdf_xaxis / 1000, smoothed_pdf, color='red', lw=2)
    ax.set_xlim(110, 140)
    ax.set_ylabel('Entries / 1 GeV', horizontalalignment='right', y=1.0)
    ax.get_xaxis().set_ticklabels([])

    for name, x_val, y_val in parameters_info:
        ax.text(x_val, y_val, name, transform=ax.transAxes, fontsize=20)
    ax.text(0.05, 0.87, 'ATLAS', style='italic', fontweight='bold', transform=ax.transAxes, fontsize=22)
    ax.text(0.19, 0.87, 'Preliminary', transform=ax.transAxes, fontsize=22)
    ax.text(0.05, 0.79, r'$\sqrt{s} = 13$ TeV; L $= 140$ fb$^{-1}$', transform=ax.transAxes, fontsize=20)
    print(binning[ibin], binning[ibin-1])
    if not binning[ibin-1] == binning[-1]: 
        if quantity == 'N_j_30':                                                                                                                  
            ax.text(0.05, 0.71, r'%d < %s < %d' % (binning[ibin-1],
                                                   str(latex_label.split('[')[0]),
                                                   binning[ibin]), transform=ax.transAxes, fontsize=20)
        elif quantity == 'Dphi_j_j_30_signed':
            if binning[ibin] == binning[0]:
                ax.text(0.05, 0.71, r'%s for <2-jets events' % str(latex_label.split('[')[0]), transform=ax.transAxes, fontsize=20)
            else:
                ax.text(0.05, 0.71, r'%.2f < %s < %.2f' % (binning[ibin-1],
                                                           str(latex_label.split('[')[0]),
                                                           binning[ibin]), transform=ax.transAxes, fontsize=20)
        elif quantity == 'yAbs_yy':
            ax.text(0.05, 0.71, r'%.2f < %s < %.2f' % (binning[ibin-1],
                                                       str(latex_label.split('[')[0]),
                                                       binning[ibin]), transform=ax.transAxes, fontsize=20)
        else:
            ax.text(0.05, 0.71, r'$%d$ [GeV] $<$ %s $< %d$ [GeV]' % (binning[ibin-1] / 1000,
                                                                     str(latex_label.split('[')[0]),
                                                                     binning[ibin] / 1000), transform=ax.transAxes, fontsize=20)
    elif binning[ibin-1] == binning[-1]:
        if quantity == 'N_j_30':                                                                                                                  
            ax.text(0.05, 0.71, r'%s $\geq$ %d' % (str(latex_label.split('[')[0]),
                                              binning[ibin-1]), transform=ax.transAxes, fontsize=20)
        elif quantity == 'Dphi_j_j_30_signed' or quantity == 'yAbs_yy':
            ax.text(0.05, 0.71, r'%s > %.2f' % (str(latex_label.split('[')[0]),
                                                binning[ibin-1]), transform=ax.transAxes, fontsize=20)
        else:
            ax.text(0.05, 0.71, r'%s $> %d$ [GeV]' % (str(latex_label.split('[')[0]),
                                                      binning[ibin-1] / 1000), transform=ax.transAxes, fontsize=20)
    # Residue plot
    ax = fig.add_axes((0.,0., 1., 0.3))
    ax.plot(xs / 1000, residue, 'o', color='k')
    ax.axhline(-0.5, ls='--', color='red')
    ax.axhline(0, color='red')
    ax.axhline(0.5, ls='--', color='red')
    ax.set_xlim(110, 140)
    ax.set_ylim(-2, 2)
    ax.set_xlabel(r'$m_{\gamma\gamma}$ [GeV]', horizontalalignment='right', x=1.0)
    ax.set_ylabel('Residue', horizontalalignment='right', y=1.0)

    return fig
