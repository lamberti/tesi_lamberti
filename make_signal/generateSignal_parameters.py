import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.ProcessLine(".L RooTwoSidedCBShape.cxx")
# Silence RooFit
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.InputArguments)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval)
ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.DataHandling)
ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)
ROOT.RooMsgService.instance().setSilentMode(True)
ROOT.gROOT.SetBatch(True) 

import os
import numpy as np
import logging
from matplotlib import pyplot as plt
import signal_plot_layout as signal_plot

import sys
sys.path.append('..')
from common import common


output_basedir = '../output/signalParametrisation'

latex_quantities = {
    'pT_yy': 'p_T^{\gamma\gamma}',
    'yAbs_yy': '|y|_{\gamma\gamma}',
    'Dphi_j_j_30_signed': '\delta\phi_{jj}^{30}',
    'pT_j1_30': 'p_T^{j1, 30}',
    'N_j_30': 'N_j^{30}',
    'm_jj_30': 'm_{jj}^{30}'
    }


logging.basicConfig(level=logging.INFO)

def create_folder_if_not_exists(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

def create_workspace():
    ws = ROOT.RooWorkspace('signalWS')
    ws.factory('mass[105000, 160000]')
    ws.factory('m0[125E3, 123E3, 127E3]')
    ws.factory('sigma[2E3, 0.5E3, 4E3]')
    ws.factory('alphaLo[2, 0.5, 4]')
    ws.factory('nLo[10, 0.1, 100]')
    ws.factory('alphaHi[2, 0.5, 4]')
    ws.factory('nHi[5, 0.1, 10]')
    ws.factory('RooTwoSidedCBShape::signal(mass, m0, sigma, alphaLo, nLo, alphaHi, nHi)')
    
    return ws


def get_histogram(f, quantity, ibin):
    hist_name = 'histo2d_m_yy_%s__All_isPassed' % quantity
    logging.info('Getting histogram 2d %s' % hist_name)
    hist2d = f.Get(hist_name).Clone()
    w_initial_plusDalitz = f.Get('w_initial_plusDalitz').GetBinContent(1)
    hist2d.Scale(1. / w_initial_plusDalitz)
    hist1d = hist2d.ProjectionY('_py', ibin, ibin)
    return hist1d

def fit_histo(histo, pdf, obs):
    rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(obs), histo)
    fr = pdf.fitTo(rdh,
                   ROOT.RooFit.PrintLevel(-1),
                   ROOT.RooFit.SumW2Error(True),
                   ROOT.RooFit.Save())
    return fr

def fit(f, directory, quantity, ibin, ws, do_plot=False):
    pdf = ws.pdf('signal')
    obs = ws.var('mass')
    histo = get_histogram(f, quantity, ibin)
    fr = fit_histo(histo, pdf, obs)
    ws.SaveAs("%s/signalWS_%s_bin_%s.root" % (directory, quantity, ibin))

    if do_plot:
        frame = obs.frame()
        rdh = ROOT.RooDataHist('root_data_hist', 'root_data_hist', ROOT.RooArgList(obs), histo)
        rdh.plotOn(frame, ROOT.RooFit.Binning(55))
        pdf.plotOn(frame)
        fig_plot = signal_plot.signal_plot(quantity, ibin, ws, frame, 'signal_Norm[mass]', 'h_root_data_hist')
        plt.savefig('%s/fit_%s_bin_%d.pdf' % (directory, quantity, ibin), bbox_inches='tight')
    return fr

def write_parameters(f, fr, output_dir, quantity, ibin):

    def get_yield(f, quantity, ibin):
        yield_histo = f.Get('histo1d_%s_reco__All_isPassed_norm' % quantity) 
        yield_value = yield_histo.GetBinContent(ibin)
        return yield_value

    def get_val(fr, name):
        return fr.floatParsFinal().find(name).getVal()

    m0_value = get_val(fr, 'm0')
    sigma_value = get_val(fr, 'sigma')
    alphaLo_value = get_val(fr, 'alphaLo')
    alphaHi_value = get_val(fr, 'alphaHi')
    nLo_value = get_val(fr, 'nLo')
    nHi_value = get_val(fr, 'nHi')
    sigYield_value = get_yield(f, quantity, ibin)

    text = ''
    text += 'muCBNom_SM_%s_%d_m125000_c0 %f\n' % (quantity, ibin, m0_value / 1E3)
    text += 'sigmaCBNom_SM_%s_%d_m125000_c0 %f\n' % (quantity, ibin, sigma_value / 1E3)
    text += 'alphaCBLo_SM_%s_%d_m125000_c0 %f\n'  % (quantity, ibin, alphaLo_value)
    text += 'alphaCBHi_SM_%s_%d_m125000_c0 %f\n'  % (quantity, ibin, alphaHi_value)
    text += 'nCBLo_SM_%s_%d_c0 %f\n'  % (quantity, ibin, nLo_value)
    text += 'nCBHi_SM_%s_%d_c0 %f\n'  % (quantity, ibin, nHi_value)
    text += 'sigYield_SM_%s_%d_m125000_c0 %f' % (quantity, ibin, sigYield_value)
    parameters_output = open('%s/resonance_paramList.txt' % output_dir, 'w')
    parameters_output.write(text)
    
if __name__ == '__main__':

    import argparse
    
    __help__ = '''python generateSignal_parameters.py ../merged_normalized_final/mcAll_prodAll_MxAODJetSys._sysNominal.merged.norm.root --quantity pT_yy --bin 1'''
    
    parser = argparse.ArgumentParser(description='Fit signal shape.', epilog=__help__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('filename', help='the file holding the histograms')
    parser.add_argument('--quantity')
    parser.add_argument('--bin', type=int)
    parser.add_argument('--do-plot', action='store_true')

    args = parser.parse_args()
    
    ws = create_workspace()
    ws.saveSnapshot('initial', ws.allVars())
    f = ROOT.TFile.Open(args.filename)

    if args.quantity and args.bin:
        quantities = [args.quantity]
        all_ibins = [[args.bin]]

    elif args.quantity and not args.bin:
        quantities = [args.quantity]
        all_ibins = [range(len(common.binnings[args.quantity])+1)]
        logging.info('reco bins: %s' % all_ibins)
        home_directory = os.path.join(output_basedir, args.quantity)
   
    elif not args.quantity and not args.bin:
        quantities = common.quantities
        all_ibins = []
        for quantity in common.quantities:
            all_ibins.append(range(len(common.binnings[quantity])+1))

    else:
        raise ValueError('not valid input')


    for quantity, ibins in zip(quantities, all_ibins):
        home_directory = "%s/%s" % (output_basedir, quantity)
        create_folder_if_not_exists(home_directory)
        for ibin in ibins:
            logging.info('Fitting quantity %s bin %d' % (quantity, ibin)) 
            ws.loadSnapshot('initial')
            output_dir = '%s/%s_bin_%s' % (home_directory, quantity, ibin)
            create_folder_if_not_exists(output_dir)
            fr = fit(f, output_dir, quantity, ibin, ws, do_plot=True)
            write_parameters(f, fr, output_dir, quantity, ibin)
 
